<?php

/**
 * Easy form builder.
 *
 * This is a widget, making it very easy to build big forms with ease.
 *  This widget not only creates a standardized layout, it also generates
 *  the required JavaScript code on the fly. This includes validation,
 *  Datepicker initialization, and so on.
 * 
 * @property CFormModel $model The builder requires a model which contains
 *  the attribute labels and validation rules.
 * @property string $formName Name of the form. Uniquely generated each time
 *  the widget is used.
 * @property Controller $controller The controller using the Widget.
 */
class Form extends CWidget
{
	/**
	 * Name of the form (unique name)
	 *
	 * @var string
	 */
	protected $formName;
	
	/**
	 * Form Model object.
	 * 
	 * @var CFormModel
	 */
	public $model;
	
	/**
	 * The action of the form.
	 * 
	 * @var string
	 */
	public $action = '';
	
	/**
	 * Method of the form (get/post etc).
	 * 
	 * Defaults to <code>'post'</code>
	 * 
	 * @var string
	 */
	public $method = 'post';
	
	/**
	 * HTML options (additional HTML attributes for the form)
	 * 
	 * @see CHtml
	 * 
	 * @var array
	 */
	public $htmlOptions = array();
	
	/**
	 * Set this to true to initialize the RTE javascript and styles
	 * 
	 * @var boolean
	 */
	public $useRte = false;
	
	/**
	 * List of RTE editors.
	 * 
	 * @var array
	 */
	protected $_rteFields = array();
	
	/**
	 * Set this to true to enable client side validation.
	 * 
	 * If enabled, the form will automatically generate javascript code
	 *  for validation, based on the validators in the model.
	 *  
	 * @var boolean
	 */
	public $useClientValidation = true;
	
	/**
	 * Success functions for individual fields.
	 * 
	 * List of functions to execute upon a field being validated successfully.
	 *
	 * @var array
	 */
	public $validationSuccessFunctions = array();
	
	/**
	 * Sets whether the form submit should run validation.
	 * 
	 * If enabled, the form will validate all fields upon being submitted.
	 *  The form will not submit when there are any errors.
	 *  
	 * @var boolean
	 */
	public $validateOnSubmit = true;
	
	/**
	 * Enable beta AJAX submit feature.
	 * 
	 * Note that this requires additional code in the controller to work.
	 *  If this code is not present, an error will be sent instead.
	 * 
	 * @var boolean
	 */
	public $useAjaxSubmit = false;
	
	/**
	 * Contains a list of attributes that should be validated through AJAX.
	 * 
	 * Attributes that are added to this array will be validated through AJAX,
	 *  instead of normal JavaScript. This allows for more advanced validation
	 *  while still using JavaScript. Client side validation must be enabled
	 *  for this array to have any effect.
	 *  
	 * @var array
	 */
	public $ajaxValidationFields = array();
	
	/**
	 * Javascript function to be called when the form was submitted successfully
	 * 
	 * Name of the function to use, or the function itself.
	 *  The function can take 2 parameters: the success JSON returned from the
	 *  server, and the form that calls it. Set to false to disable (default)
	 * 
	 * @var string|false
	 */
	public $ajaxSuccessFunction = false;
		
	/**
	 * CSS class given to error messages.
	 * 
	 * @var string
	 */
	public $errorMessageCssClass = 'errorMessage';
	
	/**
	 * Variable to keep track of whether a row is currently being built or not.
	 * 
	 * @var boolean
	 */
	protected $_rowActive;
	
	/**
	 * Variable to keep track of fields that have an error message.
	 * 
	 * Used when generating client side validation code.
	 * 
	 * @var array
	 */
	protected $_errorFields = array();
	
	/**
	 * Cache for JavaScript code to place at the bottom.
	 * 
	 * @var array
	 */
	protected $jsCode = array();
	
	/**
	 * Extra Javascript code to add
	 * 
	 * @var array
	 */
	protected $extraJs = array();
	
	/**
	 * Lists the fields that should not be validated on blur.
	 * 
	 * e.g. Rich Text editors, date fields, ...
	 * 
	 * @var array
	 */
	protected $noBlurFields = array();
	
	/**
	 * Gets the form name of this form.
	 * 
	 * @return string
	 */
	public function getFormName()
	{
		return $this->formName;
	}
	
	/**
	 * Add extra JS code from the form itself
	 * 
	 * @param string $jsCode
	 */
	public function addJs($jsCode)
	{
		$this->extraJs[] = $jsCode;
	}
		
	/**
	 * Initializes the widget.
	 * 
	 * Starts the actual form, both in the HTML and the JavaScript code.
	 */
	public function init()
	{
		CHtml::$errorCss = '';
		$this->htmlOptions['id'] = $this->id;
		$this->formName = 'f' . CHtml::modelName($this->model) . uniqid();
		echo CHtml::beginForm($this->action, $this->method, $this->htmlOptions);
	}
	
	/**
	 * This method is invoked at the end of the widget.
	 * 
	 * In this method, the form is properly closed and the javascript code
	 *  is generated and outputted.
	 */
	public function run()
	{
		// Close the form
		echo CHtml::endForm();
		
		// Generate validation code
		$this->generateClientValidation();
		
		// If the RTE is needed, load its required script file.
		if($this->useRte)
		{
			Yii::app()->clientScript->registerScriptFile(
				Yii::app()->baseUrl . '/js/tinymce/tinymce.min.js'
			);
		}
		
		// Do we have JavaScript to output?
		if(count($this->jsCode)) {
			// If this is ajax, output it directly
			if(Yii::app()->request->isAjaxRequest)
			{			
				if($this->useAjaxSubmit)
				{
					$this->jsCode[] = "modalSave = function() { {$this->formName}.ajaxSubmit() }";
				}
				else
				{
					$this->jsCode[] = "modalSave = function() { $('#{$this->id}').submit() }";
				}
				echo '<script>' . implode("\n",$this->jsCode) . '</script>';
			}
			// Else register it for later
			else
			{
				Yii::app()->clientScript->registerScript($this->id.'_AutoJS',implode("\n",$this->jsCode));
			}
		}
	}
	
	/**
	 * Generates the client side JavaScript code used for validation.
	 */
	public function generateClientValidation()
	{	
		// Enabled?
		if(!$this->useClientValidation)
		{
			return;
		}
		
		// Create the form
		$this->jsCode[] = "{$this->formName} = new Form('" . $this->id . "',"
			. CJavaScript::encode(array(
				'ajaxSubmit' => $this->useAjaxSubmit,
				'validateSubmit' => $this->validateOnSubmit,
				'ajaxSuccess' => ($this->ajaxSuccessFunction ? new CJavaScriptExpression($this->ajaxSuccessFunction) : false),
			)) . ');';
		
		// Extra JavaScript to add?
		foreach($this->extraJs as $extra)
		{
			$this->jsCode[] = $extra;
		}		

		// For each field that actually has an error field (would be pretty redundant otherwise)
		foreach($this->_errorFields as $field => $errorId)
		{
			// Perse the ID of the field in the form
			$fieldId = CHtml::activeId($this->model, $field);
			
			// Get all validators
			$validators = $this->model->getValidators($field);
			
			$clientValidators = array();
			foreach($validators as $validator)
			{
				/* @var $validator CValidator */
				if($res=$validator->clientValidateAttribute($this->model, $field))
				{
					$clientValidators[] = $res;
				}
			}
			
			// Field uses AJAX validation?
			if(in_array($field,$this->ajaxValidationFields))
			{
				$clientValidators[] = "if(!messages.length && !skipAjax){\n"
					."\t{$this->formName}.ajaxValidate(field,value,messages);\n}\n";
			}
			
			// So, do we have any validators?
			if(count($clientValidators))
			{
				// Standard options for the field:
				$fieldOptions = array(
					// Validation function
					'validation' => new CJavaScriptExpression("function(field,value,messages,skipAjax){"
						. (!empty($this->_rteFields[$field]) ? "\ntinymce.get('{$fieldId}').save(); value = $('#{$fieldId}').val();" : '')
						. implode('',$clientValidators) . '}'),
						
					// ID of the error message HTML element
					'errorId' => $errorId,
					
					//name of the attribute in the form
					'attribute' => $field,
				);
				
				// Don't validate these fields on blur (e.g. DatePickers are validated on close)
				if(!empty($this->noBlurFields[$fieldId]))
				{
					$fieldOptions['disableBlurValidation'] = true;	
				}
				
				// Function to be called if validation was successful?
				if(!empty($this->validationSuccessFunctions[$field]))
				{
					$fieldOptions['validationSuccess'] 
						= new CJavaScriptExpression($this->validationSuccessFunctions[$field]);
				}
				
				// Register the field to the form
				$this->jsCode[] = "{$this->formName}.addField('{$fieldId}'," . CJavaScript::encode($fieldOptions) . ");";
			}
			else
			{
				// Register the field anyway, but without validators. Maybe we want to manually add validation.
				$this->jsCode[] = "{$this->formName}.addField('{$fieldId}',{errorId:'{$errorId}',attribute:'{$field}'});";
			}
		}
	}
	
	/**
	 * Generic function to wrap a form element in the correct structure.
	 * 
	 * This function adds the correct layout, and will also add the label and error message.
	 * 
	 * @param string $text Text to wrap
	 * @param string $attribute Name of the attribute, as defined in the model
	 * @param string $type Type of the input field (text/radio/...)
	 * @param array $htmlOptions HTML attributes, see CHtml
	 */
	public function wrap($text, $attribute, $type='', $htmlOptions=array())
	{
		// Already have a row open?
		if( $this->_rowActive )
		{
			echo $text;
			if( $type == 'check' || $type == 'radio' )
			{
				// Put label behind the field in case of checkbox or radio
				echo '&nbsp;', CHtml::activeLabelEx($this->model, $attribute, $htmlOptions);
			}
		}
		else
		{
			$errorCss = $this->model->hasErrors($attribute) ? ' error' : '';
			echo '<div class="row',$errorCss,'"><div class="label">',
				CHtml::activeLabelEx($this->model, $attribute, $htmlOptions),
				'</div><div class="form_input">',
				$text,
				'<br />';
			$this->error($attribute, $htmlOptions);
			echo '</div></div>';
		}
	}
	
	/**
	 * Starts a new empty row
	 * 
	 * @param string $label Optional label for the row
	 * @param string $labelfor The "for" attribute of the label if set
	 * @param array $htmlOptions
	 */
	public function startRow($label=false,$labelfor=false,$htmlOptions=array())
	{
		$this->_rowActive = true;
		echo '<div class="row"><div class="label">';
		if( $label )
		{
			echo CHtml::label($label,$labelfor,$htmlOptions);
		}
		echo '</div><div class="form_input">';
	}
	
	/**
	 * Ends an open row
	 */
	public function endRow()
	{
		if(!$this->_rowActive)
		{
			return;
		}
		$this->_rowActive = false;
		echo '</div></div>';
	}
	
	/**
	 * Creates a single checkbox
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML attributes
	 */
	public function button($label, $htmlOptions=array())
	{
		if(!($active = $this->_rowActive))
		{
			$this->startRow();
		}
		if(empty($htmlOptions['class']))
		{
			$htmlOptions['class'] = 'button';
		}
		echo CHtml::button($label, $htmlOptions);
		if(!$active)
		{
			$this->endRow();
		}
	}
	
	/**
	 * Creates a single checkbox
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML attributes
	 */
	public function checkbox($attribute, $htmlOptions=array())
	{
		if(!($active = $this->_rowActive))
		{
			$this->startRow();
		}
		$this->wrap(CHtml::activeCheckBox($this->model, $attribute, $htmlOptions),$attribute,'check',$htmlOptions);
		if(!$active)
		{
			$this->endRow();
		}
	}
	
	/**
	 * Creates a checkbox list
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $data Key-Value pairs of the boxes to add
	 * @param array $htmlOptions HTML attributes
	 */
	public function checkboxes($attribute, $data, $htmlOptions=array())
	{
		$this->wrap(CHtml::activeCheckBoxList($this->model, $attribute, $data, $htmlOptions),$attribute,'checks',$htmlOptions);
	}
	
	/**
	 * Creates a text date field
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML attributes
	 */
	public function date($attribute, $pickerOptions = array(), $htmlOptions=array())
	{
		$fieldId = CHtml::activeId($this->model, $attribute);
		
		// Date picker options
		$pickOpts = $pickerOptions + array(
			'dateFormat' => "yy-mm-dd",
			'changeMonth' => true,
			'changeYear' => true,
			'showOtherMonths' => true,
			'selectOtherMonths' => true,
			'onClose' => new CJavaScriptExpression("function() { {$this->formName}.validate(\$('#{$fieldId}'));  }"),
		);
		
		// Init date picker code
		$this->jsCode[] = '$("#' . $fieldId . '").datepicker('
			. CJavaScript::encode($pickOpts) . ');';
		
		// Output actual field
		$this->wrap(CHtml::activeTextField($this->model, $attribute, $htmlOptions),$attribute,'date',$htmlOptions);
		
		// Register as non-blur field
		$this->noBlurFields[CHtml::activeId($this->model, $attribute)] = true;
	}
	
	/**
	 * Creates a dropdown list
	 * 
	 * @param string $attribute Name of the attribute in the model
	 * @param array $params Array of key-value pairs with the dropdown options
	 * @param array $htmlOptions HTML attributes
	 */
	public function dropdown($attribute, $params, $htmlOptions = array())
	{
		$this->wrap(CHtml::activeDropDownList($this->model, $attribute, $params, $htmlOptions), $attribute, 'dropdown', $htmlOptions);
	}
	
	/**
	 * Creates an error field
	 * 
	 * @param string $attribute Name of the attribute the error is for
	 * @param array $htmlOptions HTML attributes
	 */
	public function error($attribute, $htmlOptions=array())
	{
		if(!isset($htmlOptions['id']))
		{
			$inputID=isset($htmlOptions['inputID']) ? $htmlOptions['inputID'] : CHtml::activeId($this->model,$attribute);
			$htmlOptions['id']=$inputID.'_em_';
		}
		$out = CHtml::error($this->model, $attribute, $htmlOptions);
		if(!$out)
		{
			if(isset($htmlOptions['style']))
			{
				$htmlOptions['style']=rtrim($htmlOptions['style'],';').';display:none';
			}
			else
			{
				$htmlOptions['style']='display:none';
			}
			if(empty($htmlOptions['class']))
			{
				$htmlOptions['class']=$this->errorMessageCssClass;
			}
			echo CHtml::tag(CHtml::$errorContainerTag,$htmlOptions,'');
		}
		else
		{
			echo $out;
		}
		$this->_errorFields[$attribute] = $htmlOptions['id'];
	}
	
	/**
	 * Creates a file upload field
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML attributes
	 */
	public function file($attribute, $htmlOptions=array())
	{
		$this->wrap(CHtml::activeFileField($this->model, $attribute, $htmlOptions), $attribute, 'file', $htmlOptions);
	}
	
	/**
	 * Creates a hidden field
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML attributes
	 */
	public function hidden($name, $value='', $htmlOptions=array())
	{
		echo CHtml::hiddenField($name, $value, $htmlOptions);
	}
	
	/**
	 * Creates a numeric field
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML attributes
	 */
	public function number($attribute,$htmlOptions=array())
	{
		$this->wrap(CHtml::activeNumberField($this->model, $attribute, $htmlOptions),$attribute,'text',$htmlOptions);
	}
	
	/**
	 * Creates a password field
	 * 
	 * @param string $name Name of the password field
	 * @param array $htmlOptions HTML attributes
	 */
	public function password($attribute, $htmlOptions=array())
	{
		$this->wrap(CHtml::activePasswordField($this->model,$attribute,$htmlOptions), $attribute, 'password', $htmlOptions);
	}
	
	/**
	 * Creates a single radio button
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML attributes
	 */
	public function radio($attribute, $htmlOptions=array())
	{
		$this->wrap(CHtml::activeRadioButton($this->model, $attribute, $htmlOptions),$attribute,'radio',$htmlOptions);
	}
	
	/**
	 * Creates a list of radio buttons
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $data Key-Value pairs of the buttons to add
	 * @param array $htmlOptions HTML attributes
	 */
	public function radios($attribute, $data, $htmlOptions=array())
	{
		$this->wrap(CHtml::activeRadioButtonList($this->model, $attribute, $data, $htmlOptions),$attribute,'radios',$htmlOptions);
	}
	
	/**
	 * Creates a rich text editor field
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML attributes
	 */
	public function richText($attribute,$htmlOptions=array())
	{
		$fieldId = CHtml::activeId($this->model, $attribute);
		
		// JavaScript code to init the RTE
		$this->jsCode[] = 
"var opts = mceConfig; opts['selector'] = '#" . $fieldId  . "';
opts['setup'] = function(editor) {
	editor.on('blur', function(e) {
		{$this->formName}.validate($('#'+editor.id));
	});
};
tinymce.init(opts);";
		
		// Make sure to load the RTE code
		$this->useRte = true;
		$this->_rteFields[$attribute] = true;
		$this->noBlurFields[$fieldId] = true;
		
		$this->wrap(CHtml::activeTextArea($this->model, $attribute, $htmlOptions),$attribute,'text_area',$htmlOptions);
	}
	
	/**
	 * Creates a submit button.
	 * 
	 * Integrates with the Modal system: If a modal is being sent, the submit
	 *  button is added to the modal buttons, instead of the form itself.
	 * 
	 * @param string $buttonText Text that should appear on the button
	 * @param array $htmlOptions HTML options
	 */
	public function submit($buttonText='Submit', $htmlOptions=array())
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			// Modal
			$this->controller->setExtraModalButton($buttonText);
		}
		else
		{
			if(empty($htmlOptions['class']))
			{
				$htmlOptions['class'] = 'button form_submit';
			}
			
			echo '<div class="buttons">',
				CHtml::submitButton($buttonText,$htmlOptions);
			
			if($this->useAjaxSubmit)
			{
				echo '<span class="ajax-loading form-ajax-loading" style="display:none;"><img src="',
					Yii::app()->request->baseUrl,
					'/images/ajax-throbber-blk.gif" alt="AJAX..." /></span>';
			}
			
			echo '</div>';
		}
	}
	
	/**
	 * Creates a text field
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML options
	 */
	public function text($attribute,$htmlOptions=array())
	{
		$this->wrap(CHtml::activeTextField($this->model, $attribute, $htmlOptions),$attribute,'text',$htmlOptions);
	}
	
	/**
	 * Creates a text area
	 *
	 * @param string $attribute Name of the attribute in the model
	 * @param array $htmlOptions HTML options
	 */
	public function textArea($attribute,$htmlOptions=array())
	{
		$this->wrap(CHtml::activeTextArea($this->model, $attribute, $htmlOptions),$attribute,'text_area',$htmlOptions);
	}
}