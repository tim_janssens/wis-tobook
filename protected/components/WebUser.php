<?php

/**
 * Extension of the user component.
 * 
 * The user can be accessed through <code>Yii::app()->user</code>
 * 
 * @property string $username Username of the logged in user
 * @property array $roles Roles of the user. Currently, we only use "user" as
 *  a role, so it is pretty redundant to check this if you already know
 *  they are logged in.
 * @property string $url URL of the user's profile page.
 * @property string $avatarUrl URL of the user's avatar.
 */
class WebUser extends CWebUser
{
	/**
	 * Gets the user's username.
	 * 
	 * @return string|false
	 */
	public function getUsername()
	{
		return $this->getState('username',false);
	}
	
	/**
	 * Gets the user's roles.
	 * 
	 * @return array
	 */
	public function getRoles()
	{
		return $this->getState('roles',array());
	}
	
	/**
	 * Gets the url to the user's profile.
	 * 
	 * @return string
	 */
	public function getUrl()
	{
		return $this->getState('url','');
	}
	
	/**
	 * Gets the URL for the user's avatar.
	 * 
	 * @return string
	 */
	public function getAvatarUrl()
	{
		return $this->getState('avatar','');
	}
	
	/**
	 * Checks whether the user has access to the page.
	 */
	public function checkAccess($operation,$params=array())
	{
		// ? represents anonymous users
		if($operation == '?')
		{
			return empty($this->id);
		}
		
		// @ represents authenticated users
		if($operation == '@')
		{
			return !empty($this->id);
		}
		
		// * represents all users
		if($operation == '*')
		{
			return true;
		}
		
		// check for a specific role
		$roles = $this->getState('roles');
		return !empty($roles[$operation]);
	}
	
}