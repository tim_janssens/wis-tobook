<?php 

/**
 * Utility class, contains various methods that are useful throughout the application.
 */
class Util
{
	/**
	 * Maximum length of the URL SEO Slugs
	 *  
	 * @var int
	 */
	const MAX_SLUG_LENGTH = 50;
	
	/**
	 * Function to create a HTML seo slug from a title.
	 *
	 * @return string The slug string
	 */
	public static function slug($title)
	{
		// Change spaces to dash
		$ret = str_replace(' ', '-', $title);
	
		// Strip all weird characters
		$ret = preg_replace("/[^a-zA-Z0-9_.-]/","",$ret);
	
		// Trimming...
		$ret = trim(substr($ret, 0, self::MAX_SLUG_LENGTH));
	
		// And Lowercase
		return strtolower($ret);
	}
	
	/**
	 * Shortcut to create a URL
	 * 
	 * @param string $route The route for the URL
	 *  (eg. <code>'controller/action'</code>)
	 * @param array $params Query string parameters.
	 *  You can also use # as a key for the anchor point.
	 * @param string $ampersand Symbol to use for the ampersand (default &)
	 * 
	 * @see CUrlManager::createUrl()
	 * 
	 * @return string The URL
	 */
	public static function url($route,$params=array(),$ampersand='&')
	{
		return Yii::app()->urlManager->createUrl($route,$params,$ampersand);
	}
	
	/**
	 * Cleans entered HTML against XSS etc.
	 *
	 * @param string $text The text to purify
	 * 
	 * @return string The cleaned text. Can be an empty string.
	 */
	public static function purify($text)
	{
		$p = new CHtmlPurifier();
		$p->setOptions(array(
			'HTML.Allowed' => 'a[href|target],b,i,u,s,p,strong,em,ins,del,br,span[style]',
			'CSS.AllowedProperties' => array('color','text-decoration'),
		));
		return $p->purify($text); // Purify
	}
	
	/**
	 * Encrypts a password
	 *
	 * @param string $password The password to encrypt.
	 * 
	 * @throws RuntimeException If the encryption fails.
	 * 
	 * @return string The hashed password.
	 */
	public static function encryptPassword($password)
	{
		require_once Yii::app()->basePath . '/vendor/Password.php';
		$ret = password_hash($password, PASSWORD_DEFAULT);
		if(!$ret)
		{
			throw new RuntimeException('Password Encryption Failed');
		}
		return $ret;
	}
	
	/**
	 * Checks whether the password for a user is correct
	 *
	 * @param string $password Password to verify
	 * @param string $hash The hashed password to match
	 * 
	 * @return boolean If the password matches
	 */
	public static function verifyPassword($password, $hash)
	{
		require_once Yii::app()->basePath . '/vendor/Password.php';
		return password_verify($password, $hash);
	}
	
	/**
	 * Gets a standard stream context
	 * 
	 * This context sets a timeout for requests to the Google APIs
	 *  (and any other requests)
	 * 
	 * @return resource Stream Context Resource
	 */
	public static function context()
	{
		return stream_context_create(array(
			'http' => array(
				'timeout' => 10, // Timout after 10 seconds
			),
		));
	}
	
	/**
	 * Converts an ISBN-10 number to ISBN-13
	 * 
	 * @param string $isbn10 The ISBN-10 to convert to ISBN-13. Should
	 *  only contain digits. If you pass an ISBN-13, it will be returned
	 *  as is. In case of errors, false is returned. 
	 */
	public static function convertToIsbn13($isbn10)
	{
		if(($l = strlen($isbn10)) == 13)
		{
			return $isbn10;
		}
		if($l == 10)
		{
			$isbn10 = substr($isbn10,0,-1);
		}
		elseif($l != 9 || preg_match('/[^0-9X]/', $isbn10))
		{
			return false;
		}
		
		$tmp = '978' . $isbn10;
		$check = self::calculateIsbnChecksum($tmp);
		
		return $tmp . $check;
	}
		
	/**
	 * Calculates the ISBN checksum
	 *
	 * @param string $isbn ISBN, must be a string containing only digits
	 *  with a length of either 9, 10 (ISBN10) 12, or 13 (ISBN13) characters.
	 *  If the length is 10 or 13, the 9 or 12 relevant characters for the checksum
	 *  will be extracted automatically.
	 *  
	 * @return string The checksum character (0-9 or X), or false for failure.
	 */
	public static function calculateIsbnChecksum($isbn)
	{	
		$extract = null;
		$isbn13 = false;
		switch(strlen($isbn))
		{
			case 12: // ISBN-13
				$isbn13 = true;
				/* Break ommitted */
			case 9:
				$extract = $isbn;
				break;
			case 13: // ISBN-13
				$isbn13 = true;
				/* Break ommitted */
			case 10: // ISBN-10
				$extract = substr($isbn,0,-1);
				break;
			default:
				return false;
		}
		
		// Check if we can calculate the checksum
		if(preg_match('/\D/',$extract))
		{
			return false;
		}
		
		$res = 0;
		if($isbn13)
		{
			for($i = 0; $i < 12; $i++)
			{
				$res += $extract[$i] * (($i % 2) ? 3 : 1);
			}
			$res = $res % 10;
			if($res != 0)
			{
				$res = 10 - $res;
			}
			else
			{
				$res = 0;
			}
		}
		else
		{
			for($i = 0; $i < 9; $i++)
			{
				$res += $extract[$i] * ($i+1);
			}
			$res = $res % 11;
		}
		
		return $res == 10 ? 'X' : (string)$res;
	}
}