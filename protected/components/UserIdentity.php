<?php

/**
 * UserIdentity represents the data needed to identify a user.
 * 
 * It contains the authentication method that checks if the provided
 *  data can identify the user.
 */
class UserIdentity extends CUserIdentity
{
	protected $user_id;
	protected $fullname;
	
	/**
	 * Authenticates a user.
	 * 
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		/* @var $user User */
		$user = User::model()->with('location')->findByAttributes(array(
			'username'=>$this->username
		));
		
		if(!$user)
		{
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		elseif(!Util::verifyPassword($this->password,$user->passwd))
		{
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		else
		{
			$this->onAuthenticationSuccess($user);
			$this->errorCode=self::ERROR_NONE;
		}
		
		return !$this->errorCode;
	}
	
	/**
	 * Extracts some more data from the user if the authentication is successful
	 * 
	 * @param User $user User to extract data from
	 */
	protected function onAuthenticationSuccess(User $user)
	{
		$this->user_id = $user->user_id;
		$this->fullname = $user->name;
		$this->setState('roles', $user->roles);
		$this->setState('profile', $user->getUrl());
		$this->setState('avatar', $user->getAvatarUrl());
		$this->setState('username', $user->username);
		
		if(!empty($user->location_id))
		{
			$this->setState('address', $user->location->address);
			$this->setState('latLng', array(
				'lat' => $user->location->lat,
				'lng' => $user->location->lng,
			));
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CUserIdentity::getId()
	 */
	public function getId()
	{
		return $this->user_id;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CUserIdentity::getName()
	 */
	public function getName()
	{
		return $this->fullname;
	}
}