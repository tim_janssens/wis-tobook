<?php
/**
 * Controller is the customized base controller class.
 * 
 * All controller classes for this application should extend from this base class.
 */
abstract class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	/**
	 * Extra button for the modal (if any), other than the close button.
	 * 
	 * @var string|false Text to appear on the button, or false for no button.
	 */
	protected $extraModalButton = false;
	
	/**
	 * Additional filters to load from the Yii Framework.
	 * 
	 * We make use of the accessControl filters to define role based acces rules.
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			// Database errors
			if(isset($error['errorCode']))
			{
				if($error['errorCode'] == 1049)
				{
					$error['message'] 
						= 'Connection to database server OK. Could not find the correct database.'
								. ' Check the installation manual for help.';
				}
				elseif($error['errorCode'] == 1045)
				{
					$error['message'] 
						= 'Could not connect to the database (bad username/password).'
								. ' Check the installation manual for help.';
				}
			}
			
			if(Yii::app()->request->isAjaxRequest)
			{
				echo $error['message'];
			}
			else
			{
				$this->render('error', $error);
			}
		}
	}
	
	/**
	 * Sets the extra button for the Modal (only if viewed through Modal)
	 */
	public function setExtraModalButton($buttonText)
	{
		$this->extraModalButton = $buttonText;
	}
	
	/**
	 * Loads a CSS file
	 * 
	 * @param string $name Name of the CSS file to load
	 */
	public function loadCss($name)
	{
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->baseUrl . '/css/' . $name . '.css'
		);
	}
	
	/**
	 * Loads a script file
	 * 
	 * The script file should be in the js/tobook/<name of controller>
	 * 
	 * @param string $name Name of the script to load
	 */
	public function loadScript($name)
	{
		Yii::app()->clientScript->registerScriptFile(
			Yii::app()->baseUrl . '/js/' . $this->id . '/' . $name . '.js'
		);
	}
	
	/**
	 * Loads a jQuery script file
	 * 
	 * @param string $name Name of the jQuery script to load
	 */
	public function loadJQuery($name)
	{
		Yii::app()->clientScript->registerScriptFile(
			Yii::app()->baseUrl . '/js/jquery/jquery.' . $name . '.js'
		);
	}
	
	/**
	 * Checks whether the request was made through AJAX
	 *
	 * @return boolean
	 */
	public function isAjax()
	{
		return Yii::app()->request->isAjaxRequest;
	}
	
	/**
	 * AJAX validate a single form attribute.
	 * 
	 * The name and value of the attribute to be validated should be sent
	 *  through POST as 'ajax_validate_attribute' and 'ajax_validate_value'
	 *  respectively.
	 * 
	 * @param CModel $model The Model for which the attribute should be validated.
	 */
	public function ajaxValidate(CModel $model)
	{	
		if(!$this->isAjax() || !isset($_GET['ajax_validate_attribute']))
		{
			return;
		}
		
		$model->setAttributes(array(
			$_GET['ajax_validate_attribute'] => $_GET['ajax_validate_value']
		));
		
		if(!$model->validate(array($_GET['ajax_validate_attribute'])))
		{
			$this->renderAjaxValidationError($model);
		}
		
		$this->renderAjaxSuccess();
	}
	
	/**
	 * Prints the ajax errors in json format
	 * 
	 * @param CModel $model The model to print the validation errors for.
	 */
	public function renderAjaxValidationError(CModel $model, $returnData = array())
	{
		$errors = array();
		
		foreach($model->getErrors() as $attribute => $errs)
		{
			$errors[CHtml::activeId($model,$attribute)] = $errs[0];
		}
		
		$ret = $returnData + array(
			'message' => 'Form Validation Failed',
			'validationErrors' => $errors,
		);
		
		$this->renderAjaxError($ret);
	}
	
	/**
	 * Renders an AJAX redirect
	 * 
	 * This is a standardized reply in the ToBook application. It is
	 *  recognized by e.g. the Forms component.
	 * 
	 * @param string $redirectUrl The URL to redirect to
	 */
	public function renderAjaxRedirect($redirectUrl)
	{	
		$this->renderAjax(array(
			'status' => 'redirect',
			'redirectUrl' => $redirectUrl,
		));
	}
	
	/**
	 * Renders an AJAX modal
	 *
	 * @param string $modalContent The Content that should be shown
	 *  in the modal.
	 */
	public function renderAjaxModal($modalContent)
	{
		$this->renderAjaxSuccess(array(
			'modal' => $modalContent,
			'title' => $this->pageTitle,
			'button' => $this->extraModalButton,
		));
	}
	
	/**
	 * Sends an AJAX success result
	 * 
	 * @param array $returnData Additional data to return
	 */
	public function renderAjaxSuccess(array $returnData = array())
	{
		$returnData['status'] = 'success';
		$this->renderAjax($returnData);
	}
	
	/**
	 * Sends an AJAX error result
	 *
	 * @param array $returnData Additional data to return
	 */
	public function renderAjaxError(array $returnData = array())
	{
		$returnData['status'] = 'error';
		$this->renderAjax($returnData);
	}
	
	/**
	 * Sends a generic AJAX reply
	 * 
	 * @param array $returnData Data to return
	 */
	public function renderAjax(array $returnData = array())
	{		
		$this->layout = false;
		
		if(ob_get_length())
		{
			// Clean anything in the output buffer
			ob_clean();
		}
		
		// Send reply as JSON
		header('Content-type: application/json');
		echo CJSON::encode($returnData);
		Yii::app()->end();
	}
	
	/**
	 * Renders a no permission page
	 * 
	 * @throws CHttpException
	 */
	public function renderNoPermission($message=false)
	{
		if(!$message)
		{
			$message = "You are not authorized to perform this action";
		}
		throw new CHttpException(403,$message);
	}
	
	/**
	 * Redirect to a specific URL.
	 * 
	 * If this is an AJAX request, will render an AJAX redirect.
	 */
	public function redirect($url,$terminate=true,$statusCode=302)
	{
		if($this->isAjax())
		{
			$this->renderAjaxRedirect($url);
		}
		parent::redirect($url,$terminate,$statusCode);
	}
	
	/**
	 * Things to do before a page is rendered (with the render function)
	 */
	public function beforeRender($view)
	{
		// If this is an AJAX request
		if($this->isAjax())
		{
			// Is it for a modal?
			if(isset($_GET['modal']))
			{
				// Universal Modal System
				$this->layout = 'modal';
			}
			else
			{
				// We should never render a page normally with AJAX
				$this->renderAjaxError(array(
					'message'=>'Controller Action does not support AJAX',
				));
			}
		}

		// Automatically load the JS file by the name of the view
		$jsFile = '/js/' . $this->id . '/' . $view . '.js';
		if(file_exists(Yii::getPathOfAlias('webroot') . $jsFile))
		{
			Yii::log('Autoloaded JS: ' . $jsFile);
			Yii::app()->clientScript->registerScriptFile(
				Yii::app()->baseUrl . $jsFile
			);
		}
		
		// Automatically load the CSS file by the name of the controller
		$cssFile = '/css/' . $this->id . '.css';
		if(file_exists(Yii::getPathOfAlias('webroot') . $cssFile))
		{
			Yii::log('Autoloaded CSS: ' . $cssFile);
			Yii::app()->clientScript->registerCssFile(
				Yii::app()->baseUrl . $cssFile
			);
		}
		
		// Yes, continue rendering
		return true;
	}
	
	/**
	 * Initializes jQuery Datatables.
	 * 
	 * Loads the JavaScript and CSS needed for DataTables to work properly.
	 */
	public function initDataTables()
	{
		Yii::app()->clientScript->registerScriptFile(
			Yii::app()->baseUrl . '/js/jquery/jquery.dataTables.min.js'
		);
		Yii::app()->clientScript->registerCssFile(
			Yii::app()->baseUrl . '/css/dataTables.css'
		);
	}
	
	/**
	 * Initializes the Google Maps JavaScripts
	 * 
	 * Used on pages that use Google Maps.
	 */
	public function initGoogleMaps()
	{
		Yii::app()->clientScript->registerScriptFile(
			'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places'
		);
	}
}