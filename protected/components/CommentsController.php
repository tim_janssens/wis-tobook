<?php

/**
 * Extended Controller that makes use of the Universal Comments System
 */
abstract class CommentsController extends Controller
{
	/**
	 * Gets the comments configuration.
	 * 
	 * You also can use this function to do additional checks, like for
	 *  example trying to comment on a non-existing item. 
	 * 
	 * @return array An array should be returned with the following data:<ul>
	 *  <li><b>itemId:</b> ID of the relevant item</li>
	 *  <li><b>itemType:</b> Type of the item (e.g. 'book','event',...)</li>
	 *  <li><b>returnUrl:</b> The URL to return to if the comment was saved (used
	 *   for non-ajax).</li></ul>
	 */
	abstract protected function beforeComment();
	
	/**
	 * Checks if the current user can add comments.
	 *
	 * Should be overridden if needed.
	 * 
	 * @return boolean
	 */
	public function canComment()
	{
		return !Yii::app()->user->isGuest;
	}
	
	/**
	 * Loads the comments for a specific type and id
	 *
	 * @param string $type The Type to load comments for (e.g. 'book','event')
	 * @param int $id The ID to load comments for.
	 *
	 * @return CActiveDataProvider A data provider which can be passed
	 *  to the CListView widget.
	 */
	public function loadComments($type,$id)
	{
		return new CActiveDataProvider('Comment',array(
			'criteria' => array(
				'condition'=>"item_type = :item_type AND item_id = :item_id",
				'params'=>array(
					':item_type'=>$type,
					':item_id'=>$id,
				),
			),
			'pagination'=>array(
				'pageSize'=>99999,
			),
		));
	}
	
	/**
	 * Function to execute when adding a new comment.
	 * 
	 * On succes, this function should return a parsed comment if the
	 *  request was made by AJAX, or redirect somewhere otherwise. In case
	 *  of errors, the comments form page should be rendered, or an error
	 *  should be sent back in case of AJAX.
	 */
	public function actionComment()
	{
		$config = $this->beforeComment();
		
		if(!isset($_POST['CommentForm']))
		{
			$this->redirect($config['returnUrl']);	
		}
		
		$form = new CommentForm();
		$form->attributes = $_POST['CommentForm'];
				
		if($form->validate())
		{
			$c = new Comment();
			$c->comment_text = $form->comment;
			$c->user_id = Yii::app()->user->id;
			$c->item_id = $config['itemId'];
			$c->item_type = $config['itemType'];
			$c->save();
			
			if($this->isAjax())
			{
				$this->renderAjaxSuccess(array(
					'comment' => $this->renderPartial('/comment',array(
						'data' => $c,
					), true),
				));
			}
			else 
			{
				$this->redirect($config['returnUrl'] . "#comment-" . $c->comment_id);
			}
		}
		elseif($this->isAjax())
		{
			$this->renderAjaxValidationError($form,	array(
				'message' => 'The comment contained invalid characters and could not be saved',
				'comment_text' => $form->comment,
			));
		}
		
		// Fallback in case of no AJAX....
		$this->render('/comment-form',array(
			'model' => $form,
			'formAction' => Util::url($this->id.'/comment',array(
				'id'=>$config['itemId']
			)),
		));
	}
}