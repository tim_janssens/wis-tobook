<?php

/**
 * Behavior to automatically serialize/unserialize arrays for the database.
 * 
 * This is needed since it is not possible to store an array directly in the database.
 */
class AutoSerializeBehavior extends CActiveRecordBehavior {
	
	/**
	 * List of attributes that should be auto (un)serialized.
	 * 
	 * @var array
	 */
	public $attributes = array();
	
	/**
	 * Temporary cache of unserialized data.
	 * 
	 * @var array
	 */
	protected $cache;
	
	/**
	 * Things to do before the record is saved in the database.
	 * 
	 * In our case, we serialize the attributes that have been specified.
	 *  We store the unserialized version in a temporary cache.
	 * 
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::beforeSave()
	 */
	public function beforeSave($event)
	{
		ob_get_clean();
		$this->cache = array();
		if(count($this->attributes))
		{
			foreach($this->attributes as $attribute)
			{
				$oldVal = $this->owner->getAttribute($attribute);
				$this->cache[$attribute] = $oldVal;
				
				$this->owner->setAttribute($attribute, serialize($oldVal));
			}
		}
	}
	
	/**
	 * Things to do after the record is saved to the database.
	 * 
	 * We restore the unserialized version to the object from our cache.
	 * 
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::afterSave()
	 */
	public function afterSave($event)
	{
		if(count($this->cache))
		{
			foreach($this->cache as $k => $v)
			{
				$this->owner->setAttribute($k, $v);
			}
			$this->cache = array();
		}
		elseif(count($this->attributes))
		{
			foreach($this->attributes as $attribute)
			{
				$oldVal = $this->owner->getAttribute($attribute);
				$this->owner->setAttribute($attribute, @unserialize($oldVal));
			}
		}
	}
	
	/**
	 * Things to do when a record is loaded from the database through find methods.
	 * 
	 * In our case, we unserialize the specified attributes.
	 * 
	 * (non-PHPdoc)
	 * @see CActiveRecordBehavior::afterFind()
	 */
	public function afterFind($event)
	{
		if(count($this->attributes))
		{
			foreach($this->attributes as $attribute)
			{
				$oldVal = $this->owner->getAttribute($attribute);
				$this->owner->setAttribute($attribute, @unserialize($oldVal));
			}
		}
	}
}