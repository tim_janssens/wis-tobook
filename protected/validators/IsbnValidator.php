<?php

/**
 * Validator that validates names.
 * 
 * This should be used for first and last names.
 */
class IsbnValidator extends CValidator
{
	/**
	 * The message to show when there is an error.
	 * 
	 * @var string
	 */
	public $message = "ISBN is invalid";
		
	/**
	 * (non-PHPdoc)
	 * @see CValidator::validateAttribute()
	 */
	protected function validateAttribute($object,$attribute)
	{
		$value = preg_replace('/[^0-9X]/','',$object->$attribute);
		if(!$value)
		{
			$this->addError($object, $attribute, $this->message . ' (bad chars)');
		}
		else
		{
			$len = strlen($value);
			if($len != 10 && $len != 13)
			{
				$this->addError($object, $attribute, $this->message . ' (bad length)');
			}
			
			$check = Util::calculateIsbnChecksum($value);
			if($check != substr($value,-1))
			{
				$this->addError($object, $attribute, $this->message . ' (bad checksum)');
			}
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object,$attribute)
	{
		return "var tmp = value.replace(/[^0-9X]/,'');
if(!tmp || (tmp.length != 10 && tmp.length != 13) ) {
	messages.push('".$this->message."');
}
";
	}
}