<?php

/**
 * Validator that validates names.
 * 
 * This should be used for first and last names.
 */
class NameValidator extends CValidator
{
	/**
	 * The message to show when there is an error.
	 * 
	 * @var string
	 */
	public $message = "Please enter a valid name";
	
	/**
	 * The regex that matches the invalid characters.
	 * 
	 * @var string
	 */
	protected $matchRegex = '"(){}<>#%^$&;~=`\,:.\[\]\|\+\*\/\n\t\r²³§°£€µ';
	
	/**
	 * (non-PHPdoc)
	 * @see CValidator::validateAttribute()
	 */
	protected function validateAttribute($object,$attribute)
	{
		$value = $object->$attribute;
		if(preg_match('/['.$this->matchRegex.']/', $value))
		{
			$this->addError($object, $attribute, $this->message);
		}
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CValidator::clientValidateAttribute()
	 */
	public function clientValidateAttribute($object,$attribute)
	{
		return "
if(jQuery.trim(value).match(/[" . $this->matchRegex . "]/)){
	messages.push(".CJSON::encode($this->message).");
}
";
	}
}