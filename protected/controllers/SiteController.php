<?php

/**
 * Controller that handles general site pages.
 */
class SiteController extends Controller
{		
	/**
	 * Access rules.
	 * 
	 * Determines what actions can be performed by what users.
	 * 
	 * @see WebUser::checkAccess()
	 */
	public function accessRules()
	{
		return array(
			array('deny',
				'actions' => array('preferences','avatarPrefs'),
				'users' => array('?'),
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 *  when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// Top 5 books     
		$new = Book::model()->with('users','genre')->findAll(array(
			'order' => 'book_id DESC',
			'limit' => 5
		));

		// Books count       
		$totalBooks = Book::model()->count(); 

		// Author count
		$totalAuthors = Yii::app()->db->createCommand("
			SELECT COUNT(DISTINCT author) FROM book
		")->queryScalar();

	    // The top 5 rated books
	    $top = Book::model()->findAll(array(
			'alias' => 'b',
			'select' => array(
				'*', '(SELECT AVG(rating) FROM rating r WHERE r.book_id = b.book_id) AS a'
			),
			'order' => 'a DESC',
			'limit' => 5,
		));
	    
	    // Render page
		$this->render('index', array(
			'totalBooks' => $totalBooks, 
			'totalAuthors' => $totalAuthors,
			'new' => $new,
			'top' => $top,
	    ));
	}
	
	/**
	 * Register an account
	 */
	public function actionRegister()
	{
		// Are we logged in maybe?
		if(!Yii::app()->user->isGuest)
		{
			$this->redirect(Yii::app()->homeUrl);
		}
		
		$form = new RegisterForm();
		
		// Ajax validate the username
		$this->ajaxValidate($form);
		
		// Save?
		if(isset($_POST['RegisterForm']))
		{
			$form->attributes=$_POST['RegisterForm'];
			if($form->validate())
			{
				// Create user
				$user = new User();
				$user->username = $form->username;
				$user->passwd = Util::encryptPassword($form->password);
				$user->email = $form->email;
				
				$user->first_name = $form->first_name ? $form->first_name : '';
				$user->last_name = $form->last_name ? $form->last_name : '';
				
				$user->register_date = time();
				$user->addRole('user');

				$user->save();
				
				// Attempt automatic login
				$identity = new UserIdentity($user->username, $user->passwd);
				$identity->authenticate();
				
				// Go to user page
				$this->redirect($user->url);
			}
			elseif($this->isAjax())
			{
				$this->renderAjaxValidationError($form);
			}
		}
		
		$this->render('register',array('model'=>$form));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		// Already logged in?
		if(!Yii::app()->user->isGuest)
		{
			$this->redirect(Yii::app()->homeUrl);
		}
		
		$form = new LoginForm();

		// Collect user input data
		if(isset($_POST['LoginForm']))
		{
			$form->attributes = $_POST['LoginForm'];
			
			// Validate user input and redirect to the previous page if valid
			if($form->validate() && $form->login())
			{
				if(!empty($_POST['returnTo']))
				{
					$this->redirect($_POST['returnTo']);
				}
				else
				{
					$this->redirect(Yii::app()->user->returnUrl);
				}
			}
		}
		
		// Display the login form
		$this->render('login',array('model'=>$form));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		// Are we logged in?
		if(!Yii::app()->user->isGuest)
		{
			// Then logout
			Yii::app()->user->logout();
		}
		
		// Back to the home page
		$this->redirect(Yii::app()->homeUrl);
	}
	
	/**
	 * Site preferences
	 */
	public function actionPreferences()
	{	
		/* @var $u User */
		$u = User::model()->with('location')->findByPk(Yii::app()->user->id);
	
		$mainForm = new PreferencesForm();
		$passForm = new PasswordForm();
	
		/* PREFERENCES FORM */
		if(isset($_POST['PreferencesForm']))
		{
			$mainForm->attributes = $_POST['PreferencesForm'];
			if($mainForm->validate())
			{
				// Use transaction since we are updating both user and location
				$t = Yii::app()->db->beginTransaction();
				try {
					// Update location
					if($mainForm->address)
					{
						$l = Location::model()->findByHash(md5($mainForm->address));
						if(!$l)
						{
							$l = new Location();
							$l->address = $mainForm->address;
							$l->save();
						}
						$u->location_id = $l->location_id;
					}
					else
					{
						$u->location_id = null;
					}
					
					// Update other stuff
					$u->introduction = $mainForm->introduction;
					$u->first_name = $mainForm->first_name;
					$u->last_name = $mainForm->last_name;
					$u->email = $mainForm->email;
					
					// Save user
					$u->save();
					
					$t->commit();
				}
				catch(CDbException $e)
				{
					$t->rollback();
					throw $e;
				}
				
				// Update the session data
				Yii::app()->user->setState('address', (empty($u->location_id) ? false : $l->address));
				Yii::app()->user->setState('latLng', (empty($u->location_id) ? false : array(
					'lat' => $l->lat,
					'lng' => $l->lng,
				)));
	
				// Back to the profile
				$this->redirect($u->url);
			}
			elseif($this->isAjax())
			{
				$this->renderAjaxValidationError($mainForm);
			}
		}
		/* PASSWORD FORM */
		elseif(isset($_POST['PasswordForm']))
		{
			$passForm->attributes = $_POST['PasswordForm'];
			$passForm->setUser($u);
			if($passForm->validate())
			{
				/* @var $u User */
				$u->passwd = Util::encryptPassword($passForm->password);
				$u->save();
	
				$this->redirect($u->url);
			}
			elseif($this->isAjax())
			{
				$this->renderAjaxValidationError($passForm);
			}
		}
		else
		{
			// Extract user to display the form
			$mainForm->first_name = $u->first_name;
			$mainForm->last_name = $u->last_name;
			$mainForm->introduction = $u->introduction;
			$mainForm->email = $u->email;
			$mainForm->address = empty($u->location_id) ? '' : $u->location->address;
		}	
		
		// Render the preferences page
		$this->render('preferences',array('model'=>$mainForm,'passModel'=>$passForm));
	}
	
	/**
	 * Change avatar preferences
	 */
	public function actionAvatarPrefs()
	{
		$this->pageTitle = 'Avatar Preferences';
		
		$form = new AvatarForm();
		
		// Save?
		if(isset($_POST['AvatarForm']))
		{
			$form->attributes = $_POST['AvatarForm'];
			
			// Upload Avatar
			if($form->uploadAvatar())
			{
				/* @var $u User */
				$u = User::model()->findByPk(Yii::app()->user->id);
				$u->avatar_date = time();
				$u->save();
				
				// Update avatar in session
				Yii::app()->user->setState('avatar', $u->getAvatarUrl());
				
				// Go to profile page
				$this->redirect($u->url);
			}
		}
		
		// Render preferences page
		$this->render('avatar-prefs',array('model'=>$form));
	}
}