<?php

/**
 * Controller that handles users.
 */
class UserController extends Controller
{
	/**
	 * View a user profile page.
	 * 
	 * Not that while this page shows datatables, it does not handle the
	 *  AJAX data for thos tables. See the functions below for that.
	 */
	public function actionView()
	{
		/* @var $u User */
		$u = $this->_getUserOrError();
		$this->pageTitle = 'Viewing profile of ' . $u->name;
		
		$t = time();
		
		// LOAD: Books			
		$books = UserBook::model()->with('book')->findAllByAttributes(array(
			'user_id' => $u->user_id,
		), array(
			'limit' => 10
		));
		
		// COUNT: Books
		$totalBooks = UserBook::model()->countByAttributes(array(
			'user_id' => $u->user_id,	
		));
		
		// LOAD: Events
		$events = Event::model()->with('location','attendees')->findAllByAttributes(array(
			'user_id' => $u->user_id,
		), new CDbCriteria(array(
			'limit' => 10,
			'offset' => 0,
			'order' => 'starttime ASC',
			'condition' => 'IF(endtime=0,starttime+86399,endtime) > :time',
			'params' => array(
				':time' => $t,
			),
		)));
		
		// COUNT: Events
		$totalEvents = Event::model()->countByAttributes(array(
			'user_id' => $u->user_id,
		));
		
		// COUNT: Currently visible events
		$activeEvents = Event::model()->countByAttributes(array(
			'user_id' => $u->user_id,
		), array(
			'condition' => 'IF(endtime=0,starttime+86399,endtime) > :time',
			'params' => array(
				':time' => $t,
			),
		));
		
		// Render profile page
		$this->render('view',array(
			'user' => $u,
			'books' => $books,
			'totalBooks' => $totalBooks,
			'events' => $events,
			'totalEvents' => $totalEvents,
			'activeEvents' => $activeEvents,
		));
	}
	
	/**
	 * Gets the events for a user (for DataTable)
	 */
	public function actionEvents()
	{	
		// Get user ID
		$userId = $_GET['id'];
		
		// Init Conditions
		$conds = new CDbCriteria();
		
		// Search (all relevant columns)
		if(!empty($_GET['sSearch']))
		{
			$search = urldecode(trim($_GET['sSearch']));
			if($search)
			{
				$conds->addSearchCondition('e.name', $search);
				$conds->addSearchCondition('l.address', $search, true, 'OR');
			}
		}
		
		// Filter by the current user
		$conds->addColumnCondition(array(
			'user_id' => $userId,
		));
		
		// Filter by type (upcoming, ongoing, etc)
		if(empty($_GET['event_types']))
		{
			$_GET['event_types'] = 'active';
		}
		
		switch($_GET['event_types'])
		{
			case 'in_progress':
				$conds->addCondition(array(
					'IF(e.endtime=0,e.starttime+86399,e.endtime) > :time',
					'e.starttime < :time',
				));
				$conds->params[':time'] = time();
				break;
			case 'upcoming':
				$conds->addCondition('e.starttime > :time');
				$conds->params[':time'] = time();
				break;
			case 'active':
				$conds->addCondition(array(
					'IF(e.endtime=0,e.starttime+86399,e.endtime) > :time',
				));
				$conds->params[':time'] = time();
				break;
			case 'all':
				break;
			case 'archive':
				$conds->addCondition('IF(e.endtime=0,e.starttime+86399,e.endtime) < :time');
				$conds->params[':time'] = time();
				break;
		}
		
		// Show X results
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] != '-1')
		{
			$conds->limit = (int)$_GET['iDisplayLength'];
			
			if(!empty($_GET['iDisplayStart']))
			{
				$conds->offset = (int)$_GET['iDisplayStart'];
			}
			else
			{
				$conds->offset = 0;
			}
		}
		
		// Sorting
		$sortCol = $this->getEventSortColumn($_GET['iSortCol_0']);
		
		$sort = array(
			$sortCol . ' ' . ($_GET['sSortDir_0'] == 'asc' ? 'ASC' : 'DESC')
		);
		
		// Secondary sort
		if(!empty($_GET['iSortCol_1']))
		{
			$sort[] = $this->getEventSortColumn($_GET['iSortCol_1'] . ' ' . ($_GET['sSortDir_1'] == 'asc' ? 'ASC' : 'DESC'));
		}
		
		// Do the actual select
		$c = Yii::app()->db->createCommand()
			->select('e.event_id, e.name, e.starttime, IF(e.endtime=0,e.starttime+86399,e.endtime) as endtime, l.address, (SELECT count(*) FROM rsvp AS r WHERE r.event_id = e.event_id) AS attendees', 'SQL_CALC_FOUND_ROWS')
			->from('event e')
			->join('location l','e.location_id = l.location_id')
			->order($sort)
			->limit($conds->limit, $conds->offset);
		
		$events = null;
		if($conds->condition)
		{
			$c->where($conds->condition);
			$events = $c->queryAll(true, $conds->params);
		}
		else
		{
			$events = $c->queryAll();
		}
		
		// Get results count
		$results = (int)Yii::app()->db->createCommand("SELECT FOUND_ROWS()")->queryScalar();
		
		// Get total events from the user
		$totalCount = Event::model()->countByAttributes(array(
			'user_id' => $userId
		));
		
		// No results?
		if(!$results)
		{
			$this->renderAjax(array(
				'sEcho' => intval($_GET['sEcho']),
				'iTotalRecords' => $totalCount,
				'iTotalDisplayRecords' => 0,
				'aaData' => array(),
			));
		}
		
		$t = time();
		
		// Format results
		$ret = array();
		foreach($events as &$event)
		{
			$type = 'active';
			if($event['starttime'] > $t )
			{
				$type = 'upcoming';
			}
			elseif( $event['endtime'] < $t )
			{
				$type = 'archived';
			}
			
			$icon = '<img src="'.Yii::app()->request->baseUrl.'/images/'.$type.'.ico" alt="'.$type.'" title="'.$type.'" />';
			if( $type == 'upcoming' )
			{
				$icon = '<a href="' . Util::url(
						'event/ical', array(
								'id' => $event['event_id'],
						)
				) . '">' . $icon . '</a>';
			}
			
			$ret[] = array(
				'name' => '<a href="'
					. Util::url('event/view',array(
						'id' => $event['event_id'],
						'title' => Util::slug($event['name']),
					)) .'">' . $event['name'] . '</a>',
				'date' => strftime('%B %d, %Y', $event['starttime']),
				'location' => strtok($event['address'],','),
				'attendees' => $event['attendees'],
				'icon' => $icon,
				'type' => $type,
			);
		}
		unset($event);
		
		// Send results
		$this->renderAjax(array(
			'sEcho' => intval($_GET['sEcho']),
			'iTotalRecords' => $totalCount,
			'iTotalDisplayRecords' => $results,
			'aaData' => $ret,
		));
	}
	
	/**
	 * Gets the books for a user
	 */
	public function actionBooks()
	{	
		$userId = $_GET['id'];
		
		// Init conditions
		$conds = new CDbCriteria();
		
		// Global search
		if(!empty($_GET['sSearch']))
		{
			if($search = urldecode(trim($_GET['sSearch'])))
			{
				$conds->addSearchCondition('b.title', $search);
				$conds->addSearchCondition('b.author', $search, true, 'OR');
				$conds->addSearchCondition('g.genre', $search, true, 'OR');
		
				$isbnSearch = preg_replace('/[^0-9]/', '', $search);
				$len = strlen($isbnSearch);
				if($len == 10 || $len == 13)
				{
					$conds->addSearchCondition('b.isbn', $isbnSearch, true, 'OR');
				}
			}
		}
		
		// Filter by current user
		$conds->addColumnCondition(array(
			'user_id' => $userId,
		));
		
		// Filter by status
		if(!empty($_GET['book_types']))
		{
			switch($_GET['book_types'])
			{
				case 'all':
				default:
					break;
				case 'wishlist':
					// Break omitted
				case 'progress':
					// Break omitted
				case 'finished':
					$conds->addColumnCondition(array(
						's.status' => $_GET['book_types'],
					));
					break;
			}
		}
		
		// Show X entries
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] != '-1')
		{
			$conds->limit = (int)$_GET['iDisplayLength'];
		
			if(!empty($_GET['iDisplayStart']))
			{
				$conds->offset = (int)$_GET['iDisplayStart'];
			}
			else
			{
				$conds->offset = 0;
			}
		}
		
		// Sorting
		$sortCol = $this->getBookSortColumn($_GET['iSortCol_0']);
		
		$sort = array(
				$sortCol . ' ' . ($_GET['sSortDir_0'] == 'asc' ? 'ASC' : 'DESC')
		);
		
		// Secondary sort
		if(!empty($_GET['iSortCol_1']))
		{
			$sort[] = $this->getSortColumn($_GET['iSortCol_1'] . ' ' . ($_GET['sSortDir_1'] == 'asc' ? 'ASC' : 'DESC'));
		}
		
		// Do the actual select from the database
		$c = Yii::app()->db->createCommand()
			->select('b.book_id, b.title, b.author, b.isbn, s.status', 'SQL_CALC_FOUND_ROWS')
			->from('user_book s')
			->join('book b','s.book_id = b.book_id')
			->order($sort)
			->limit($conds->limit, $conds->offset);
		
		$books = null;
		if($conds->condition)
		{
			$c->where($conds->condition);
			$books = $c->queryAll(true, $conds->params);
		}
		else
		{
			$books = $c->queryAll();
		}
		
		// Get result count
		$results = (int)Yii::app()->db->createCommand("SELECT FOUND_ROWS()")->queryScalar();
		
		// Total books in database
		$totalCount = UserBook::model()->countByAttributes(array(
			'user_id' => $userId,
		));
		
		// No results?
		if(!$results)
		{
			$this->renderAjax(array(
				'sEcho' => intval($_GET['sEcho']),
				'iTotalRecords' => $totalCount,
				'iTotalDisplayRecords' => 0,
				'aaData' => array(),
			));
		}
		
		// Format results
		$ret = array();
		foreach($books as &$book)
		{
			$status = 'On wishlist';
			switch($book['status'])
			{
				case 'wishlist':
					break;
				case 'progress':
					$status = 'Reading';
					break;
				case 'finished':
					$status = 'Finished';
					break;
			}
			
			$ret[] = array(
				'image' => '<a href="javascript:void(0);" class="show_book_cover" data-isbn="'.$book['isbn'].'">
	<img src="' . Yii::app()->request->baseUrl . '/images/book.ico" alt="">
</a>',
				'title' => '<a href="'
				. Util::url('book/view',array(
						'id' => $book['book_id'],
						'title' => Util::slug($book['title']),
				)) .'">' . $book['title'] . '</a>',
				'author' => $book['author'],
				'status' => $status,
			);
		}
		unset($book);
		
		// Send results
		$this->renderAjax(array(
			'sEcho' => intval($_GET['sEcho']),
			'iTotalRecords' => $totalCount,
			'iTotalDisplayRecords' => $results,
			'aaData' => $ret,
		));
	}
	
	/**
	 * Gets the sorting column from the number (events datatable)
	 * 
	 * @param int $colNumber number of the column to be sorted in the table
	 * 
	 * @return string name of the column in the database
	 */
	protected function getEventSortColumn($colNumber)
	{
		switch($colNumber)
		{
			case 1:
				return 'e.name';
			case 2:
				return 'e.starttime';
			case 3:
				return 'l.address';
			case 4:
				return 'attendees';
		}
	}
	
	/**
	 * Gets the sorting column from the number (books datatable)
	 *
	 * @param int $colNumber number of the column to be sorted in the table
	 * 
	 * @return string name of the column in the database
	 */
	protected function getBookSortColumn($colNumber)
	{
		switch($colNumber)
		{
			case 1:
				return 'b.title';
			case 2:
				return 'b.author';
			case 3:
				return 's.status';
		}
	}
	
	/**
	 * Gets the user from the ID provided in the URL.
	 * 
	 * If the user is not found, an error page is shown.
	 * 
	 * @return User The user (if no errors occurred).
	 */
	protected function _getUserOrError()
	{
		$user = User::model()
			->findByPk((int)Yii::app()->getRequest()->getQuery('id'));
		
		if(!$user)
		{
			$this->render('user_notfound');
			Yii::app()->end();
		}
		return $user;
	}
}