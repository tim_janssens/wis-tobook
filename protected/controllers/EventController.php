<?php

/**
 * Controller that handles everything related to one specific event.
 */
class EventController extends CommentsController
{
	/**
	 * Access Rules list.
	 * 
	 * Determines what actions can be done by what users.
	 * 
	 * @see WebUser::checkAccess()
	 */
	public function accessRules()
	{
		return array(
			array('deny',
				'users'	=> array('?'),
				'actions' => array('edit','new','rsvp','comment')
			),
		);
	}
		
	/**
	 * Index action: Not used, redirects to events controller.
	 * 
	 * Index is called when no identifier and action is given in the URL.
	 *  (e.g. just "/event")
	 */
	public function actionIndex()
	{
		$this->redirect('events');
	}
	
	/**
	 * Action to view the page of an event.
	 */
	public function actionView()
	{
		$event = $this->_getEventOrError();
		
		$this->pageTitle = 'Event: ' . $event->name;
		
		$viewParams = array(
			'event' => $event,
			'comments' => $this->loadComments('event', $event->event_id), // Preload the comments of the event
		);
		
		// Preload comment form
		$viewParams['model'] = ($this->canComment() ? new CommentForm() : null);
		
		$this->render('view',$viewParams);
	}
	
	/**
	 * Action to create a new event.
	 */
	public function actionNew()
	{
		$this->pageTitle = 'Create an Event';
		
		$form = new EventForm();

		// Save?
		if(isset($_POST['EventForm']))
		{
			$form->setAttributes($_POST['EventForm']);
			if($form->validate())
			{
				// Note: we use a transaction here as we save both location and event
				$t = Yii::app()->db->beginTransaction();
				try
				{
					// Create event
					$event = new Event();
					
					$event->user_id = Yii::app()->user->id;
					
					$event->extractForm($form);
					$event->save();
					$t->commit();
				}
				catch(CDbException $e)
				{
					$t->rollback();
					throw $e;
				}
				
				// Redirect to the event page
				$this->redirect($event->getUrl());
			}
			elseif($this->isAjax())
			{
				$this->renderAjaxValidationError($form);
			}
		}
		
		$this->breadcrumbs = array(
			'Events' => array('events/'),
		);
		
		$this->render('form',array('model'=>$form,'mode'=>'new'));
	}
	
	/**
	 * Action to edit an existing event.
	 */
	public function actionEdit()
	{	
		/* @var $oldEvent Event */
		$oldEvent = $this->_getEventOrError();
		
		// Can we edit it?
		if( $oldEvent->user_id != Yii::app()->user->id )
		{
			$this->renderNoPermission("You are not authorized to edit this event");
		}
		
		// Show the form
		$this->pageTitle = 'Editing event: ' . $oldEvent->name;
		
		$form = new EventForm();

		// Save?
		if(isset($_POST['EventForm']))
		{
			$form->setAttributes($_POST['EventForm']);
			if($form->validate())
			{
				// Transaction is needed since we modify multiple tables (event and location)
				$t = Yii::app()->db->beginTransaction();
				try
				{
					$oldEvent->extractForm($form);				
					$oldEvent->save();
					
					$t->commit();
				}
				catch(CDbException $e)
				{
					$t->rollback();
					throw $e;
				}
				
				$this->redirect($oldEvent->getUrl());
			}
			elseif($this->isAjax())
			{
				$this->renderAjaxValidationError($form);
			}
		}
		else
		{
			$form->extractEvent($oldEvent);
		}
		
		$this->breadcrumbs = array(
			'Events' => array('events/'),
			$oldEvent->name => array(
				'event/view',
				'id'=>$oldEvent->event_id,
				'title'=>Util::slug($oldEvent->name),
			),
		);
		$this->render('form',array('model'=>$form,'mode'=>'edit'));
	}
	
	/**
	 * Adds or removes an RSVP for the current user.
	 */
	public function actionRsvp()
	{
		$event = $this->_getEventOrError();
		
		// Trying to attend our own event?
		if($event->user_id == Yii::app()->user->id)
		{
			if($this->isAjax())
			{
				$this->renderAjaxError(array(
					'message' => 'You cannot RSVP to your own event',
				));
			}
			else
			{
				$this->redirect($event->url);
			}
		}
		
		// Load any old RSVP
		$rsvp = Rsvp::model()->findByPk(array(
			'user_id' => Yii::app()->user->id,
			'event_id' => $event->event_id,
		));
		
		if($rsvp)
		{
			// We were already RSVP-ing
			$rsvp->delete();
			
			if($this->isAjax())
			{
				$this->renderAjaxSuccess(array(
					'has_rsvp' => false,
					'rsvp' => array(
						'id' => Yii::app()->user->id,
					),
				));
			}
		}
		else
		{
			// Not yet RSVP-ing
			$rsvp = new Rsvp();
			$rsvp->user_id = Yii::app()->user->id;
			$rsvp->event_id = $event->event_id;
			$rsvp->save();
			
			if($this->isAjax())
			{
				// Send back some extra data to add to the attending list
				$this->renderAjaxSuccess(array(
					'has_rsvp' => true,
					'rsvp' => array(
						'id' => Yii::app()->user->id,
						'name' => Yii::app()->user->name,
						'avatar' => Yii::app()->user->getState('avatar'),
						'url' => Yii::app()->user->getState('url'),
					),
				));
			}
		}
		
		$this->redirect($event->url);
	}
	
	/**
	 * Delete an event
	 */
	public function actionDelete()
	{
		$e = $this->_getEventOrError();
		
		// Are we the owner of the event?
		if($e->user_id != Yii::app()->user->id)
		{
			$this->renderNoPermission('You are not authorized to delete this event');
		}
		
		// Confirmed Delete?
		if(!empty($_POST['delete_confirm']))
		{
			// Start a transaction since we are removing event and comments
			$t = Yii::app()->db->beginTransaction();
			
			try
			{
				Comment::model()->deleteAllByAttributes(array(
					'item_type' => 'event',
					'item_id' => $e->event_id,
				));
				$e->delete();
				$t->commit();
			}
			catch( CDbException $e )
			{
				$t->rollback();
				throw $e;
			}
			
			if( $this->isAjax() )
			{
				$this->renderAjaxRedirect('/events');
			}
			else 
			{
				$this->redirect(array('/events'));
			}
		}
		
		// If not confirmed: Show the confirmation form
		$this->pageTitle = 'Confirm delete: ' . $e->name;
				
		if( $this->isAjax() )
		{
			$this->renderAjaxModal(
				$this->renderPartial('delete_confirm',array('event'=>$e),true)
			);
		}
		$this->render('delete_confirm',array('event'=>$e));
	}
	
	/**
	 * Export to iCalendar
	 */
	public function actionIcal()
	{
		$event = $this->_getEventOrError();
		$this->renderPartial('ical',array('event'=>$event));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CommentsController::beforeComment()
	 */
	public function beforeComment()
	{
		$event = $this->_getEventOrError();
		
		return array(
			'itemId' => $event->event_id,
			'itemType' => 'event',
			'returnUrl' => $event->url,
		);
	}
	
	/**
	 * Gets the current event from the ID provided in the URL.
	 * 
	 * If the event is not found, an error message will be shown; else the event is returned.
	 * 
	 * @return Event The loaded event
	 */
	protected function _getEventOrError()
	{
		$event = Event::model()->with('location','user')
			->findByPk((int)Yii::app()->request->getQuery('id'));
		
		if(!$event)
		{
			throw new CHttpException(404,'The requested event could not be found');
		}
		
		return $event;
	}
}