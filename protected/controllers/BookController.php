<?php

/**
 * Controller that handles individual books.
 */
class BookController extends CommentsController
{
	/**
	 * Access Rules: Determines what actions can be executed by which users.
	 * 
	 * @see WebUser::checkAccess()
	 */
	public function accessRules() {
		return array(
			array(
				'deny',
				'users' => array('?'),
				'actions' => array('edit','new','rate','status','comment') 
			) 
		);
	}
	
	/**
	 * Index action. Does nothing, redirects to Books index.
	 * 
	 * This action is only called if there is no identifier or action in
	 *  the URL (e.g. just "/book")
	 */
	public function actionIndex()
	{
		$this->redirect('/books');
	}
	
	/**
	 * View a book
	 */
	public function actionView() {
		$book_id = $_GET["id"];
		
		/* @var $book Book */
		$book = Book::model()->with('users', 'genre')->findByPk($book_id);
		
		if (!$book) {
			throw new CHttpException(404, 'The requested book could not be found');
		}
		
		// Preload the comment form model
		$model = ($this->canComment() ? new CommentForm() : null);
		
		// Recommended books: Select books with the same genre ordered by average rating
		$recommended = Book::model()->findAll(array(
			'alias' => 'b',
			'condition' => 'book_id <> :book AND genre_id = :genre',
			'params' => array(
				':book' => $book->book_id,
				':genre' => $book->genre_id,
			),
			'select' => array(
				'*', '(SELECT AVG(rating) FROM rating r WHERE r.book_id = b.book_id) AS a'
			),
			'order' => 'a DESC',
			'limit' => 5,
		));

		// Get the statuses for the people who have the book
		$statuses = Yii::app()->db->createCommand()
			->select('ub.*')
			->from('user_book ub')
			->where('ub.book_id = :book', array(
				':book' => $book_id 
			))
			->queryAll();
		
		// Get current user's status
		$myStatus = UserBook::model()->findByAttributes(array(
			'book_id' => $book_id,
			'user_id' => Yii::app()->user->id,
		));
		
		// Get all ratings
		$ratings = Yii::app()->db->createCommand()
			->select('AVG(r.rating) as rating, COUNT(r.rating) as votes')
			->from('rating r')
			->where('r.book_id=:book', array(
				':book' => $book_id 
			))
			->queryRow();
		
		// Get the current user's rating
		$myRating = Rating::model()->findByAttributes(array(
			'book_id' => $book_id,
			'user_id' => Yii::app()->user->id,
		));
		
		if($myRating)
		{
			/* @var $myRating Rating */
			$myRating = $myRating->rating;
		}

		// Render the view
		$this->render('view', array(
			'book' => $book,
			'comments' => $this->loadComments('book', $book_id),
			'model' => $model,
			'recommended' => $recommended,
			'statuses' => $statuses,
			'rating' => $ratings,
			'myRating' => $myRating,
			'myStatus' => $myStatus,
		));
	}
	
	/**
	 * Action to create a new book.
	 */
	public function actionNew()
	{
		$this->pageTitle = 'Add a new Book';
		
		$book = Book::model();
		$form = new BookForm();
		
		// Individual field AJAX validation
		$this->ajaxValidate($form);
		
		// Form Submitted?
		if(isset($_POST['BookForm']))
		{
			$form->setAttributes($_POST['BookForm']);
			if($form->validate())
			{
				// Fetch the book from Google to see if it even exists
				if(!($gBooks = $this->_fetchGoogleBooks($form->isbn)))
				{
					// ... it didn't exist
					$form->addError('isbn', 'Book could not be found on Google Books');
					if($this->isAjax())
					{
						$this->renderAjaxValidationError($form);
					}
				}
				else
				{
					// All or nothing
					$t = Yii::app()->db->beginTransaction();
					
					try
					{
						// Create Book
						$book = new Book();
						$book->extractForm($form);
						$book->user_count = 0;
						$book->save();
					
						// My Status for the book
						if($form->status)
						{
							$u = new UserBook();
							$u->book_id = $book->book_id;
							$u->user_id = Yii::app()->user->id;
							$u->status = $form->status;
							$u->save();
						}
						
						// Google books cover cache
						$this->_cacheBookCover($book->isbn, $gBooks);
						
						$t->commit();
					}
					catch(Exception $e)
					{
						$t->rollback();
						throw $e;
					}
					
					// Go to book page
					$this->redirect($book->getUrl());
				}
			}
			elseif ($this->isAjax())
			{
				$this->renderAjaxValidationError($form);
			}
		}
		
		$this->breadcrumbs = array(
			'Books' => array(
				'books/' 
			) 
		);
		
		$this->render('form', array(
			'model' => $form 
		));
		
	}
	
	/**
	 * Action to edit an existing book.
	 */
	public function actionEdit()
	{
		/* @var $oldBook Book */
		$oldBook = $this->_getBookOrError();
		
		$book_id = $_GET["id"];
		
		// Show the form
		$this->pageTitle = 'Editing book: ' . $oldBook->title;
		
		$form = new BookForm();
		$this->ajaxValidate($form);
		
		// Save?
		if (isset($_POST ['BookForm']))
		{
			$form->setAttributes($_POST['BookForm']);
			$form->book_id = $book_id;
			if ($form->validate())
			{
				// Did we change the ISBN?
				if($reCache = ($form->isbn != $oldBook->isbn))
				{
					// Get new ISBN from Google Books
					if(!($gBooks = $this->_fetchGoogleBooks($form->isbn)))
					{
						// Book didn't exist
						$form->addError('isbn', 'Book could not be found on Google Books');
						if($this->isAjax())
						{
							$this->renderAjaxValidationError($form);
						}
						else
						{
							$this->breadcrumbs = array(
								'Books' => array(
									'books/'
								),
								$oldBook->title => array(
									'book/view',
									'id' => $oldBook->book_id,
									'title' => Util::slug($oldBook->title)
								)
							);
							
							$this->render('form', array(
								'model' => $form
							));
							
							return;
						}
					}
				}

				// Update Book
				$oldBook->extractForm($form);
				$oldBook->save();
				
				// Update my Status
				$u = UserBook::model()->findByAttributes(array(
					'book_id' => $oldBook->book_id,
					'user_id' => Yii::app()->user->id 
				));
				
				if(!$form->status)
				{
					if($u)
					{
						$u->delete();
					}
				}
				else
				{
					if(!$u)
					{
						$u = new UserBook();
						$u->book_id = $oldBook->book_id;
						$u->user_id = Yii::app()->user->id;
					}
					
					$u->status = $form->status;
					$u->save();
				}
				
				// Recache the new book cover
				if($reCache)
				{
					$this->_cacheBookCover($oldBook->isbn, $gBooks);
				}
				
				// Goto book page
				$this->redirect($oldBook->getUrl());
			}
			elseif($this->isAjax())
			{
				$this->renderAjaxValidationError($form);
			}
		}
		else
		{
			$form->extractBook($oldBook);
		}
		
		// Set breadcrumbs
		$this->breadcrumbs = array(
			'Books' => array(
				'books/'
			),
			$oldBook->title => array(
				'book/view',
				'id' => $oldBook->book_id,
				'title' => Util::slug($oldBook->title) 
			) 
		);
		
		// Render view page
		$this->render('form', array(
			'model' => $form 
		));
	}
	
	/**
	 * Rate a book
	 */
	public function actionRate()
	{
		$book = $this->_getBookOrError();
		
		// Save?
		if(isset($_POST['rating']))
		{
			/* @var $r Rating */
			$r = Rating::model()->findByAttributes(array(
				'book_id' => $book->book_id,
				'user_id' => Yii::app()->user->id,
			));
			
			$newRating = (int)$_POST['rating'];
			
			if($newRating)
			{
				// Don't mess with the select boxes
				if($newRating > 10)
				{
					$newRating = 10;
				}
				
				if(!$r)
				{
					// Add new rating
					$r = new Rating();
					$r->user_id = Yii::app()->user->id;
					$r->book_id = $book->book_id;
				}
				
				$r->rating = $newRating;
				$r->save();
				
				if($this->isAjax())
				{
					$ret = Yii::app()->db->createCommand()
						->select('AVG(r.rating) as avg, COUNT(r.rating) as votes')
						->from('rating r')
						->where('r.book_id = :book', array(
							':book' => $book->book_id,
						))
						->queryRow();
						
					$this->renderAjaxSuccess(array(
						'avg' => round($ret['avg'],2),
						'votes' => $ret['votes'],
					));
				}
			}
			elseif($this->isAjax())
			{
				// Manually send validation errors
				$this->renderAjaxError(array(
					'validationErrors' => array(
						'rating' => 'Please select a rating',
					)
				));
			}
		}
		
		// Redirect back to the book page
		$this->redirect($book->url);
	}
	
	/**
	 * Change the (current user's) status of a book
	 */
	public function actionStatus()
	{
		$book = $this->_getBookOrError();
		
		$form = new StatusForm();
		$this->pageTitle = 'Change status';
		
		// Get current status
		$old = UserBook::model()->findByAttributes(array(
			'book_id' => $book->book_id,
			'user_id' => Yii::app()->user->id,
		));
		
		// Save?
		if(isset($_POST['StatusForm']))
		{
			$form->attributes = $_POST['StatusForm'];
			if($form->validate())
			{
				$new = $form->status;
				
				if($new == 'none')
				{
					// Remove book from list
					if($old)
					{
						$old->delete();
						
						if($this->isAjax())
						{
							$this->renderAjaxSuccess(array(
								'new' => 'removed',
							));
						}
					}
					elseif($this->isAjax())
					{
						// We didn't change anything...
						$this->renderAjaxSuccess(array(
							'new' => 'unchanged',
						));
					}
				}
				else 
				{
					// Set new status		
					if(!$old)
					{
						$old = new UserBook();
						$old->book_id = $book->book_id;
						$old->user_id = Yii::app()->user->id;
					}
					
					$old->status = $new;
					$old->save();
					
					$this->renderAjaxSuccess(array(
						'new' => $new,
					));
				}
				
				$this->redirect($book->url);
			}
		}
		elseif($old)
		{
			// Initialize the form
			$form->status = $old->status;
		}
		
		// Render Status form
		$this->render('status-form',array(
			'model' => $form,
			'book' => $book
		));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CommentsController::beforeComment()
	 */
	public function beforeComment()
	{
		$book = $this->_getBookOrError();
	
		return array(
			'itemId' => $book->book_id,
			'itemType' => 'book',
			'returnUrl' => $book->url,
		);
	}
	
	/**
	 * Gets the current book from the ID provided in the URL.
	 *
	 * If the book is not found, an error message will be shown; else the book is returned.
	 *
	 * @return Book The loaded book
	 */
	protected function _getBookOrError()
	{
		$book_id = $_GET["id"];
		$book = Book::model()->findByPk($book_id);
		if(!$book)
		{
			throw new CHttpException(404, 'The requested book could not be found');
		}
		return $book;
	}
	
	/**
	 * Fetches data from Google books
	 * 
	 * @param string $isbn The ISBN number of the book to load information from
	 */
	protected function _fetchGoogleBooks($isbn)
	{		
		$url = 'https://www.googleapis.com/books/v1/volumes?key='
			. Yii::app()->params['api_key'] . '&projection=lite&q=isbn:' . $isbn;
		
		// Get the data
		$data = file_get_contents($url, null, Util::context());
		
		// Something went wrong..
		if(!$data)
		{
			return false;
		}
		
		// Decode the JSON
		$res = json_decode($data);
		
		// Invalid JSON
		if(!$res)
		{
			return false;
		}

		// Do we event have anything? (Maybe the book doesn't exist, or we have used up our API limit)
		if(empty($res->totalItems))
		{
			return false;
		}
		
		// Return the first (and only) book
		return $res->items[0];
	}
	
	/**
	 * Caches the cover of the book locally.
	 * 
	 * @param string $isbn ISBN number of the book to cache the cover for.
	 *  This is used as the file name.
	 * @param stdClass $gBooks Google Books data, which is the data returned
	 *  by the {@link _fetchGoogleBooks()} function.
	 */
	protected function _cacheBookCover($isbn, $gBooks)
	{
		// Do we have an image?
		if(empty($gBooks->volumeInfo) || empty($gBooks->volumeInfo->imageLinks))
		{
			return;
		}
		
		// Get thumbnail URL from the data
		$thumbUrl = $gBooks->volumeInfo->imageLinks->thumbnail;
		
		// Grab it from the server
		$data = file_get_contents($thumbUrl, null, Util::context());
		
		// Save it locally
		if($data)
		{
			file_put_contents(Yii::getPathOfAlias('webroot') . '/images/covers/' . $isbn . '.jpeg', $data);
		}
	}

}
