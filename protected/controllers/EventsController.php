<?php

/**
 * Controller that handles Events in general (not a specific event)
 */
class EventsController extends Controller
{
	/**
	 * Index page, shows an overview of all events.
	 * 
	 * Note: This does not handle the DataTable. The DataTable loads from
	 *  {@link actionAjaxList()} instead.
	 */
	public function actionIndex()
	{
		$t = time();
		
		// Grab events
		$events = Event::model()->with('location','user')->findAll(array(
			'select' => array(
				'event_id', 'name', 'description', 'starttime'
			),
			'condition' => 'IF(endtime=0,starttime+86399,endtime) > :time',
			'params' => array(
				':time' => $t,
			),
			'order' => 'starttime ASC',
			'offset' => 0,
			'limit' => 10,
		));
		
		// Count events
		$eventCount = Event::model()->count();
		
		// Count current events in the list
		$activeEventCount = Event::model()->count(array(
			'condition' => 'IF(endtime=0,starttime+86399,endtime) > :time',
			'params' => array(
				':time' => $t,
			),
		));
		
		// Render the page
		$this->render('list',array(
			'events' => $events,
			'current' => true,
			'eventCount' => $eventCount,
			'activeEventCount' => $activeEventCount,
		));
	}
	
	/**
	 * Ajax list: Processes the DataTable on the events index page.
	 */
	public function actionAjaxList()
	{
		// Are we event the datatable requesting?
		if(!$this->isAjax())
		{
			$this->redirect(array('/events'));
		}
		
		// Ensure consistent times
		$t = time();
		
		// Init conditions
		$conds = new CDbCriteria();
		
		// Search Globally (all columns)
		if(!empty($_GET['sSearch']))
		{
			$search = urldecode(trim($_GET['sSearch']));
			if($search)
			{
				$conds->addSearchCondition('e.name', $search);
				$conds->addSearchCondition('l.address', $search, true, 'OR');
				$conds->addSearchCondition('u.username', $search, true, 'OR');
				$conds->addSearchCondition('u.first_name', $search, true, 'OR');
				$conds->addSearchCondition('u.last_name', $search, true, 'OR');
			}
		}
		
		// Filtering by event type (the select box with 'upcoming','ongoing',etc.)
		if(empty($_GET['event_types']))
		{
			$_GET['event_types'] = 'active';
		}
		
		switch($_GET['event_types'])
		{
			case 'in_progress':
				$conds->addCondition(array(
					'IF(e.endtime=0,e.starttime+86399,e.endtime) > :time',
					'e.starttime < :time',
				));
				$conds->params[':time'] = $t;
				break;
			case 'upcoming':
				$conds->addCondition('e.starttime > :time');
				$conds->params[':time'] = $t;
				break;
			case 'active':
				$conds->addCondition(array(
					'IF(e.endtime=0,e.starttime+86399,e.endtime) > :time',
				));
				$conds->params[':time'] = $t;
				break;
			case 'all':
				break;
			case 'archive':
				$conds->addCondition('IF(e.endtime=0,e.starttime+86399,e.endtime) < :time');
				$conds->params[':time'] = $t;
				break;
		}
		
		// Show X items
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] != '-1')
		{
			$conds->limit = (int)$_GET['iDisplayLength'];
				
			if(!empty($_GET['iDisplayStart']))
			{
				$conds->offset = (int)$_GET['iDisplayStart'];
			}
			else
			{
				$conds->offset = 0;
			}
		}
		
		// Sorting
		$sortCol = $this->getSortColumn($_GET['iSortCol_0']);
		
		$sort = array(
			$sortCol . ' ' . ($_GET['sSortDir_0'] == 'asc' ? 'ASC' : 'DESC')
		);
		
		if($_GET['iSortCol_0'] == 4)
		{
			$sort[] = 'u.last_name ' . ($_GET['sSortDir_0'] == 'asc' ? 'ASC' : 'DESC');
		}
		
		// Secondary sort
		if(!empty($_GET['iSortCol_1']))
		{
			$sort[] = $this->getSortColumn($_GET['iSortCol_1'] . ' ' . ($_GET['sSortDir_1'] == 'asc' ? 'ASC' : 'DESC'));
		}
		
		// Do the actual select
		$c = Yii::app()->db->createCommand()
			->select('e.event_id, e.name, e.starttime, IF(e.endtime=0,e.starttime+86399,e.endtime) as endtime, l.address, u.first_name, u.last_name, u.username, u.user_id', 'SQL_CALC_FOUND_ROWS')
			->from('event e')
			->join('location l','e.location_id = l.location_id')
			->join('user u','e.user_id = u.user_id')
			->order($sort)
			->limit($conds->limit, $conds->offset);
		
		$events = null;
		if($conds->condition)
		{
			$c->where($conds->condition);
			$events = $c->queryAll(true, $conds->params);
		}
		else
		{
			$events = $c->queryAll();
		}
		
		// Count found results (SQL_CALC_FOUND_ROWS gets the count ignoring the limit)
		$results = (int)Yii::app()->db->createCommand("SELECT FOUND_ROWS()")->queryScalar();
		
		// Total events in database.
		$totalCount = Event::model()->count();
		
		// No results?
		if(!$results)
		{
			$this->renderAjax(array(
				'sEcho' => intval($_GET['sEcho']),
				'iTotalRecords' => $totalCount,
				'iTotalDisplayRecords' => 0,
				'aaData' => array(),
			));
		}
		
		// Format results
		$ret = array();
		foreach($events as &$event)
		{
			$type = 'active';
			if($event['starttime'] > $t )
			{
				$type = 'upcoming';
			}
			elseif( $event['endtime'] < $t )
			{
				$type = 'archived';
			}
			
			$icon = '<img src="'.Yii::app()->request->baseUrl.'/images/'.$type.'.ico" alt="'.$type.'" title="'.$type.'" />';
			if( $type == 'upcoming' )
			{
				$icon = '<a href="' . Util::url(
					'event/ical', array(
						'id' => $event['event_id'],
					)
				) . '">' . $icon . '</a>';
			}
			
			$ret[] = array(
				'name' => '<a href="'
				. Util::url('event/view',array(
					'id' => $event['event_id'],
					'title' => Util::slug($event['name']),
				)) .'">' . $event['name'] . '</a>',
				'date' => strftime('%B %d, %Y', $event['starttime']),
				'location' => strtok($event['address'],','),
				'user' => '<a href="'
					. Util::url('user/view', array(
						'id' => $event['user_id'],
						'title' => Util::slug($event['username']),
					)) . '">' . $event['first_name'] . ' ' . $event['last_name'] . '</a>',
				'icon' => $icon,
				'type' => $type,
			);
		}
		unset($event);
		
		// Return results data
		$this->renderAjax(array(
				'sEcho' => intval($_GET['sEcho']),
				'iTotalRecords' => $totalCount,
				'iTotalDisplayRecords' => $results,
				'aaData' => $ret,
		));
	}
	
	/**
	 * Gets the sorting column from the number
	 *
	 * @param int $colNumber number of the column to be sorted in the table
	 * @return string name of the column in the database
	 */
	protected function getSortColumn($colNumber)
	{
		switch($colNumber)
		{
			case 1:
				return 'e.name';
			case 2:
				return 'e.starttime';
			case 3:
				return 'l.address';
			case 4:
				return 'u.first_name';
		}
	}
}