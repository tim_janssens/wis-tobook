<?php

/**
 * Controller that handles books in general (not one specific book).
 */
class BooksController extends Controller
{
	/**
	 * Index page, shows a list of all books.
	 * 
	 * Note: This page shows the DataTable, but the DataTable itself loads
	 *  its data from the {@link actionAjaxList()} action instead.
	 */
	public function actionIndex()
	{
 		$books = Book::model()->with('users','genre')->findAll(array(
 			'limit' => 10,
 			'order' => 'title ASC',
 		));
 		
 		$totalBooks = Book::model()->count();
    
		$this->render('list',array('books'=>$books,'totalBooks'=>$totalBooks));
	}
	
	/**
	 * AJAX list action.
	 * 
	 * This action is used by the DataTable on the index (list) page. It does
	 *  all the server side processing (search, limit, etc.)
	 */
	public function actionAjaxList()
	{
		// Is this the DataTable requesting?
		if(!$this->isAjax())
		{
			$this->redirect('/books');
		}
		
		// Init query conditions
		$conds = new CDbCriteria();
		
		// Global search
		if(!empty($_GET['sSearch']))
		{
			// Have something to search?
			if($search = urldecode(trim($_GET['sSearch'])))
			{
				// Search all (relevant) columns
				$conds->addSearchCondition('b.title', $search);
				$conds->addSearchCondition('b.author', $search, true, 'OR');
				$conds->addSearchCondition('g.genre', $search, true, 'OR');
				
				// Can we search for ISBN?
				$isbnSearch = preg_replace('/[^0-9]/', '', $search);
				if($isbn = Util::convertToIsbn13($isbnSearch))
				{
					$conds->addSearchCondition('b.isbn', $isbn, true, 'OR');
				}
			}
		}
		
		// Individual Title search
		if(!empty($_GET['sSearch_1']))
		{
			if($search = urldecode(trim($_GET['sSearch_1'])))
			{
				$conds->addSearchCondition('b.title', $search);
			}
		}
		
		// Individual Author search
		if(!empty($_GET['sSearch_2']))
		{
			if($search = urldecode(trim($_GET['sSearch_2'])))
			{
				$conds->addSearchCondition('b.author', $search);
			}
		}
		
		// Individual Genre search
		if(!empty($_GET['sSearch_3']))
		{
			if($search = urldecode(trim($_GET['sSearch_3'])))
			{
				$conds->addColumnCondition(array('b.genre_id' => $search));
			}
		}
		
		// Individual Date search
		if(!empty($_GET['sSearch_4']))
		{
			// Split in start and end
			$start = strtok($_GET['sSearch_4'], ';');
			
			// Start?
			if($start == '---')
			{
				$start = false;
			}
			else
			{
				$start = date_parse($start);
				if($start && $start['error_count'] == 0)
				{
					$sTime = mktime(0,0,0,$start['month'],$start['day'],$start['year']);
					$conds->compare('b.release_date', '> ' . $sTime);
				}
			}
			
			// End?
			$end = strtok(';');
			if($end == '---')
			{
				$end = false;
			}
			else
			{
				$end = date_parse($end);
				if($end && $end['error_count'] == 0)
				{
					$eTime = mktime(0,0,0,$end['month'],$end['day'],$end['year']);
					$conds->compare('b.release_date', '< ' . $eTime);
				}
			}
		}
		
		// Show X entries
		if(!empty($_GET['iDisplayLength']) && $_GET['iDisplayLength'] != '-1')
		{
			$conds->limit = (int)$_GET['iDisplayLength'];
				
			if(!empty($_GET['iDisplayStart']))
			{
				$conds->offset = (int)$_GET['iDisplayStart'];
			}
			else
			{
				$conds->offset = 0;
			}
		}
		
		// Sorting
		$sortCol = $this->getSortColumn($_GET['iSortCol_0']);
		
		$sort = array(
			$sortCol . ' ' . ($_GET['sSortDir_0'] == 'asc' ? 'ASC' : 'DESC')
		);
		
		// Secondary sorting (sort on one column and then on another)
		if(!empty($_GET['iSortCol_1']))
		{
			$sort[] = $this->getSortColumn($_GET['iSortCol_1'] . ' ' . ($_GET['sSortDir_1'] == 'asc' ? 'ASC' : 'DESC'));
		}
		
		// Do the actual select from the database
		$c = Yii::app()->db->createCommand()
			->select('b.book_id, b.title, b.author, b.release_date, b.isbn, g.genre', 'SQL_CALC_FOUND_ROWS')
			->from('book b')
			->join('genre g','b.genre_id = g.genre_id')
			->order($sort)
			->limit($conds->limit, $conds->offset);
		
		$books = null;
		if($conds->condition)
		{
			$c->where($conds->condition);
			$books = $c->queryAll(true, $conds->params);
		}
		else
		{
			$books = $c->queryAll();
		}
		
		// Get the result count (SQL_CALC_FOUND_ROWS will calculate the results without the LIMIT)
		$results = (int)Yii::app()->db->createCommand("SELECT FOUND_ROWS()")->queryScalar();
		
		// Get total books in the database
		$totalCount = Book::model()->count();
		
		// No results?
		if(!$results)
		{
			$this->renderAjax(array(
				'sEcho' => intval($_GET['sEcho']),
				'iTotalRecords' => $totalCount,
				'iTotalDisplayRecords' => 0,
				'aaData' => array(),
			));
		}
		
		// Format the results
		$ret = array();
		foreach($books as &$book)
		{			
			$ret[] = array(
				'image' => '<a href="javascript:void(0);" class="show_book_cover" data-isbn="'.$book['isbn'].'">
	<img src="' . Yii::app()->request->baseUrl . '/images/book.ico" alt="">
</a>',
				'title' => '<a href="'
					. Util::url('book/view',array(
						'id' => $book['book_id'],
						'title' => Util::slug($book['title']),
					)) .'">' . $book['title'] . '</a>',
				'author' => $book['author'],
				'genre' => $book['genre'],
				'release_date' => strftime('%B %d, %Y', $book['release_date']),
			);
		}
		unset($book);
		
		// Send the results
		$this->renderAjax(array(
			'sEcho' => intval($_GET['sEcho']),
			'iTotalRecords' => $totalCount,
			'iTotalDisplayRecords' => $results,
			'aaData' => $ret,
		));
	}
	
	/**
	 * Gets the sorting column from the number
	 *
	 * @param int $colNumber number of the column to be sorted in the table
	 * 
	 * @return string name of the column in the database
	 */
	protected function getSortColumn($colNumber)
	{
		switch($colNumber)
		{
			case 1:
				return 'b.title';
			case 2:
				return 'b.author';
			case 3:
				return 'g.genre';
			case 4:
				return 'b.release_date';
		}
	}
	
	/**
	 * Gets an array of all genres
	 * 
	 * @return array An array of all genres in the database.
	 */
	public function getAllGenres()
	{
		$genres = Genre::model()->findAll();
	
		$ret = array(
			'' => 'Any genre'
		);
		foreach($genres as $genre)
		{
			/* @var $genre Genre */
			$ret[$genre->genre_id] = $genre->genre;
		}
	
		return $ret;
	}
	
}      

