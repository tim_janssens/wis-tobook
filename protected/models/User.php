<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property int $user_id
 * @property string $username
 * @property string $passwd
 * @property int $register_date
 * @property int $location_id
 * @property string $introduction
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $avatar_date
 * @property array $roles
 * 
 * The followings are the available model relations:
 * @property Location $location
 * @property Comment[] $comments
 * @property Event[] $events
 * @property Rating[] $ratings
 * @property Book[] $books
 * @property Event[] $rsvps
 *
 * The followings are available due to getters:
 * @property string $url
 * @property string $name
 */
class User extends DatabaseModel
{	
	/**
	 * Method to easily get the URL for a user.
	 *
	 * @param string $action action to generate URL for
	 * @return string the generated URL
	 */
	public function getUrl($action='view')
	{
		return Yii::app()->createUrl('user/' . $action, array(
			'id' => $this->user_id,
			'title' => Util::slug($this->username)
		));
	}
	
	/**
	 * Method to easily get the avatar URL for a user.
	 *
	 * @return string the avatar URL
	 */
	public function getAvatarUrl()
	{
		if(!$this->avatar_date)
		{
			return Yii::app()->baseUrl . '/images/avatars/default.png';
		}
		else
		{
			return Yii::app()->baseUrl . '/images/avatars/'.$this->user_id.'.png?' . $this->avatar_date;
		}
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}
	
	/**
	 * @return string the primary key column(s) of the table
	 */
	public function primaryKey()
	{
		return 'user_id';
	}
	
	/**
	 * Returns the user's name
	 */
	public function getName()
	{
		return $this->first_name . ' ' . $this->last_name;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::behaviors()
	 */
	public function behaviors()
	{
	    return array(
	          'AutoSerializeBehavior' => array(
	               'class' => 'application.behaviors.AutoSerializeBehavior',
	               'attributes' => array('roles'),
	           )
	    );
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('username, passwd, register_date, first_name, last_name, email', 'required'),
			array('username, first_name, last_name', 'length', 'max'=>45),
			array('passwd', 'length', 'min'=>60, 'max'=>60),
			array('email','email'),
			array('introduction, roles, avatar_date, location_id', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'comments' => array(self::HAS_MANY, 'Comment', 'user_id'),
			'events' => array(self::HAS_MANY, 'Event', 'user_id'),
			'ratings' => array(self::HAS_MANY, 'Rating', 'user_id'),
			'books' => array(self::MANY_MANY, 'Book', 'user_book(user_id, book_id)'),
			'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
			'rsvps' => array(self::MANY_MANY, 'Event', 'rsvp(user_id, event_id)')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'username' => 'Username',
			'passwd' => 'Password',
			'register_date' => 'Register Date',
			'country' => 'Country',
			'introduction' => 'Introduction',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email Address',
			'roles' => 'Roles',
		);
	}
	
	/**
	 * Adds a role to the user.
	 * 
	 * @param string $role The role to add
	 */
	public function addRole($role)
	{
		$tmp = isset($this->roles) ? $this->roles : array();
		$tmp[$role] = true;
		$this->roles = $tmp;
	}
	
	/**
	 * Revokes a role from a user.
	 * 
	 * @param string $role The role to revoke
	 */
	public function removeRole($role)
	{
		if(isset($this->roles[$role]))
		{
			unset($this->roles[$role]);
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
