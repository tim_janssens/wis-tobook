<?php

/**
 * Form to change preferences
 */
class PreferencesForm extends CFormModel
{
	public $introduction;
	public $address;
	public $first_name;
	public $last_name;
	public $email;
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return array(
			array('first_name, last_name, email', 'required'),
			array('email','email'),
			array('first_name','application.validators.NameValidator','message'=>'This is not a valid first name'),
			array('last_name','application.validators.NameValidator','message'=>'This is not a valid last name'),
			array('introduction, address','safe'),
		);
 	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{
		return array(
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'introduction' => 'Introduction',
			'address' => 'Address',
		);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::beforeValidate()
	 */
	public function beforeValidate()
	{
		$this->introduction = strip_tags($this->introduction);
		return true;
	}

}