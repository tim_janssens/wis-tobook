<?php

/**
 * This is the model class for table "comment".
 *
 * The followings are the available columns in table 'comment':
 * @property int $comment_id
 * @property string $comment_text
 * @property int $user_id
 * @property int $item_id
 * @property string $item_type
 * @property int $timestamp
 *
 * The followings are the available model relations:
 * @property Book $book
 * @property User $user
 */
class Comment extends DatabaseModel
{
	/**
	 * Method to easily get the URL for a comment.
	 *
	 * @param string $action action to generate URL for
	 * @return string the generated URL
	 */
	public function getUrl($action='view')
	{
		return Yii::app()->createUrl('comment/' . $action, array(
			'id' => $this->comment_id
		));
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comment';
	}
	
	/**
	 * @return string the primary key column(s) of the table
	 */
	public function primaryKey()
	{
		return 'comment_id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('comment_text, user_id, item_id, item_type, timestamp', 'required'),
			array('item_type','in','range'=>array('book','event'),'allowEmpty'=>false),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comment_id' => 'Comment',
			'comment' => 'Comment Text',
			'user_id' => 'User',
			'book_id' => 'Book',
		);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeValidate()
	{
		if(empty($this->timestamp))
		{
			$this->timestamp = time();
		}
		return true;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
