<?php

/**
 * This is the model class for table "location".
 *
 * The followings are the available columns in table 'location':
 * @property int $location_id
 * @property string $address
 * @property string $address_hash
 * @property float $lat
 * @property float $lng
 *
 * The followings are the available model relations:
 * @property Event[] $events
 */
class Location extends DatabaseModel
{
	/**
	 * Method to easily get the URL for a location.
	 *
	 * @param string $action action to generate URL for
	 * @return string the generated URL
	 */
	public function getUrl($action='view')
	{
		return Yii::app()->createUrl('location/' . $action, array(
			'id' => $this->location_id,
		));
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'location';
	}
	
	/**
	 * @return string the primary key column(s) of the table
	 */
	public function primaryKey()
	{
		return 'location_id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('address', 'required'),
			array('address', 'length', 'max'=>255),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'events' => array(self::HAS_MANY, 'Event', 'location_id'),
			'users' => array(self::HAS_MANY, 'User', 'location_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'location_id' => 'Location',
			'name' => 'Name',
			'address' => 'Address',
			'address_hash' => 'Address Hash',
		);
	}
	
	/**
	 * Finds a location by its address hash
	 * 
	 * @param string $hash
	 * @return Location|null
	 */
	public function findByHash($hash)
	{
		return $this->findByAttributes(array('address_hash'=>$hash));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CActiveRecord::beforeSave()
	 */
	public function beforeSave()
	{
		// Update hash
		$this->address_hash = md5($this->address);
		if(empty($this->lat) || !intval($this->lat))
		{
			$this->updateLatLng();
		}
		return true;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * Updates the latitude and longitude for the location based on the address.
	 */
	public function updateLatLng()
	{	
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='
			. urlencode($this->address)
			. '&sensor=false';
		
		try
		{			
			$data = file_get_contents($url,false,Util::context());
			
			if($data !== false)
			{
				if($res = json_decode($data))
				{	
					if($res->status == 'OK')
					{
						$this->lat = $res->results[0]->geometry->location->lat;
						$this->lng = $res->results[0]->geometry->location->lng;
					}
				}
			}
		}
		catch( Exception $e )
		{
			$this->lat = 0;
			$this->lng = 0;
		}
	}
	
	/**
	 * Calculates the distance to the logged in user.
	 */
	public function distanceToUser()
	{
		$earthRadius = 6372.795;
		
		if(!$latLng = Yii::app()->user->getState('latLng',false))
		{
			return 0;
		}
		
		$userLat = $latLng['lat'];
		$userLng = $latLng['lng'];
		
		$latDelta = deg2rad($userLat - $this->lat);
		$lngDelta = deg2rad($userLng - $this->lng);
				
		$angle = 2 * asin(
			sqrt(
				pow(sin($latDelta/2), 2)
				+ cos(deg2rad($this->lat)) * cos(deg2rad($userLat))
				* pow(sin($lngDelta/2), 2)
			)
		);
		
		return $earthRadius * $angle;
		
	}
}
