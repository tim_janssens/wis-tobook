<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property int $event_id
 * @property string $name
 * @property int $location_id
 * @property int $user_id
 * @property string $description
 * @property int $starttime
 * @property int $endtime
 *
 * The followings are the available model relations:
 * @property Location $location
 * @property User $user
 * @property User $rsvps
 * @property int $attendees
 * 
 * The following is accessible through getters
 * @property string $url
 */
class Event extends DatabaseModel
{
	/**
	 * Method to easily get the URL for a event.
	 *
	 * @param string $action action to generate URL for
	 * @return string the generated URL
	 */
	public function getUrl($action='view')
	{
		return Yii::app()->createUrl('event/' . $action, array(
			'id' => $this->event_id,
			'title' => Util::slug($this->name)
		));
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'event';
	}
	
	/**
	 * @return string the primary key column(s) of the table
	 */
	public function primaryKey()
	{
		return 'event_id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name, location_id, user_id, description, starttime, endtime', 'required'),
			array('name', 'length', 'max'=>45),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'rsvps' => array(self::MANY_MANY, 'User', 'rsvp(user_id,event_id)'),
			'attendees' => array(self::STAT, 'User', 'rsvp(user_id,event_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'event_id' => 'Event',
			'name' => 'Name',
			'location_id' => 'Location',
			'user_id' => 'User',
			'description' => 'Description',
			'timestamp' => 'Timestamp'
		);
	}
	
	/**
	 * Extracts data from an EventForm
	 * 
	 * @param EventForm $form The form to extract data from
	 */
	public function extractForm(EventForm $form)
	{		
		// Default data
		$this->name = $form->name;
		$this->description = $form->description;
		
		$start = new DateTime($form->date);
		$start->setTime($form->start_hour, $form->start_min, 0);
		
		$this->starttime = $start->getTimestamp();
		
		if($form->allday == 'true')
		{
			$this->endtime = 0;
		}
		else
		{
			$end = new DateTime($form->date);
			$end->setTime($form->end_hour, $form->end_min, 0);
			$this->endtime = $end->getTimestamp();
		}
						
		// Location
		$new_hash = md5($form->location_address);
		if( empty($this->event_id) || $this->location->address_hash != $new_hash)
		{
			// Does it exist then?
			$location = Location::model()->findByHash($new_hash);
			if($location)
			{
				$this->location_id = $location->location_id;
			}
			else 
			{
				$location = new Location();
				$location->address = $form->location_address;
				$location->save();
				
				$this->location_id = $location->location_id;
			}
		}
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Event the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
