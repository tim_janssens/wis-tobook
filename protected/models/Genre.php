<?php

/**
 * This is the model class for table "genre".
 *
 * The followings are the available columns in table 'genre':
 * @property int $genre_id
 * @property string $genre
 *
 * The followings are the available model relations:
 * @property Book[] $books
 */
class Genre extends DatabaseModel
{
	/**
	 * Method to easily get the URL for a genre.
	 *
	 * @param string $action action to generate URL for
	 * @return string the generated URL
	 */
	public function getUrl($action='view')
	{
		return Yii::app()->createUrl('genre/' . $action, array(
				'id' => $this->genre_id,
		));
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'genre';
	}
	
	/**
	 * @return string the primary key column(s) of the table
	 */
	public function primaryKey()
	{
		return 'genre_id';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('genre', 'required'),
			array('genre', 'length', 'max'=>45),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'books' => array(self::HAS_MANY, 'Book', 'genre_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'genre_id' => 'Genre',
			'genre' => 'Genre Name',
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Genre the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
