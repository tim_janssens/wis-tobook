<?php

/**
 * Form for avatar uploading.
 */
class AvatarForm extends CFormModel
{
	/**
	 * The image to upload
	 * 
	 * @var array
	 */
	public $image;
	
	/**
	 * x1 (left) coordinate of the cropping section.
	 * 
	 * @var int
	 */
	public $x1;
	
	/**
	 * y1 (top) coordinate of the cropping section.
	 * 
	 * @var int
	 */
	public $y1;
	
	/**
	 * x2 (right) coordinate of the cropping section.
	 * 
	 * @var int
	 */
	public $x2;
	
	/**
	 * y2 (bottom) coordinate of the cropping section.
	 * 
	 * @var int
	 */
	public $y2;
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return array(
			array('x1, y1, x2, y2','numerical'),
		);
 	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{
		return array(
			'image' => 'Avatar Image',
			'x1' => 'X1',
			'y1' => 'Y1',
			'x2' => 'X2',
			'y2' => 'Y2',
		);
	}
	
	/**
	 * Process Avatar Upload
	 */
	public function uploadAvatar()
	{
		if(!$this->validate())
		{
			return false;
		}
		
		try
		{
			// Do we have a file?
			if(empty($_FILES['AvatarForm']))
			{
				$this->addError('image', 'No file was uploaded');
				return false;
			}
			
			// Get the image data from the file
			$fName = $_FILES['AvatarForm']['tmp_name']['image'];
			if(!$fName)
			{
				$this->addError('image', 'No file was uploaded');
				return false;
			}
			
			// Get some info on the file
			$info = getimagesize($fName);

			$img = null;
			
			// So, what type is it?
			switch($info[2])
			{
				case IMAGETYPE_JPEG:
					$img = imagecreatefromjpeg($fName);
					break;
				case IMAGETYPE_PNG:
					$img = imagecreatefrompng($fName);
					break;
				default:
					$this->addError('image', 'File type not supported');
					return false;
			}
			
			// Make sure the cropping stuff is integers
			$this->x1 = intval($this->x1);
			$this->y1 = intval($this->y1);
			$this->x2 = intval($this->x2);
			$this->y2 = intval($this->y2);
			
			// Calculate width and height
			$w = abs($this->x2 - $this->x1);
			if(!$w)
			{
				$w = $info[0];
			}
			$h = abs($this->y2 - $this->y1);
			if(!$h)
			{
				$h = $info[1];
			}
			
			// Get the smallest of the two
			$dim = 0;
			if($w == $h)
			{
				$dim = $w;
			}
			elseif($w > $h)
			{
				$dim = $h;
			}
			else
			{
				$dim = $w;
			} 
			
			// New dimension of the image (100x100px)
			$newDim = 100;
			
			// Create a blank new image
			$newImg = imagecreatetruecolor($newDim, $newDim);
			
			// Copy the relevant part over and resize
			imagecopyresampled($newImg, $img, 0, 0, $this->x1, $this->y1, $newDim, $newDim, $dim, $dim);
			
			// Save it as a PNG
			imagepng($newImg,Yii::getPathOfAlias('webroot').'/images/avatars/' . Yii::app()->user->id . '.png');
			
			// Success
			return true;
		}
		catch( Exception $e )
		{
			$this->addError('image', 'Upload failed (' . $e->getMessage() . ')');
			return false;
		}
	}

}