<?php

/**
 * Form to add comments
 */
class CommentForm extends CFormModel
{
	/**
	 * Text of the comment
	 * 
	 * @var string
	 */
	public $comment;
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return array(
			array('comment','required'),
		);
 	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{
		return array(
			'comment' => 'Comment',
		);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::beforeValidate()
	 */
	public function beforeValidate()
	{
		$this->comment = Util::purify($this->comment);
		return true;
	}

}