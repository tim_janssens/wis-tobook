<?php

/**
 * Form to update your status for a book
 */
class StatusForm extends CFormModel
{
	public $status;
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return array(
			array('status','required'),
			array('status','in','range' => array('none','wishlist','progress','finished'))
		);
 	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{
		return array(
			'status' => 'Status',
		);
	}
	
	/**
	 * Gets a list of available statuses
	 */
	public function getAllStatuses()
	{
		return array(
			'none' => "Not on my list",
			'wishlist' => 'Wishlist',
			'progress' => 'Currently Reading',
			'finished' => 'Finished Reading',
		);
	}

}