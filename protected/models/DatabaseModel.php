<?php

/**
 * Generic model that represents a database table.
 */
abstract class DatabaseModel extends CActiveRecord
{		
	/**
	 * Save the record to the database.
	 * 
	 * Modified to add clearer error messages when something does not save.
	 */
	public function save($runValidation=true,$attributes=null)
	{
		$ret = parent::save($runValidation,$attributes);
		if(!$ret)
		{
			$errString = '';
			foreach($this->getErrors() as $error => $messages)
			{
				$errString .= "\n - {$error}: " . implode(';', $messages);
			}
			throw new CDbException('Could not save (' . get_class($this) . "):" . $errString);
		}
		return $ret;
	}
	
} 