<?php

/**
 * Form for creation and editing of events.
 * 
 * @property int $event_id
 * @property string $name
 * @property string $description
 * @property int $date
 * @property string $location_name
 * @property string $location_address
 * @property string $allday
 * @property int $start_hour
 * @property int $start_min
 * @property int $end_hour
 * @property int $end_min
 */
class EventForm extends CFormModel
{
	public $event_id;
	public $name;
	public $description;
	public $date;
	
	public $location_address;
	
	public $allday;
	public $start_hour;
	public $start_min;
	public $end_hour;
	public $end_min;
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return array(
			array('event_id','required','on'=>'update'),
			array('name, description, date, location_address, allday', 'required'),
			array('start_hour, start_min, end_hour, end_min', 'safe'),
			array('allday','validateTime'),
		);
 	}
 	
 	/**
 	 * (non-PHPdoc)
 	 * @see CModel::beforeValidate()
 	 */
 	public function beforeValidate()
 	{
 		$this->description = Util::purify($this->description);
 		return true;
 	}
	
	/**
	 * Gets a list of valid location types for use in radio buttons.
	 * 
	 * @see CHtml::activeRadioButtonList()
	 */
	public function getLocationTypes()
	{
		return array(
			'database' => 'From Database',
			'new' => 'New location'
		);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{
		return array(
			'event_id' => 'ID',
			'name' => 'Event Name',
			'description' => 'Description',
			'date' => 'Date',
			'location_address' => 'Address',
			'allday' => 'All Day',
			'start_hour' => 'Start Hour',
			'start_min' => 'Start Minutes',
			'end_hour' => 'End Hour',
			'end_min' => 'End Minutes',
		);
	}
	
	/**
	 * Validates the entered time.
	 * 
	 * Will check whether the selected date actually exists, and if the starting
	 *  time is before the ending time.
	 *  
	 * @param string $attribute
	 * @param array $params
	 */
	public function validateTime($attribute,$params)
	{
		$this->allday = ($this->allday == 'true') ? 'true' : 'false';
		$this->start_hour = (int)$this->start_hour;
		$this->start_min = (int)$this->start_min;
		$this->end_hour = (int)$this->end_hour;
		$this->end_min = (int)$this->end_min;
		
		$d = date_parse($this->date);
		if( $d['warning_count'] > 0 || $d['error_count'] > 0 )
		{
			$this->addError('date', 'The entered date is invalid');
		}
		
		if( $this->allday != 'true' )
		{
			if($this->start_hour < 0 || $this->start_hour > 23)
			{
				$this->addError('allday', 'Invalid starting time');
			}
			elseif($this->start_min < 0 || $this->start_min > 59)
			{
				$this->addError('allday', 'Invalid starting time');
			}
			
			if($this->end_hour < 0 || $this->end_hour > 23)
			{
				$this->addError('allday', 'Invalid ending time');
			}
			elseif($this->end_min < 0 || $this->end_min > 59)
			{
				$this->addError('allday', 'Invalid ending time');
			}
			
			if( empty($this->errors['allday']) )
			{
				// Start time > End time ?
				if( $this->start_hour * 100 + $this->start_min >= $this->end_hour * 100 + $this->end_min )
				{
					$this->addError('allday', 'Ending time must be after starting time');
				}
			}
		}
	}
	
	/**
	 * Extracts the data from an existing event into the form.
	 * 
	 * This should be used when editing an existing event.
	 * 
	 * @param Event $event The event to extract
	 */
	public function extractEvent(Event $event)
	{
		$this->event_id = $event->event_id;
		$this->name = $event->name;
		$this->description = $event->description;
		$this->location_address = $event->location->address;
		
		$this->date = strftime('%Y-%m-%d',$event->starttime);
				
		if($event->endtime == 0)
		{
			$this->allday = 'true';
		}
		else
		{
			$this->allday = 'false';
			
			$d = getdate($event->starttime);
			$this->start_hour = $d['hours'];
			$this->start_min = $d['minutes'];
				
			$d = getdate($event->endtime);
			$this->end_hour = $d['hours'];
			$this->end_min = $d['minutes'];
		}
	}
}