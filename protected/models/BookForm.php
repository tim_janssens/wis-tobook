<?php

/**
 * Form for creation and editing of Book.
 * 
 * @property int $book_id
 * @property string $title
 * @property string $author
 * @property string $blurb
 * @property string $genre 
 * @property int $release_date
 * @property string $isbn
 */
class BookForm extends CFormModel
{
	public $book_id;
	public $title;
	public $author;
	public $blurb;
	public $release_date;
	public $genre;
	public $status;
	public $isbn;

	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return array(
			array('book_id','required','on'=>'update'),
			array('title, author, blurb, genre, release_date, status', 'required'),
			array('isbn','required','message'=>'ISBN is required. If you dont know the ISBN, use the title and author to search.'),	
			array('isbn','application.validators.IsbnValidator'),
			array('isbn','checkDuplicateIsbn'),
		);
 	}
 	
 	/**
 	 * (non-PHPdoc)
 	 * @see CModel::beforeValidate()
 	 */
 	public function beforeValidate()
 	{
 		$this->blurb = strip_tags($this->blurb);
 		if(!empty($this->isbn))
 		{
 			$this->isbn = preg_replace('/[^0-9X]/', '', $this->isbn);
 			$this->isbn = Util::convertToIsbn13($this->isbn);
 		}
 		return true;
 	}
 	
 	/**
 	 * 
 	 */
 	public function checkDuplicateIsbn($attribute,$params)
 	{ 		
 		/* @var $b Book */
 		$b = Book::model()->findByIsbn($this->isbn);
 		if($b && ( empty($this->book_id) || $b->book_id != $this->book_id ))
 		{
 			$this->addError('isbn', 'This book is already in the database (<a href="'.$b->url.'">view</a>)');
 		}
 	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{
		return array(
			'book_id' => 'ID',
			'title' => 'Book Title',
			'author' => 'Author',
			'blurb' => 'Blurb',
			'release_date' => 'Release Date',
			'genre' => 'Genre',
      		'status' => 'Status',
			'isbn' => 'ISBN',
		);
	}

	/**
	 * Extracts the data from an existing book into the form.
	 * 
	 * This should be used when editing an existing book.
	 * 
	 * @param Book $book The book to extract
	 */
	public function extractBook(Book $book)
	{
		$this->book_id = $book->book_id;
		$this->title = $book->title;
		$this->author = $book->author;
		$this->blurb = $book->blurb;
		$this->genre = $book->genre->genre_id;
		$this->isbn = $book->isbn;
		
		$this->release_date = strftime('%Y-%m-%d',$book->release_date);
		
		$u = UserBook::model() -> findByAttributes(array(
			'book_id' => $_GET["id"],
			'user_id' => Yii::app()->user->id,  
		));
    
		if(!$u)
		{
			$status='Wishlist';
		}                               
		else
		{
			$this->status = $u->status;
		}
	}
	
	/**
	 * Gets an array of all genres
	 */
	public function getAllGenres()
	{
		$genres = Genre::model()->findAll();
		
		$ret = array(
			'' => '-Choose One-',
		);
		foreach($genres as $genre)
		{
			/* @var $genre Genre */
			$ret[$genre->genre_id] = $genre->genre;
		}
		
		return $ret;
	}
  
	/**
	 * Gets an array of all user book status
	 */ 
	public function getAllStatus() 
	{
		return array(
			'Do not add book to my list',
			'wishlist' => 'On wishlist',
			'progress'=>'Currently reading',
			'finished'=>'Finished reading',
		); 
	}
}   
