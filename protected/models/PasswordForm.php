<?php
/**
 * Form for password resets
 */
class PasswordForm extends CFormModel
{
	public $old_password;
	public $password;
	public $password_confirm;
	
	/**
	 * User for whom the form is displayed.
	 * 
	 * Should be filled by the calling controller.
	 * 
	 * @var User
	 */
	protected $user;
	
	/**
	 * Sets the user for the form, used to validate the old password.
	 */
	public function setUser(User $user)
	{
		$this->user = $user;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return array(
			array('old_password, password, password_confirm','required'),
			array('password_confirm','compare','compareAttribute' => 'password', 'message'=>'Passwords must match'),
			array('old_password','checkOldPassword'),
		);
 	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{
		return array(
			'old_password' => 'Old Password',
			'password' => 'Password',
			'password_confirm' => 'Confirm Password',
		);
	}
	
	/**
	 * Checks whether the old password is correct.
	 */
	public function checkOldPassword($attribute,$params)
	{
		$pwd = $this->$attribute;
		if(!Util::verifyPassword($pwd,$this->user->passwd))
		{
			$this->addError('old_password', 'Old password is incorrect');
		}
	}

}