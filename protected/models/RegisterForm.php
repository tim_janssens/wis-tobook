<?php

/**
 * Model that represents the Registration form
 */
class RegisterForm extends CFormModel
{
	public $username;
	public $password;
	public $password_confirm;
	public $email;
	
	public $first_name;
	public $last_name;
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::rules()
	 */
	public function rules()
	{
		return array(
			array('username, password, password_confirm, email, first_name, last_name','required'),
			array('username','checkUsername'),
			array('email','email'),
			array('password_confirm', 'compare', 'compareAttribute' => 'password', 'message'=>'Passwords must match'),
			array('first_name','application.validators.NameValidator','message'=>'This is not a valid first name'),
			array('last_name','application.validators.NameValidator','message'=>'This is not a valid last name'),
		);
	}

	/**
	 * (non-PHPdoc)
	 * @see CModel::attributeLabels()
	 */
	public function attributeLabels()
	{		
		return array(
			'username' => 'Username',
			'password' => 'Password',
			'password_confirm' => 'Confirm Password',
			'email' => 'Email',
			'first_name' => 'First name',
			'last_name' => 'Last name',
		);
	}
	
	/**
	 * Checks whether a username is valid and available
	 */
	public function checkUsername($attribute,$params)
	{
		if(preg_match('/[^a-zA-Z0-9_.-]/',$this->username))
		{
			$this->addError('username', 'Username contains invalid characters');
			return;
		}
		
		$user = Yii::app()->db->createCommand()
			->select('username')
			->from('user')
			->where('username=:username', array(':username'=>$this->username))
			->queryRow();
		
		if($user)
		{
			$this->addError('username', 'Username is already in use');
		}
	}
}