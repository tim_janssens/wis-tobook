<?php

/**
 * This is the model class for table "book".
 *
 * The followings are the available columns in table 'book':
 * @property int $book_id
 * @property string $title
 * @property string $blurb
 * @property string $author
 * @property int $user_count
 * @property int $release_date
 * @property int $genre_id
 * @property string $isbn
 *
 * The followings are the available model relations:
 * @property Genre $genre
 * @property Comment[] $comments
 * @property Rating[] $ratings
 * @property User[] $users
 * @property UserBook[] $user_books
 * 
 * The followings are available through getters:
 * @property string $url
 * @property string $imageUrl
 */
class Book extends DatabaseModel
{
	/**
	 * Method to easily get the URL for a book.
	 *
	 * @param string $action action to generate URL for
	 * 
	 * @return string the generated URL
	 */
	public function getUrl($action='view')
	{
		return Yii::app()->createUrl('book/' . $action, array(
			'id' => $this->book_id,
			'title' => Util::slug($this->title)
		));
	}
	
	/**
	 * Gets the URL of the book cover image
	 * 
	 * @return string The cover URL
	 */
	public function getImageUrl()
	{
		return Yii::app()->request->baseUrl . '/images/covers/' . $this->isbn . '.jpeg';
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'book';
	}
	
	/**
	 * @return string the primary key column(s) of the table
	 */
	public function primaryKey()
	{
		return 'book_id';
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title, user_count, release_date, isbn', 'required'),
			array('title', 'length', 'max'=>100),
			array('author', 'length', 'max'=>60),
			array('isbn', 'application.validators.IsbnValidator'),
			array('user_count, release_date', 'length', 'max'=>10),
			array('blurb', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'genre' => array(self::BELONGS_TO, 'Genre', 'genre_id'),
			'comments' => array(self::HAS_MANY, 'Comment', 'book_id'),
			'ratings' => array(self::HAS_MANY, 'Rating', 'book_id'),
			'users' => array(self::HAS_MANY, 'User', 'user_id', 'through'=>'user_books'),
			'user_books' => array(self::HAS_MANY, 'UserBook', 'book_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'book_id' => 'Book',
			'title' => 'Title',
			'blurb' => 'Blurb',
			'author' => 'Author',
			'user_count' => 'User Count',
			'release_date' => 'Release Date',
			'isbn' => 'ISBN',
		);
	}
	
	/**
	 * Extracts the data from the form into the book.
	 * 
	 * @param BookForm $form The form to be extracted.
	 */
	public function extractForm(BookForm $form)
	{
		$this->title = $form->title;
		$this->blurb = $form->blurb;
		$this->author = $form->author;
		$this->genre_id = $form->genre;
		$this->isbn = $form->isbn;

		$release_date = new DateTime($form->release_date);
		$this->release_date = $release_date->getTimestamp();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CModel::beforeValidate()
	 */
	public function beforeValidate()
	{
		if(!empty($this->isbn))
		{
			$this->isbn = preg_replace('/[^0-9]/', '', $this->isbn);
		}
		return true;
	}
	
	/**
	 * Finds a book by ISBN
	 * 
	 * @param string $isbn
	 */
	public function findByIsbn($isbn)
	{
		return self::model()->findByAttributes(array(
			'isbn' => $isbn,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Book the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
