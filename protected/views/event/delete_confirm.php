<?php
/* @var $this EventController */
/* @var $event Event */
/* @var $form Form */
?>

<?php $form=$this->beginWidget('Form', array(
	'id'=>'delete-event-form',
));
?>

	<fieldset>
		<p>Are you sure you wish to delete this event? This action can not be undone!</p>
		
		<?php
		$form->hidden('delete_confirm',1);
		?>
	</fieldset>
	
	<?php $form->submit('Delete Event'); ?>

<?php $this->endWidget(); ?>