<?php
/* @var $this EventController */
/* @var $model EventForm */
/* @var $form Form */
$this->initGoogleMaps();
?>
<div class="form">

<?php $form=$this->beginWidget('Form', array(
	'id'=>'event-form',
	'model'=>$model,
	'useAjaxSubmit'=>true,
));
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<fieldset>
		<legend>Event Details</legend>
		<?php		
		$form->text('name');
		$form->richText('description');
		
		$pickOpts = array(
			'minDate' => strftime('%Y-%m-%d'),
		);
		$form->date('date',$pickOpts);
		
		$form->startRow();
		$form->radios('allday',array('true'=>'All Day','false'=>'From&nbsp;'));
		$errClass = !empty($model->errors['allday']) ? 'error' : '';
		$form->number('start_hour',array('min'=>'0','max'=>'23','placeholder'=>'hh','class'=>$errClass)); ?>
		<label for="EventForm_start_min">:</label>
		<?php $form->number('start_min',array('min'=>'0','max'=>'59','placeholder'=>'mm','class'=>$errClass)); ?>
		<label for="EventForm_end_hour">&nbsp;To&nbsp;</label>
		<?php $form->number('end_hour',array('min'=>'0','max'=>'23','placeholder'=>'hh','class'=>$errClass)); ?>
		<label for="EventForm_end_min">:</label>
		<?php $form->number('end_min',array('min'=>'0','max'=>'59','placeholder'=>'mm','class'=>$errClass));
		echo '<br />';
		$form->error('allday');
		$form->endRow();
		?>
	</fieldset>
	
	<fieldset>
		<legend>Location Details</legend>
		<?php 
		$form->text('location_address');
		?>
	</fieldset>
	
	<?php $form->submit('Save event'); ?>

<?php $this->endWidget(); ?>

</div><!-- form -->