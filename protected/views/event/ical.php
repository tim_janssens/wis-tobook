<?php 
/* @var $event Event */
/* @var $this EventController */
header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: attachment; filename=event.ics');
?>
BEGIN:VCALENDAR
PRODID:-//ToBook v1.0//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
BEGIN:VTIMEZONE
TZID:Universal
BEGIN:DAYLIGHT
TZOFFSETFROM:+0000
TZOFFSETTO:+0100
TZNAME:BST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=10
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0100
TZOFFSETTO:+0000
TZNAME:UTC
DTSTART:19700329T030000
RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=3
END:STANDARD
END:VTIMEZONE
BEGIN:VEVENT
SUMMARY:<?php echo $event->name; ?> 
DTSTAMP:<?php echo strftime('%Y%m%dT%H%M%SZ'); ?> 
<?php if($event->endtime == 0) { // All day ?>
DTSTART;TZID=Universal;VALUE=DATE:<?php echo strftime('%Y%m%d', $event->starttime); ?> 
DTEND;TZID=Universal;VALUE=DATE:<?php echo strftime('%Y%m%d', $event->starttime + 86400); // DTEND is non-inclusive ?> 
<?php } else { // Not all day ?>
DTSTART;TZID=Universal:<?php echo strftime('%Y%m%dT%H%M00Z', $event->starttime); ?> 
DTEND;TZID=Universal:<?php echo strftime('%Y%m%dT%H%M00Z', $event->endtime); ?> 
<?php } ?>
UID:TOBOOK<?php echo md5('ToBook_EVENT_'.$event->event_id); ?> 
DESCRIPTION:<?php echo substr(strip_tags($event->description),0,150); ?> 
LOCATION:<?php echo $event->location->address; ?> 
END:VEVENT
END:VCALENDAR