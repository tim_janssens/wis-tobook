<?php
/* @var $this EventController */
/* @var $event Event */
/* @var $model CommentForm */
$this->initGoogleMaps();
$this->breadcrumbs = array(
	'Events'=>array('events/'),
);

$hasRsvp = false;
$rsvps = $event->rsvps;
foreach( $rsvps as $r ) /* @var $r User */
{
	if($r->user_id == Yii::app()->user->id)
	{
		$hasRsvp = true;
		break;
	}
}
$rsvps[] = $event->user;
$ending_time = $event->endtime == 0 ? $event->starttime + 86399 : $event->endtime;
$t = time();
?>
<div itemscope="" itemtype="http://schema.org/LiteraryEvent">
	<span class="microdata_info">
		<meta itemprop="name" content="<?php echo $event->name; ?>" />
		<meta itemprop="url" content="<?php echo Yii::app()->request->hostInfo, $event->url; ?>" />
		<meta itemprop="startDate" content="<?php echo strftime("%Y-%m-%d",$event->starttime); ?>" />
		<meta itemprop="endDate" content="<?php echo strftime("%Y-%m-%d",$ending_time); ?>" />
		<meta itemprop="doorTime" content="<?php echo strftime("%Y-%m-%dT%H-%M-%SZ",$event->starttime); ?>" />
	</span>
	<?php if($ending_time > $t) { ?>
	<div id="event_controls">
		<?php if(!Yii::app()->user->isGuest) { ?>
			<?php if(Yii::app()->user->id == $event->user_id) { ?>
				<a href="<?php echo $event->getUrl('edit'); ?>" class="button">Edit Event</a>
				<a href="<?php echo $event->getUrl('delete'); ?>" class="button red modal_link">Delete Event</a>
			<?php } else { ?>
				<?php echo CHtml::form($event->getUrl('rsvp'),'post',array('id'=>'rsvp-form')); ?>
					<input type="submit" class="button<?php if($hasRsvp) {?> green<?php } ?>" value="RSVP" id="rsvp_button" />
				<?php echo CHtml::endForm(); ?>
			<?php } ?>
		<?php } ?>
		<a href="<?php echo $event->getUrl('ical')?>" class="button bluegrey">Export to iCal</a>
	</div>
	<?php } ?>
	<br class="clear" />
	<div id="event_details">
		<p itemscope="" itemtype="http://schema.org/Person">Event hosted by <a itemprop="name" href="<?php echo Yii::app()->request->hostInfo, $event->user->url; ?>"><?php echo $event->user->name; ?></a></p>
		<hr />
		<p><strong>When:</strong>
		<?php
		echo strftime('%B %d, %Y',$event->starttime);
		if($event->endtime != 0) {
			echo ', from ', strftime('%H:%M',$event->starttime);
			echo ' to ', strftime('%H:%M',$event->endtime);
		}
		?><br /><?php 
		if( $event->starttime > $t ) {
			if($event->starttime - $t < 86400)
			{ 
				if($event->starttime - $t < 3600)
				{
					echo 'Starts in less than 1 hour';
				}
				else
				{
					$diff = ceil(($event->starttime - $t)/3600);
					echo 'Starts in ', $diff, ' hour', ($diff == 1 ? '' : 's');
				}
			}
			else
			{
				$diff = ceil(($event->starttime - $t)/86400);
				echo 'Starts in ', $diff, ' day' . ($diff == 1 ? '' : 's');
			}
		}
		elseif( $ending_time > $t )
		{
			echo 'Event is ongoing';
		}
		else
		{
			if($t - $ending_time < 86400)
			{
				if($t - $ending_time < 3600)
				{
					echo 'Ended less than 1 hour ago';
				}
				else
				{
					$diff = ceil(($t - $ending_time)/3600);
					echo 'Ended ', $diff, ' hour', ($diff == 1 ? '' : 's'), ' ago';
				}
			}
			else
			{
				$diff = ceil(($t - $ending_time)/86400);
				echo $diff, ' day' . ($diff == 1 ? '' : 's'), ' ago';
			}
		}
		?></p>
		
		<hr />
	
		<p><strong>What:</strong> <span itemprop="description"><?php echo $event->description; ?></span></p>
		
		<hr />
		
		<p><strong>Where:</strong> <?php echo $event->location->address; ?></p>
	</div>
	
	<div id="event_location">
		<div id="event_location_map"></div>
		<?php if($dist = $event->location->distanceToUser()) { ?>
			<span id="event_location_approx" title="Birds eye distance">Approx. <?php echo round($dist); ?>km*</span>
		<?php } ?>
		<?php echo CHtml::link('View on Google Maps','http://maps.google.com/?q='.urlencode($event->location->address)); ?>
		<?php if($addr = Yii::app()->user->getState('address',false)) {
			echo CHtml::link('Get Directions','http://maps.google.com/?saddr='
				. urlencode($addr)
				. '&daddr=' . urlencode($event->location->address)
			);
		} ?>
	</div>
	
	<br class="clear" />
	
	<div id="event_rsvps">
		<h2>People attending this event</h2>
		<ul id="rsvp_list">
			<li id="no_rsvps" <?php if(count($rsvps)) { ?>style="display:none;"<?php } ?>>No known people <?php
				if($ending_time < $t) {
			?>have attended this event.<?php 
				} else {
			?>are attending this event yet.<?php
				} 
			?></li>
			<?php foreach($rsvps as $rsvp) { /* @var $rsvp User */ ?>
				<li class="event_rsvp" id="rsvp_<?php echo $rsvp->user_id; ?>">
					<img src="<?php echo $rsvp->getAvatarUrl(); ?>" alt="ava" class="avatar mini"/>
					<span><a href="<?php echo $rsvp->url; ?>"><?php echo $rsvp->name; ?></a></span>
				</li>
			<?php } ?>
		</ul>
		<br class="clear" />
	</div>
	
	<div id="event_comments">
		<?php
			$this->renderPartial('/comments',array(
				'comments' => $comments,
				'model' => $model,
				'formAction' => $event->getUrl('comment'),
			));
		?>
	</div>
	<script>loadMap('<?php echo $event->location->address; ?>');</script>
</div>