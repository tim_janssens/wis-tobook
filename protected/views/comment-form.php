<?php
/* @var $this Controller */
/* @var $form Form */
?>
<h3>Add a Comment</h3>

<div class="form">

<?php $form=$this->beginWidget('Form', array(
	'id'=>'comment-form',
	'model'=>$model,
	'action'=>$formAction,
	'htmlOptions'=>array(
		'class'=>'no-labels',
	),
	'useAjaxSubmit'=>true,
	'ajaxSuccessFunction'=>'appendComment',
));
?>
	<?php
	$form->richText('comment');
	?>
	
	<div class="buttons">
		<?php $form->submit('Add Comment'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->