<?php
/* @var $this UserController */
/* @var $user User */
$this->initDataTables();
$t = time();
?>
<div id="avatar_container">
	<img src="<?php echo $user->getAvatarUrl(); ?>" class="avatar" alt="avatar" />
</div>
<div id="info_container">
	<p><strong>Username: </strong> <?php echo $user->username; ?></p>
	<p><strong>User number: </strong> <?php echo $user->user_id; ?></p>
	<p><strong>Member Since: </strong> <?php echo strftime('%B %d, %Y',$user->register_date); ?></p>
</div>
<br class="clear" />
<?php if($user->introduction) {?>
	<div class="user_intro">
		<h2>About Me</h2>
		<p><?php echo nl2br($user->introduction); ?></p>
	</div>
<?php } ?>
<div id="user_tabs">
	<ul class="tabs">
		<li><a href="#user_books">Books</a></li>
	    <li><a href="#user_events">Events</a></li>
	</ul>
	
	<!-- 
	  - BOOKS
	-->
	<div id="user_books">
		<?php if($totalBooks > 0) { ?>
			<span id="book_type_controls">
				<label for="book_types"><strong>Show: </strong></label>
				<select id="book_types">
					<option value="all">All books</option>
					<option value="wishlist">On wishlist</option>
					<option value="progress">Reading</option>
					<option value="finished">Finished</option>
				</select>
			</span>
			<br class="clear" />
			<table id="books_list">
				<thead>
					<tr>
						<th class="image"></th>
						<th class="title">Title</th>
						<th class="author">Author</th>
						<th class="status">Status</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($books as $book) { /* @var $book UserBook */
						$status = 'On wishlist';
						switch($book->status)
						{
							case 'wishlist':
								break;
							case 'progress':
								$status = 'Reading';
								break;
							case 'finished':
								$status = 'Finished';
								break;
						} ?>
						<tr class="book_<?php echo $book->status; ?>">
							<td class="image">
								<a href="javascript:void(0);" class="show_book_cover" data-isbn="<?php echo $book->book->isbn; ?>">
	        							<img src="<?php echo Yii::app()->request->baseUrl ?>/images/book.ico" alt="" />
	        						</a>
								</td>
								<td class="title"><a href="<?php echo $book->book->url; ?>"><?php echo $book->book->title; ?></a></td>
							<td class="author"><?php echo $book->book->author; ?></td>
							<td class="status"><?php echo $status; ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			<script>
			var loadBooksUrl = '<?php echo $user->getUrl('books'); ?>';
			var totalBooks = <?php echo $totalBooks; ?>;
			</script>
		<?php } else { ?>
			<p>This user has not added any books.</p>
			<script>var totalBooks = 0;</script>
		<?php } ?>
	</div>
	
	<!-- 
	  - EVENTS
	-->
	<div id="user_events">
		<?php if($totalEvents > 0) { ?>
			<span id="event_type_controls">
				<label for="event_types"><strong>Show: </strong></label>
				<select id="event_types">
					<option value="active">Upcoming and ongoing events</option>
					<option value="in_progress">Ongoing events</option>
					<option value="upcoming">Upcoming events</option>
					<option value="archive">Archived events</option>
					<option value="all">All Events</option>
				</select>
			</span>
			<br class="clear" />
			<table id="events_list">
				<thead>
					<tr>
						<th class="icon"></th>
						<th class="name">Event</th>
						<th class="date">Date</th>
						<th class="location">Location</th>
						<th class="attendees">Attendees</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($events as $event) { /* @var $event Event */?>
						<?php
							// Calculate type
							$ending_time = $event->endtime ? $event->endtime : $event->starttime + 86399;
							$type = 'active';
							if($event->starttime > $t)
							{
								$type = 'upcoming';	
							}
							elseif( $ending_time < $t )
							{
								$type = 'archived';
							}
						?>
						<tr class="event_<?php echo $type; ?>">
							<td class="icon">
								<?php if($type == 'upcoming') { ?><a href="<?php echo $event->getUrl('ical'); ?>"><?php }?>
								<img src="<?php echo Yii::app()->request->baseUrl, '/images/', $type ?>.ico" alt='<?php echo $type; ?>' title='<?php echo $type; ?>' />
								<?php if($type == 'upcoming') { ?></a><?php }?>
							</td>
							<td class="name"><a href="<?php echo $event->url; ?>"><?php echo $event->name; ?></a></td>
							<td class="date"><?php echo strftime('%B %d, %Y', $event['starttime']); ?></td>
							<td class="location"><?php echo strtok($event->location->address,','); ?></td>
							<td class="attendees"><?php echo $event->attendees; ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
			<script>
			var loadEventsUrl = '<?php echo $user->getUrl('events'); ?>';
			var totalEvents = <?php echo $totalEvents; ?>;
			var activeEvents = <?php echo $activeEvents; ?>;
			</script>
		<?php } else { ?>
			<p>This user has not created any events.</p>
			<script>var totalEvents = 0;</script>
		<?php } ?>
	</div>
</div>