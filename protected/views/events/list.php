<?php
/* @var $this EventsController */

$this->pageTitle='Events List';
$this->initDataTables();
$t = time();
?>
<div id="events_controls_bar">
	<div id="events_controls">
		<a href="<?php echo Util::url('event/new'); ?>" class="button"><?php if(Yii::app()->user->isGuest) { 
		?>Login and create event<?php
			} else {
		?>Create a new event<?php } ?></a>
	</div>
	<span id="event_type_controls">
		<label for="event_types"><strong>Show: </strong></label>
		<select id="event_types">
			<option value="active">Upcoming and ongoing events</option>
			<option value="in_progress">Ongoing events</option>
			<option value="upcoming">Upcoming events</option>
			<option value="archive">Archived events</option>
			<option value="all">All Events</option>
		</select>
	</span>
</div>
<br class="clear" />
<table id="events_list">
	<thead>
		<tr>
			<th class="icon"></th>
			<th class="name">Event</th>
			<th class="date">Date</th>
			<th class="address">Location</th>
			<th class="user">User</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($events as $event) { /* @var $event Event */ ?>
			<?php
				// Calculate type
				$ending_time = $event->endtime ? $event->endtime : $event->starttime + 86399;
				$type = 'active';
				if($event->starttime > $t)
				{
					$type = 'upcoming';	
				}
				elseif( $ending_time < $t )
				{
					$type = 'archived';
				}
			?>
			<tr class="event_<?php echo $type; ?>">
				<td class="icon">
					<?php if($type == 'upcoming') { ?><a href="<?php echo $event->getUrl('ical'); ?>"><?php }?>
					<img src="<?php echo Yii::app()->request->baseUrl, '/images/', $type ?>.ico" alt='<?php echo $type; ?>' title='<?php echo $type; ?>' />
					<?php if($type == 'upcoming') { ?></a><?php }?>
				</td>
				<td class="name"><a href="<?php echo $event->getUrl() ?>"><?php echo $event->name; ?></a></td>
				<td class="date"><?php echo strftime('%B %d, %Y', $event->starttime); ?></td>
				<td class="address"><?php echo strtok($event->location->address,','); ?></td>
				<td class="user"><a href="<?php echo $event->user->url; ?>"><?php echo $event->user->name; ?></a></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<script>
var eventCount = <?php echo $eventCount; ?>;
var activeEventCount = <?php echo $activeEventCount; ?>;
</script>