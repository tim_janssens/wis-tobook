<?php
/* @var $this Controller */
/* @var $data Comment */
?>
<div itemscope="" itemtype="http://schema.org/Comment" class="comment" id="comment_<?php echo $data->comment_id; ?>">
	<div class="comment_avatar">
		<img src="<?php echo $data->user->getAvatarUrl(); ?>" alt="AVA" class="avatar mini" />
	</div>
	<div class="comment_content">
		<div class="comment_data">
			<span itemprop="creator" itemscope="" itemtype="http://schema.org/Person">
				<a itemprop="url" href="<?php echo $data->user->url; ?>">
					<span itemprop="name"><?php echo $data->user->name; ?></span>
				</a>
			</span>
			&middot;
			<span class="comment_date" itemprop="dateCreated"
				title="<?php echo strftime('%A, %b %e, %Y at %l:%M %p', $data->timestamp); ?>">
				<?php echo strftime("%Y-%m-%d",$data->timestamp); ?>
			</span>
			<span class="comment_num">#<?php echo (isset($index) ? $index+1 : '<!-- NUM -->'); ?></span>
		</div>
		<div class="comment_text" itemprop="text">
			<?php echo $data->comment_text; ?>
		</div>
	</div>
</div>