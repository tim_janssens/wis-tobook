<?php
/* @var $this SiteController */
/* @var $model RegisterForm */
/* @var $form Form */
$this->pageTitle = 'Create an account';
?>
<div class="form">

	<?php $form=$this->beginWidget('Form', array(
		'id'=>'register-form',
		'model'=>$model,
		'useAjaxSubmit'=>true,
		'ajaxValidationFields'=>array(
			'username',
		)
	));
	?>
	
		<p class="note">Fields with <span class="required">*</span> are required.</p>
	
		<fieldset>
			<legend>Account Details</legend>
			<?php 
			$form->text('username');
			$form->password('password');
			$form->password('password_confirm');
			$form->text('email');
			?>
		</fieldset>
		
		<fieldset>
			<legend>Personal Details</legend>
			<?php 
			$form->text('first_name');
			$form->text('last_name');
			?>
		</fieldset>
		
		<?php $form->submit('Create Account'); ?>
	
	<?php $this->endWidget(); ?>

</div><!-- form -->