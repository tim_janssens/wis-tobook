<?php
/* @var $this SiteController */
$this->pageTitle='Welcome to ' . Yii::app()->name;
?>
<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk')
);
</script>
 
<div class="main_top">
	<h3>We have <?php echo $totalBooks; ?> books from <?php echo $totalAuthors;?> authors</h3>
</div>
  
<!--
  - The small description
-->   
<table id="intro_table">
	<tr>
		<td>
			<h4>Create your profile</h4>
			<p>You can create your own profile. 
			After creating a profile, you are able to register the books and events, 
			or add books to your profile and show the status to others (like: finished or wishlist). 
			You can also comment on books and read the other people's comments and get the book ratings.</p>
		</td>
		<td>
			<h4>Add a book</h4>
			<p>Add a new book to the database or just take already existing book and 
			link it to your profile. Like this, you will never forgot which books you already 
			read and which you want to read: just add it to your list and see all 
			your linked books on your profile page.</p>
		</td>
		<td>
			<h4>Start an event</h4>
			<p>You can start a new event or join other events to come together 
			and talk about hot topics and new books, meet the authors of your favorite books, 
			or just spend time with book friends. Make your book life more live.</p>
		</td>
	</tr>
</table>

<!--
  - The two lists of top rated books and the newest
-->
<div id="main_div">
   
	<!--
	  - The top 5 rated
	-->
	<div id="top_rated">
		<h3>Our top rated books</h3>
	              
		<table>
			<tr>
				<?php
				$i = 1;
				foreach($top as $row) { /* @var $row Book */
					$bid = $row->book_id;
					?>
					<td<?php echo ($i > 3 ? ' class="too_big"' : ''); ?>>
						<a href="<?php echo $row->url; ?>">
							<strong><?php echo $row->title; ?></strong>
						</a>
						<br />
						<?php echo $row->author; ?>
					</td>
					<?php 
					$i++;
				}
				?>
			</tr>
			
			<tr>
				<?php
				$i = 1;
				foreach($top as $row) { /* @var $row Book */
					$bid = $row->book_id;
					?>
					<td<?php echo ($i > 3 ? ' class="too_big"' : ''); ?>>
						<img src="<?php echo $row->imageUrl; ?>" alt="cover" class="cover_thumb" />
					</td>
					<?php 
					$i++;
				}
				?>
			</tr>
			
			<tr>
				<?php
				$i = 1;
				foreach($top as $row) { /* @var $row Book */
					$bid = $row->book_id;
					?>
					<td<?php echo ($i > 3 ? ' class="too_big"' : ''); ?>>
						<?php echo $row->genre->genre; ?>
						<br />
						<?php echo strftime('%Y-%m-%d', $row->release_date); ?>
					</td>
					<?php 
					$i++;
				}
				?>
			</tr>
		</table>
	</div>
	       
	<!--
	  - the newest 5 books
	-->
	<div id="latest">
		<h3>Our latest books</h3>
		
		<table>
			<tr>
				<?php
				$i = 1;
				foreach($new as $row) { /* @var $row Book */
					$bid = $row->book_id;
					?>
					<td<?php echo ($i > 3 ? ' class="too_big"' : ''); ?>>
						<a href="<?php echo $row->url; ?>">
							<strong><?php echo $row->title; ?></strong>
						</a>
						<br />
						<?php echo $row->author; ?>
					</td>
					<?php 
					$i++;
				}
				?>
			</tr>
			
			<tr>
				<?php
				$i = 1;
				foreach($new as $row) { /* @var $row Book */
					$bid = $row->book_id;
					?>
					<td<?php echo ($i > 3 ? ' class="too_big"' : ''); ?>>
						<img src="<?php echo $row->imageUrl; ?>" alt="cover" class="cover_thumb" />
					</td>
					<?php 
					$i++;
				}
				?>
			</tr>
			
			<tr>
				<?php
				$i = 1;
				foreach($new as $row) { /* @var $row Book */
					$bid = $row->book_id;
					?>
					<td<?php echo ($i > 3 ? ' class="too_big"' : ''); ?>>
						<?php echo $row->genre->genre; ?>
						<br />
						<?php echo strftime('%Y-%m-%d', $row->release_date); ?>
					</td>
					<?php 
					$i++;
				}
				?>
			</tr>
		</table>
	</div>

</div>

<div class="fb-like" data-href="<?php echo Yii::app()->request->url; ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
