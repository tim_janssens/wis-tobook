<?php
/* @var $this SiteController */
/* @var $form Form */
$this->loadCss('jquery.Jcrop.min');
$this->loadJQuery('Jcrop.min');
?>

<div class="form">

	<?php $form=$this->beginWidget('Form', array(
		'id' => 'avatar-form',
		'model' => $model,
		'htmlOptions' => array(
			'enctype'=>"multipart/form-data", // Needed for upload
		),
		'useClientValidation'=>false,
	));
	?>
		<p class="note">Fields with <span class="required">*</span> are required.</p>
	
		<fieldset>
			<legend>Select Image</legend>
			<?php
			// Build the form here
			$form->file('image',array('accept'=>"image/png, image/jpeg"));
			?>
		</fieldset>
		
		<fieldset id="crop_options_fake" style="display: none;">
			<legend>Crop Options</legend>
			<span style="color: red;">Your current browser does not support the HTML5 File API.<br />
			If you wish to use the advanced options, please update your browser.<br />
			You can still upload the image as-is.</span>
		</fieldset>
		
		<fieldset id="crop_options" style="display: none;">
			<legend>Crop Options</legend>
			<?php 
			$form->startRow();
			?>
				<span id="avatar_preview"></span>
			<?php 
			$form->endRow();
			$form->hidden('AvatarForm[x1]');
			$form->hidden('AvatarForm[y1]');
			$form->hidden('AvatarForm[x2]');
			$form->hidden('AvatarForm[y2]');
			$form->startRow('Width');
			?>
				<input type="text" id="AvatarForm_width" disabled="disabled">
			<?php 
			$form->endRow();
			$form->startRow('Height');
			?>
				<input type="text" id="AvatarForm_height" disabled="disabled">
			<?php 
			$form->endRow();
			$form->startRow();
			?>
				<button id="clear_crop" class="button grey">Clear Selection</button>
			<?php 
			$form->endRow();
			?>
		</fieldset>
		
		<?php $form->submit('Save Avatar'); ?>
	
	<?php $this->endWidget(); ?>
	
</div><!-- form -->