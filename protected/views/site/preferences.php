<?php 
/* @var $this SiteController */
/* @var $model PreferencesForm */
/* @var $form Form */
$this->pageTitle = 'Preferences';
$this->initGoogleMaps();
?>
<div class="form">

	<?php $form=$this->beginWidget('Form', array(
		'id'=>'preferences-form',
		'model'=>$model,
		'useAjaxSubmit' => true,
	));
	?>
	
		<p class="note">Fields with <span class="required">*</span> are required.</p>
	
		<fieldset>
			<legend>Personal Information</legend>
			<?php
			$form->text('first_name');
			$form->text('last_name');
			$form->textArea('introduction');
			$form->text('email');
			$form->text('address');
			$form->startRow();
			$form->button('Get Current Location',array('id'=>'btnGeoLocate','style'=>'display:none;'));
			$form->endRow();
			?>
		</fieldset>
		
		<?php $form->submit('Save Personal Information'); ?>
		
	<?php $this->endWidget(); ?>
	
	<br class='clear' />
	
	<?php $form=$this->beginWidget('Form', array(
		'id'=>'password-form',
		'model'=>$passModel,
		'useAjaxSubmit' => true,
	));
	?>
		<fieldset>
			<legend>Change Password</legend>
			<?php
			$form->password('old_password');
			$form->password('password');
			$form->password('password_confirm');
			?>
		</fieldset>
		
		<?php $form->submit('Save Password'); ?>
		
	<?php $this->endWidget(); ?>

</div><!-- form -->