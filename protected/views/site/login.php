<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form Form  */

$this->pageTitle='Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="form">
	<?php $form=$this->beginWidget('Form', array(
		'id'=>'login-form',
		'model'=>$model,
	));
	?>
	
		<p class="note">Fields with <span class="required">*</span> are required.</p>
	
		<p>Don't have an account?
			<a href="<?php echo Yii::app()->urlManager->createUrl('site/register')?>">Create a new account now</a>!
		</p>
		
		<fieldset>
			<?php
			$form->text('username');
			$form->password('password');
			$form->checkbox('rememberMe');
			?>
		</fieldset>
		
		<?php
		if(isset($_GET['referUrl'])) {
			$form->hidden('returnTo',$_GET['referUrl']);
		}
		?>
	
		<?php $form->submit('Login') ?>
	
	<?php $this->endWidget(); ?>
</div><!-- form -->
