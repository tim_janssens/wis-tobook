<?php
/* @var $this BooksController */
$this->pageTitle='Books List';
$this->initDataTables();
?>
<div>
	<span id="books_controls">
		<a href="<?php echo Yii::app()->urlManager->createUrl('book/new'); ?>" class="button">Add a book</a>
		<a href="javascript:void(0);" class="button" id="advanced_filter_toggle" style="display: none;">Advanced filters</a>
	</span>
	<br class="clear" />
</div>

<table id="books_list">
	<thead>
		<tr>
			<th class="image"></th>
			<th class="title">Title</th>
       		<th class="author">Author</th>
       		<th class="genre">Genre</th>
			<th class="release_date">Release date</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($books as $book) { /* @var $book Book */ ?>
        	<tr>
        		<td class="image">
        			<a href="javascript:void(0);" class="show_book_cover" data-isbn="<?php echo $book->isbn; ?>">
        				<img src="<?php echo Yii::app()->request->baseUrl ?>/images/book.ico" alt="" />
        			</a>
        		</td>
				<td class="title"><a href="<?php echo $book->getUrl() ?>"><?php echo $book->title; ?></a></td>
				<td class="author"><?php echo $book->author; ?></td>
          		<td class="genre"><?php echo $book->genre->genre; ?></td>
				<td class="release_date"><?php echo strftime('%B %d, %Y', $book->release_date); ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<br class="clear" />
<div class="form" id="filter_footer" style="display: none;">
	<h2>Advanced filters</h2>
	<div class="row">
		<div class="label">
			<label for="search_title">In title:</label>
		</div>
		<div class="form_input">
			<input type="text" id="search_title" />
		</div>
	</div>
	<div class="row">
		<div class="label">
			<label for="search_title">In author:</label>
		</div>
		<div class="form_input">
			<input type="text" id="search_author" />
		</div>
	</div>
	<div class="row">
		<div class="label">
			<label for="search_title">Genre:</label>
		</div>
		<div class="form_input">
			<?php echo CHtml::dropDownList(
				'search_genre',
				null,
				$this->getAllGenres(),
				array('id'=>'search_genre'));
			?>
		</div>
	</div>
	<div class="row">
		<div class="label">
			<label for="search_date_start">Released after: </label>
		</div>
		<div class="form_input">
			<input type="text" id="search_date_start" />
		</div>
	</div>
	<div class="row">
		<div class="label">
			<label for="search_date_end">Released before: </label>
		</div>
		<div class="form_input">
			<input type="text" id="search_date_end" />
		</div>
	</div>
</div>
<script>
var totalBooks = <?php echo $totalBooks; ?>;
var loadBooksUrl = '<?php echo Util::url('books/ajaxList'); ?>';
</script>
