<?php
/* @var $this Controller */
/* @var $model CommentForm */
/* @var $comments CActiveDataProvider */
Yii::app()->clientScript->registerCssFile(
	Yii::app()->baseUrl . '/css/comments.css'
);
Yii::app()->clientScript->registerScriptFile(
	Yii::app()->baseUrl . '/js/comments.js'
);
?>

<h2>Comments</h2>
<?php $this->widget('zii.widgets.CListView',array(
	'htmlOptions'=>array(
		'class' => 'comments-list',
		'id' => 'comments-list',
	),
	'dataProvider' => $comments,
	'itemView' => '/comment',
	'enablePagination' => false,
	'pager' => array(
		'pageSize' => 99999,
	),
	'template' => '{items}',
	'emptyText' => Yii::app()->user->isGuest ? 'No comments yet' : 'No comments yet, be the first!',
));
?>
<input type="hidden" id="comment_count" value="<?php echo $comments->totalItemCount; ?>" />

<?php if($this->canComment()) { ?>
<span id="add_comment"></span>
<?php $this->renderPartial('/comment-form',array(
		'model'=>$model,
		'formAction'=>$formAction,
	));
}