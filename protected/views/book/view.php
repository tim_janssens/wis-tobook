<?php
/* @var $this BookController */
/* @var $book Book */
/* @var $comments CActiveDataProvider */
$this->pageTitle=$book->title;
?>

<!--
  - Header of book
-->
<div id="book_controls">
	<?php if(!Yii::app()->user->isGuest) { ?>
		<a href="<?php echo $book->getUrl('status'); ?>" class="button <?php if($myStatus) {?>green<?php } else { ?>bluegrey<?php } ?> modal_link" id="status_link">
			<?php if(!$myStatus) { ?>
				Add to my list
			<?php } else { switch($myStatus->status) {
				case 'wishlist':
					echo 'On wishlist (change)';
					break;
				case 'progress':
					echo 'Reading (change)';
					break;
				case 'finished':
					echo 'Finished (change)';
			} } ?>
		</a>
		<a href="<?php echo $book->getUrl('edit'); ?>" class="button">Edit Book</a>
	<?php } ?>
</div>

<div itemscope="" itemtype="http://schema.org/Book">

	<div id="book_info">
		<span class="microdata_info">
			<meta itemprop="url" content="<?php echo Yii::app()->request->hostInfo, $book->url; ?>" />
			<meta itemprop="interactionCount" content="UserComments:<?php echo $comments->itemCount; ?>"/>
		</span>
		
		<div id="book_main_cover">
			<img itemprop="thumbnailUrl" src="<?php echo $book->imageUrl; ?>" alt='cover' />
		</div>
	
		<div id="book_main_info">
			<p><strong>ISBN: </strong> <span itemprop="isbn"><?php echo $book->isbn; ?></span></p>
			<p itemprop="creator" itemscope="" itemtype="http://schema.org/Person"><strong>Author: </strong> <span itemprop="name"><?php echo $book->author; ?></span></p>
			<p><strong>Release Date: </strong> <span itemprop="datePublished"><?php echo strftime('%Y-%m-%d', $book->release_date) ;?></span></p>
			<p><strong>Genre: </strong> <span itemprop="genre"><?php echo $book->genre->genre; ?></span></p>
			<p><strong>Rating: </strong> <span id="book_rating_val"><?php
				if ($rating['votes']) { ?>
				<?php echo round($rating['rating'],2); ?>
				(based on <?php echo $rating['votes']; ?> vote<?php if($rating['votes'] != 1) { ?>s<?php } ?>)
				<?php } else { ?>
				No people have rated this book yet.<?php } 
				?></span></p>
			<?php if(Yii::app()->user->id) { ?>
				<div id="rating-form-container">
					<strong>Your rating: </strong>
					<?php echo CHtml::form($book->getUrl('rate'), 'post', array('id'=>'rating-form'))?>
						<select id='rating' name='rating'>
							<option value="">-Choose-</option>
							<?php for($i = 1; $i < 11; $i++) { ?>
								<option value = "<?php echo $i; ?>"<?php if($myRating == $i) {?>selected="selected"<?php } ?>><?php echo $i; ?></option>
							<?php } ?> 
						</select>
						<input type='submit' value='Update' class="button" />
						<span id="rating-error" class="error"></span>
					<?php echo CHtml::endForm(); ?>
				</div>
			<?php } ?>
		</div>
	</div>
	
	<br class="clear" />
	
	<h2>Blurb</h2>
	<p><?php echo $book->blurb; ?></p>
	
	<!--
	  -Tabs block for book 
	-->
	<div class="book_tabs" id="book_tabs">
		<ul class="tabs" data-persist="true">
			<li><a href="#view1">Comments</a></li>
			<li><a href="#view2">Who Has It?</a></li>
			<li><a href="#view3">Recommended</a></li>
		</ul>
		
		<!--
	      - Comments
	    -->
		<div id="view1">
			<?php
			$this->renderPartial('/comments',array(
				'comments' => $comments,
				'model' => $model,
				'formAction' => $book->getUrl('comment'),
			));
			?> 
		</div>
		
		<!--
		  -   Who has it!
		-->
		<div id="view2">
	    	<?php if(!$statuses) { ?>
				<p>No people have added this book yet.</p>
	        <?php
				} else {
	        		$i = 0;
					foreach($book->users as $user) { /* @var $user User */ 
						//To get statuss from user_book
						if(	$statuses[$i]['book_id'] == $book->book_id
							&& $statuses[$i]['user_id'] == $user->user_id ){
							$status=$statuses[$i]['status'];
						}
						else
						{
							continue;
						}
						switch($status)
						{
							case "wishlist":
								$status = 'On wishlist';
								break;
							case 'progress':
								$status = 'Currently reading';
								break;
							case 'finished':
								$status = 'Finished reading';
								break;
						}
			?>
			<div class="users_row">
				<div class="users_avatar_container">
					<img src="<?php echo $user->getAvatarUrl(); ?>" class="avatar mini" alt='AVA' />
				</div>
				<div class="users_info_container">
					<a href="<?php echo $user->url; ?>"><?php echo $user->name; ?></a><br />
					<?php echo $status; ?>
				</div>
			</div>
			<?php 
				$i++;
				}}
			?>              
		</div>
	          
		<!--
		  - Recommendations
		-->
		<div id="view3">
			<?php if(count($recommended)) { ?>
		        <?php foreach($recommended as $r) { /* @var $r Book */?>
		        	<div class="recommendation">
		        		<div class="recom_cover">
		        			<img src="<?php echo $r->imageUrl ?>" alt='cover' />
		        		</div>
		        		<div class="recom_info">
		        			<p><strong><a href="<?php echo $r->url; ?>"><?php echo $r->title; ?></a></strong></p>
		        			<p><strong>By </strong><?php echo $r->author; ?></p>
		        			<p><strong>Released </strong><?php echo strftime('%Y-%m-%d', $r->release_date); ?></p>
		        		</div>
		        	</div>
		        	<hr />
		        <?php } ?>
	        <?php } else { ?>
	        	We do not currently have any recommendations for this book.
	        <?php } ?>
		</div>
	</div>

</div>