<?php 
/* @var $this BookController */
/* @var $book Book */
/* @var $model StatusForm */
/* @var $form Form */
?>
<div class="form">

<?php $form=$this->beginWidget('Form', array(
	'id'=>'status-form',
	'model'=>$model,
	'useAjaxSubmit' => true,
	'ajaxSuccessFunction' => 'statusSuccess',
));
?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<fieldset>
		Select new status for <i><?php echo $book->title; ?></i>
		<?php
		// Build the form here
		$form->dropdown('status',$model->getAllStatuses()); // Creates a text field for $some_var in the model
		?>
	</fieldset>
	
	<?php $form->submit('Save Status'); ?>
	
<?php $this->endWidget(); ?>

</div><!-- form -->