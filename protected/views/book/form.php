<?php
/* @var $this BookController */
/* @var $model BookForm */
/* @var $form Form */
?>

<div class="form">

	<?php $form=$this->beginWidget('Form', array(
		'id' => 'book-form',
		'model' => $model,
		'useAjaxSubmit' => true,
		'validationSuccessFunctions' => array(
			'isbn' => 'isbnValidated',
		),
	));
	?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<fieldset>
		<legend>Book Details</legend>
		Enter the ISBN number of the book if know. If not, you can use the title and author fields to search for a specific book.
		
		<?php
		$form->startRow('ISBN <span class="required">*</span>');
		$form->text('isbn'); //BookForm_isbn
		echo '<br />';
		$form->error('isbn');
		?>
		<div id="isbn_result" style="display: none;">
			<a href="javascript:void(0);" id="isbn_result_link"></a>
		</div>
		<?php 
		$form->endRow();
		$form->text('title');
		$form->text('author');
		$form->date('release_date');
		$form->dropdown('genre',$model->getAllGenres());  //How to insert genre
		$form->textArea('blurb');
		$form->dropdown('status',$model->getAllStatus());
		?>
	</fieldset>
  
	<?php $form->submit('Save Book'); ?>
	
	<?php $form->addJs('bookForm = ' . $form->getFormName() . ';'); ?>
  
	<?php $this->endWidget(); ?>
 
</div>
