<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19">
	<section id="content">
		<?php echo $content; ?>
	</section><!-- content -->
</div>
<div class="span-5 last">
	<aside id="sidebar">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
	</aside><!-- sidebar -->
</div>
<?php $this->endContent(); ?>