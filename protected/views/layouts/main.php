<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.min.css" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	
	<script>
	var loggedIn = <?php echo (Yii::app()->user->isGuest ? 'false' : 'true'); ?>; 
	<?php if(Yii::app()->user->id) { ?>var userId = <?php echo Yii::app()->user->id; ?>;<?php } ?> 
	var baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>/';
	var apiKey = '<?php echo Yii::app()->params['api_key']; ?>';
	</script>
	
	<?php
	Yii::app()->clientScript->registerCoreScript('jquery');
	Yii::app()->clientScript->registerScriptFile(
		Yii::app()->baseUrl . '/js/global.js'
	);
	Yii::app()->clientScript->registerScriptFile(
		Yii::app()->baseUrl . '/js/forms.js'
	);
	Yii::app()->clientScript->registerScriptFile(
		Yii::app()->baseUrl . '/js/jquery/jquery-ui.min.js'
	);
	?>
	
	<title><?php echo CHtml::encode($this->pageTitle); ?> - ToBook</title>
</head>

<body itemscope="" itemtype="http://schema.org/WebPage">

<div class="container" id="page">

	<header id="site_header">
		<div id="header_content">
			<div id="logo"><a href="<?php echo Yii::app()->urlManager->baseUrl ?>/"><?php echo CHtml::encode(Yii::app()->name); ?></a></div>
	
			<div id="mainmenu">
				<?php $this->widget('zii.widgets.CMenu',array(
					'encodeLabel' => false,
					'items'=>array(
						array(
							'label' => 'Home',
							'url' => array('site/index'),
							'itemOptions' => array(
								'id'=>'home_link',
							),
						),
						array(
							'label'=>'Books',
							'url'=>array('/books'),
							'active'=>in_array($this->id,array('book','books')),
							'itemOptions' => array(
								'id'=>'books_link',
							),
						),
						array(
							'label'=>'Events',
							'url'=>array('/events'),
							'active'=>in_array($this->id,array('event','events')),
							'itemOptions' => array(
								'id'=>'events_link',
							),
						),
						array(
							'label' => 'Login/Register',
							'url' => array('site/login'),
							'itemOptions' => array(
								'id' => 'user_controls',
								'class' => 'logged_out',
							),
							'linkOptions' => array(
								'class' => 'modal_link',
							),
							'active' => false,
							'visible' => Yii::app()->user->isGuest,
						),
						array(
							'label'=>'<img src="'
								.Yii::app()->user->avatarUrl
								.'" class="avatar mini" alt="AVA" /> <span id="username">'
								.Yii::app()->user->name
								.'</span>',
							'itemOptions' => array(
								'id' => 'user_controls'
							),
							'visible' => !Yii::app()->user->isGuest,
							'submenuOptions' => array(
								'class' => 'mainmenu-dropdown',
								'id' => 'user_dropdown',
								'style' => 'display: none',
							),
							'items' => array(
								array(
									'label' => 'Books',
									'url' => array('/books'),
									'itemOptions' => array(
										'class' => 'mobile-only',
									),
								),
								array(
									'label' => 'Events',
									'url' => array('/events'),
									'itemOptions' => array(
										'class' => 'mobile-only',
									),
								),
								array(
									'label' => 'Profile',
									'url' => array(
										'user/view',
										'id' => Yii::app()->user->id,
										'title' => Util::slug(Yii::app()->user->username),
									),
								),
								array(
									'label' => 'Preferences',
									'url' => array('site/preferences'),
								),
								array(
									'label' => 'Change Avatar',
									'url' => array('site/avatar-prefs'),
								),
								array(
									'label' => 'Logout',
									'url' => array('site/logout'),
								),
							),
						),
					),
				)); ?>
			</div><!-- mainmenu -->
		</div><!-- header content -->
	</header><!-- header -->
	
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<footer id="footer">
		<?php echo Yii::powered(); ?>
	</footer><!-- footer -->
	
	<div id="ajax-loading-main" class="ajax-loading" style="display:none;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ajax-throbber.gif" alt="AJAX" /></div>

	<div id="main_modal"></div>
</div><!-- page -->

</body>
</html>
