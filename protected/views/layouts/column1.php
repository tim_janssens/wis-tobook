<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<section id="content">
	<h1><?php echo CHtml::encode($this->pageTitle); ?></h1>
	<?php echo $content; ?>
</section><!-- content -->
<?php $this->endContent(); ?>