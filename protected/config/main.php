<?php

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'ToBook',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	// modules
	'modules'=>array(),

	// application components
	'components'=>array(
		// User component
		'user'=>array(
			'class'=>'WebUser',
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		
		// URL manager: manages seo friendly URLs
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				// Without action -> default to "view" action
				'<controller:(book|event|user)>/<id:\d+>-<title>'=>'<controller>/view',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				// With action
				'<controller:(book|event|user)>/<id:\d+>-<title>/<action:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<id:\d+>/<action:\w+>'=>'<controller>/<action>',
				// Without ID
				'site/avatar-prefs'=>'site/avatarPrefs',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'showScriptName' => false, // Hides "index.php"
		),
		
		// Database (see database.php)
		'db'=>require('database.php'),
		
		// Error Handler
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
		// Request component
		'request' => array(
			'enableCsrfValidation' => true,
		),
		
		// Logging Component (debug)
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				//*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'api_key'=>'AIzaSyALyjMbFrqdyhW0JuBCuoXVXd7vr5ie53I',  // Google API key, can be used for all google APIs
	),
);
