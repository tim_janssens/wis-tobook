SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `tobook` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `tobook` ;

-- -----------------------------------------------------
-- Table `tobook`.`genre`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`genre` (
  `genre_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `genre` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`genre_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `tobook`.`book`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`book` (
  `book_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(100) NOT NULL ,
  `blurb` TEXT NULL ,
  `author` VARCHAR(60) NULL ,
  `user_count` INT(10) UNSIGNED NOT NULL ,
  `release_date` INT(10) UNSIGNED NOT NULL ,
  `genre_id` INT(10) UNSIGNED NOT NULL ,
  `isbn` VARCHAR(13) NOT NULL ,
  PRIMARY KEY (`book_id`) ,
  INDEX `fk_book_genre` (`genre_id` ASC) ,
  UNIQUE INDEX `isbn_UNIQUE` (`isbn` ASC) ,
  CONSTRAINT `fk_book_genre`
    FOREIGN KEY (`genre_id` )
    REFERENCES `tobook`.`genre` (`genre_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `tobook`.`location`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`location` (
  `location_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `address` VARCHAR(255) NOT NULL ,
  `address_hash` CHAR(32) NOT NULL COMMENT 'Contains the MD5 hashed version of the address. Used for searching.' ,
  `lat` FLOAT(10,6) NOT NULL ,
  `lng` FLOAT(10,6) NOT NULL ,
  PRIMARY KEY (`location_id`) ,
  INDEX `idx_address_hash` (`address_hash` ASC) ,
  UNIQUE INDEX `address_hash_UNIQUE` (`address_hash` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `tobook`.`user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`user` (
  `user_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(45) NOT NULL ,
  `passwd` BINARY(60) NOT NULL ,
  `register_date` INT(10) UNSIGNED NOT NULL ,
  `location_id` INT(10) UNSIGNED NULL ,
  `introduction` TEXT NULL ,
  `first_name` VARCHAR(45) NOT NULL ,
  `last_name` VARCHAR(45) NOT NULL ,
  `email` VARCHAR(255) NOT NULL ,
  `roles` BLOB NOT NULL ,
  `avatar_date` INT(10) UNSIGNED NULL ,
  PRIMARY KEY (`user_id`) ,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) ,
  INDEX `fk_user_location` (`location_id` ASC) ,
  CONSTRAINT `fk_user_location`
    FOREIGN KEY (`location_id` )
    REFERENCES `tobook`.`location` (`location_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `tobook`.`user_book`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`user_book` (
  `user_id` INT(10) UNSIGNED NOT NULL ,
  `book_id` INT(10) UNSIGNED NOT NULL ,
  `status` ENUM('wishlist','progress','finished') NULL ,
  PRIMARY KEY (`user_id`, `book_id`) ,
  INDEX `fk_UserBook_Book` (`book_id` ASC) ,
  INDEX `fk_UserBook_User` (`user_id` ASC) ,
  CONSTRAINT `fk_UserBook_Book`
    FOREIGN KEY (`book_id` )
    REFERENCES `tobook`.`book` (`book_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserBook_User`
    FOREIGN KEY (`user_id` )
    REFERENCES `tobook`.`user` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
COMMENT = 'Contains data about the books owned by a user.';


-- -----------------------------------------------------
-- Table `tobook`.`event`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`event` (
  `event_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `location_id` INT(10) UNSIGNED NOT NULL ,
  `user_id` INT(10) UNSIGNED NOT NULL ,
  `description` TEXT NOT NULL ,
  `starttime` INT(10) UNSIGNED NOT NULL ,
  `endtime` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`event_id`) ,
  INDEX `fk_Event_Location` (`location_id` ASC) ,
  INDEX `fk_event_user1` (`user_id` ASC) ,
  CONSTRAINT `fk_Event_Location`
    FOREIGN KEY (`location_id` )
    REFERENCES `tobook`.`location` (`location_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `tobook`.`user` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `tobook`.`comment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`comment` (
  `comment_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `comment_text` TEXT NOT NULL ,
  `user_id` INT(10) UNSIGNED NOT NULL ,
  `item_id` INT(10) UNSIGNED NOT NULL ,
  `item_type` ENUM('book','event') NOT NULL ,
  `timestamp` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`comment_id`) ,
  INDEX `fk_Comment_User` (`user_id` ASC) ,
  INDEX `item` (`item_id` ASC, `item_type` ASC) ,
  CONSTRAINT `fk_Comment_User`
    FOREIGN KEY (`user_id` )
    REFERENCES `tobook`.`user` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `tobook`.`rating`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`rating` (
  `rating_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `rating` TINYINT(3) UNSIGNED NOT NULL ,
  `user_id` INT(10) UNSIGNED NOT NULL ,
  `book_id` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`rating_id`) ,
  INDEX `fk_Rating_User` (`user_id` ASC) ,
  INDEX `fk_Rating_Book` (`book_id` ASC) ,
  CONSTRAINT `fk_Rating_User`
    FOREIGN KEY (`user_id` )
    REFERENCES `tobook`.`user` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Rating_Book`
    FOREIGN KEY (`book_id` )
    REFERENCES `tobook`.`book` (`book_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `tobook`.`rsvp`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `tobook`.`rsvp` (
  `user_id` INT(10) UNSIGNED NOT NULL ,
  `event_id` INT(10) UNSIGNED NOT NULL ,
  PRIMARY KEY (`user_id`, `event_id`) ,
  INDEX `fk_rsvp_user` (`user_id` ASC) ,
  INDEX `fk_rsvp_event` (`event_id` ASC) ,
  CONSTRAINT `fk_rsvp_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `tobook`.`user` (`user_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rsvp_event`
    FOREIGN KEY (`event_id` )
    REFERENCES `tobook`.`event` (`event_id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `tobook`.`genre`
-- -----------------------------------------------------
START TRANSACTION;
USE `tobook`;
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (1, 'Biography');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (2, 'Drama');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (3, 'Essay');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (4, 'Fable');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (5, 'Fairy tale');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (6, 'Fantasy');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (7, 'Fiction in verse');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (8, 'Fiction narrative');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (9, 'Folklore');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (10, 'Historical fiction');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (11, 'Horror');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (12, 'Humor');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (13, 'Legend');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (14, 'Mystery');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (15, 'Mythology');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (16, 'Narrative nonfiction');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (17, 'Poetry');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (18, 'Realistic fiction');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (19, 'Science fiction');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (20, 'Short story');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (21, 'Speech');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (22, 'Tall tale');
INSERT INTO `tobook`.`genre` (`genre_id`, `genre`) VALUES (23, 'Textbook');

COMMIT;
