-- Simple SQL script to truncate all tables in the database.

SET foreign_key_checks = 0;
TRUNCATE TABLE book;
TRUNCATE TABLE comment;
TRUNCATE TABLE event;
TRUNCATE TABLE genre;
TRUNCATE TABLE location;
TRUNCATE TABLE rating;
TRUNCATE TABLE rsvp;
TRUNCATE TABLE user;
TRUNCATE TABLE user_book;
SET foreign_key_checks = 1;