"use strict";

// Array of available forms.
var forms = [];

// Variable to prevent multiple AJAX calls at the same time
var ajaxInProgress = false;

/**
 * Defines the form "class".
 * 
 * @param {string} formId ID of the form HTML element.
 * @param {array} options The following options are supported:<ul>
 *  <li><b>ajaxSubmit:</b> Enables AJAX submission of the form.</li>
 *  <li><b>ajaxSuccess:</b> A function callback to be executed when the
 *   form submission was successful. This function is not executed when
 *   there are form errors or when the user is redirected.</li>
 *  <li><b>validateSubmit:</b> Whether validation should be run prior to
 *   submitting the form. If enabled, the form will not submit unless
 *   there are no validation errors. Note that the server may have more
 *   restrictive validation, so the form may still have errors there.</li></ul>
 */
function Form(formId, options)
{
	var form = $("#"+formId);
	
	if(!form.length)
	{
		return; // Form doesn't exist.
	}
	
	if( undefined == options ) { options = {}; }
	
	var ajaxSubmit = ( 'ajaxSubmit' in options ? options['ajaxSubmit'] : false );
	var validateSubmit = ( 'validateSubmit' in options ? options['validateSubmit'] : true );
	var ajaxSuccess = ( 'ajaxSuccess' in options ? options['ajaxSuccess'] : false );
	
	this.formId = formId;
	this.action = form.attr('action') || '';
	this.method = form.attr('method') || 'POST';
	this.fields = {};
	
	// Register yourself in the forms array
	forms[this.formId] = this;
	
	var that = this;
	
	if(ajaxSubmit)
	{
		$("#"+formId).submit( function() {
			return that.ajaxSubmit(!validateSubmit);
		});
		if(ajaxSuccess)
		{
			this.onAjaxSuccess = ajaxSuccess;
		}
	}
	else if(validateSubmit)
	{
		$("#"+formId).submit( function() {
			return that.validateAll();
		});
	}
};

/**
 * Adds a field to the form.
 * 
 * @param {string|JQuery} fieldId ID of the field.
 * @param {array} options An array of options. Following options are supported:<ul>
 *  <li><b>validation:</b> Validation function (optional). This function should
 *   take the following parameters: fieldId (the ID of the field to validate),
 *   value (the value of the field), messages (array of error messages. The
 *   function should push additional messages here if needed), attribute (name
 *   of the attribute in the model, used for AJAX validation), skipAjax (whether
 *   or not AJAX should be skipped)</li>
 *  <li><b>validationSuccess:</b> Function to be executed when validation is
 *   successful.
 *  <li><b>errorId:</b> ID of the html element containing the error message.</li>
 *  <li><b>attribute:</b> Name of the attribute in the form model. Will be
 *   passed to the validation function.</li>
 *  <li><b>disableBlurValidation:</b> By default, this function will assign the
 *   validator to run when the input field is blurred. Set this option to
 *   true to prevent this. Note that only text inputs and select boxes have
 *   blur validation, it has no effect on other input types.</li></ul>
 */
Form.prototype.addField = function(fieldId, options)
{
	var field = $("#"+fieldId);
	
	if(!field)
	{
		return; // Field doesn't exist...
	}
	
	// OPTIONS
	if(undefined === options) { options = {}; }
	
	if(!('validation' in options) || typeof options['validation'] != 'function' )
	{
		options['validation'] = false;
		options['validationSuccess'] = false;
	}
	else
	{
		if(!('validationSuccess' in options)) { options['validationSuccess'] = false; }
	}
	
	if(!('errorId' in options)) { options['errorId'] = fieldId + '_em_'; }
	if(!('attribute' in options)) { options['attribute'] = false; }
	if(!('disableBlurValidation' in options)) { options['disableBlurValidation'] = false; }
	
	// Register the field
	this.fields[fieldId] = options;
	
	// Automatic Blur Validation
	if(options['validation'] && !options['disableBlurValidation'])
	{
		var nodeName = field.prop('nodeName');
		var that = this;
		
		switch(nodeName)
		{
			case 'INPUT':
			case 'TEXTAREA':
				field.blur( function() {
					that.validate($(this));
				});
				break;
			case 'SELECT':
				field.change( function() {
					that.validate($(this));
				});
				break;
		}
	}
};

/**
 * Validates a field.
 * 
 * @param {string|JQuery} field The field to be validated. Note that this is the field itself,
 *  not the ID of the field.
 * @param {Boolean} forSubmit This boolean has to be set to true when the validation
 *  is run before the form is being submitted. When true, all AJAX validation is
 *  skipped (as we have server side validation on submit regardless) and validation
 *  success functions will not be executed.
 */
Form.prototype.validate = function(field, forSubmit)
{
	if( undefined === forSubmit ) { forSubmit = false; }
	
	var fieldId;
	if(typeof field == 'string')
	{
		fieldId = field;
		field = $("#"+fieldId);
	}
	else
	{
		// Assume it's the field itself.
		fieldId = field.attr('id');
	}
	
	if(!(fieldId in this.fields))
	{
		return true; // Field not registered...
	}
	
	var validator = this.fields[fieldId]['validation'];
	if(!validator)
	{
		return true; // Field has no validator...
	}
	
	var messages = [];
	
	// Get current value
	var val;
	if(field.prop('nodeName') == 'SPAN')
	{
		// Radiobuttons & checkboxes are grouped in a span
		// So we need to get the value of the underlying input element
		val = $('#' + fieldId + ' input:checked').val();
	}
	else
	{
		val = field.val();
	}
	
	// Run validator
	validator(fieldId,val,messages,forSubmit);
	
	// Results...
	if(messages.length)
	{
		this.setError(fieldId, messages[0]);
		return false;
	}
	else
	{
		this.clearError(fieldId);
		
		if(!forSubmit)
		{
			// Validation success function
			var s = this.fields[fieldId]['validationSuccess'];
			if(s)
			{
				s(field, this);
			}
		}
		return true;
	}
};

/**
 * Validates all fields of the form.
 * 
 * @returns {Boolean} False if any error occurred, true otherwise
 */
Form.prototype.validateAll = function()
{
	var ret = true;
	for( var fieldId in this.fields )
	{
		if(!this.validate(fieldId,true))
		{
			ret = false;
		}
	}
	return ret;
};

/**
 * Sets an error message to a field.
 * 
 * @param {string|jQuery} field The field or field ID to set the error for.
 * @param {string} errorMessage The error message to set.
 */
Form.prototype.setError = function(field, errorMessage)
{
	var fieldId;
	if(typeof field == 'string')
	{
		fieldId = field;
		field = $("#"+fieldId);
	}
	else
	{
		// Assume it's the field itself.
		fieldId = field.attr('id');
	}
	
	var errorId = this.fields[fieldId]['errorId'];
	
	field.parent().parent().addClass('error');
	if(errorId)
	{
		$('#'+errorId).html(errorMessage).show();
	}
};

/**
 * Removes the error message of a field.
 * 
 * @param {string|jQuery} field The field or field ID to remove the error for.
 */
Form.prototype.clearError = function(field)
{
	var fieldId;
	if(typeof field == 'string')
	{
		fieldId = field;
		field = $("#"+fieldId);
	}
	else
	{
		// Assume it's the field itself.
		fieldId = field.attr('id');
	}
	
	var errorId = this.fields[fieldId]['errorId'];
	
	field.parent().parent().removeClass('error');
	if(errorId)
	{
		$('#'+errorId).hide();
	}
};

/**
 * Validates a form field through AJAX
 * 
 * @param {string|jQuery} field The field to validate
 * @param {string} value Current value of the field
 * @param {array} messages Error messages array. If any validation errors occur,
 *  this array is filled with the messages.
 * @param {string} attribute Name of the attribute in the Form Model.
 */
Form.prototype.ajaxValidate = function(field,value,messages)
{	
	var fieldId;
	if(typeof field == 'string')
	{
		fieldId = field;
	}
	else
	{
		// Assume it's the field itself.
		fieldId = field.attr('id');
	}
	
	var form = $("#"+this.formId);
	
	var attribute = this.fields[fieldId]['attribute'];
	
	if(!attribute)
	{
		return; // We need an attribute for this to work
	}
		
	var that = this;
	
	// Do the AJAX request
	$.ajax({
		dataType: "json",
		url: form.attr('action'),
		data: {
			'ajax_validate_attribute': attribute,
			'ajax_validate_value': value,
		},
		async: true,
		cache: false,
		type: 'GET',
		success: function(result)
		{
			// Validation errors
			if(result.status == AJAX_STATUS_ERROR)
			{
				that.setError(fieldId,result.validationErrors[fieldId]);
			}
			// No validation errors
			else
			{
				that.clearError(fieldId);
			}
		},
	});
};

/**
 * Submits the form through ajax.
 * 
 * @param {Boolean} skipValidation Whether validation should be skipped prior to
 *  submitting the form. If validation is enabled, the form is only
 *  submitted to the server when there are no errors. Ajax validation
 *  is skipped and will instead be done by the submit itself.
 *  
 * @returns {Boolean} Returns false to prevent default form submit action.
 */
Form.prototype.ajaxSubmit = function(skipValidation)
{		
	// Prevent multiple forms from being submitted, or from submitting twice.
	if(ajaxInProgress)
	{
		return false;
	}
	
	ajaxInProgress = true;
	$('.form_submit').addClass('grey');
	
	// Validation
	if(undefined === skipValidation) { skipValidation = false; }
	
	if(!skipValidation && !this.validateAll())
	{
		$('.form_submit').removeClass('grey');
		ajaxInProgress = false;
		return false; // Validation failed
	}
	
	var that = this;
	
	// Do the AJAX request
	$.ajax({
		dataType: "json",
		url: this.action,
		data: $("#"+this.formId).serialize(),
		cache: false,
		type: this.method,
		success: function(result)
		{
			// Validation Errors
			if(result.status == AJAX_STATUS_ERROR)
			{
				for(var att in result.validationErrors)
				{
					that.setError(att, result.validationErrors[att]);
				}
			}
			// Redirect
			else if(result.status == AJAX_STATUS_REDIRECT)
			{
				$("#" + that.formId + " .ajax-loading")
					.html('<span style="color:green;">Submit successful, redirecting...</span>');
				window.location.href = result.redirectUrl;
			}
			// Custom success function
			else if(that.onAjaxSuccess)
			{
				that.onAjaxSuccess(result,that);
			}
		},
		// An error on the server side. Handle this cleanly by putting up a modal.
		error: function(result)
		{
			var txt = null;
			if(undefined !== result.responseText)
			{
				txt = result.responseText;
			}
			else
			{
				txt = '<h1>Unknown Error</h1>An error occurred. Please try again.';
			}
			openModal(txt);
		}
	});
	
	ajaxInProgress = false;
	$('.form_submit').removeClass('grey');
	return false;
};