"use strict";

$(document).ready( function() {
	// Use Google Maps autocomplete for the address field.
	new google.maps.places.Autocomplete(
		document.getElementById('EventForm_location_address')
	);

	// If enter is pressed on the address field, don't submit the form
	$('#EventForm_location_address').keypress(function(e) {
		if (e.which == 13) {
			return false;
		}
	});
});