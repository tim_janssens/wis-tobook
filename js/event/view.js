"use strict";

/**
 * Loads the map on a specific location.
 * 
 * @param address Address on which the map should be centered
 * @param name Name of the location (used for popup text)
 */
function loadMap(address)
{
	// Some map options...
	var mapOptions = {
		'zoom': 14,
	};
	
	// Decode the address to Coordinates
	new google.maps.Geocoder().geocode({
		'address' : address,
	}, function(results, status) {
		// If all is OK
	    if (status == google.maps.GeocoderStatus.OK) {
	    	// Center on the location
			mapOptions['center'] = results[0].geometry.location;
			
			// Create the map
			var map = new google.maps.Map(document.getElementById('event_location_map'),mapOptions);
			
			// Add marker on the correct spot
			new google.maps.Marker({
	            map: map,
	            position: results[0].geometry.location,
	            title: address.split(',')[0],
	        });
	    }
	});

}

/**
 * Handle the RSVP form result
 * 
 * @param result The JSON result sent back from the server
 */
function handleRsvp(result)
{
	// Are we still RSVP-ing?
	if(result.has_rsvp)
	{
		// Hide "No people attending" message (currently unused since the owner always attends)
		$("#no_rsvps").hide();
		
		// Add me to the list
		$("<li>")
			.addClass('event_rsvp')
			.attr('id','rsvp_'+result.rsvp.id)
			.html('<img src="' + result.rsvp.avatar + '" class="avatar mini" /> <span>' + result.rsvp.name + '</span>')
			.appendTo("#rsvp_list");
		
		// Recolor the RSVP button
		$("#rsvp_button").addClass("green");
	}
	else
	{
		// Remove me from the list
		$("#rsvp_" + result.rsvp.id).remove();
		
		// Re-add the "No people attending" message if needed (aka never due to owner always attending)
		if(!$(".event_rsvp").length)
		{
			$("#no_rsvps").show();
		}
		
		// Recolor the RSVP button
		$("#rsvp_button").removeClass("green");
	}
}

$(document).ready( function() {
	if(loggedIn)
	{
		// Init RSVP form with the Forms system
		new Form('rsvp-form',{
			'ajaxSubmit': true,
			'ajaxSuccess': handleRsvp,
		});
	}
});