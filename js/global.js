"use strict";

// Is the menu (on the top right when clicking your name) open?
var menuOpen = false;

// The active modal
var activeModal = null;

// Function to call when the modal is "saved"
var modalSave = null;

/**
 * Hides the user menu if it is open
 */
function hideMenu() {
	if(menuOpen)
	{
		menuOpen = false;
		$(document).unbind( 'click', hideMenu );  // Unbind the "click anywhere to close" listener
		$("#user_dropdown").slideUp(30); // Hide the menu
	}
} 

//---------------------------------
// TinyMCE Config
//---------------------------------
var mceConfig = {
	plugins: 'autolink link preview textcolor code',
	
	toolbar: 'bold italic underline strikethrough removeformat | link unlink | forecolor | code preview',
	menubar: false,
	
	toolbar_items_size: 'small',
	forced_root_block: false,

	valid_elements: 'a[href|target=_blank],b,i,u,s,p,strong,em,ins,del,br,span[style]',
};

//---------------------------------
// Modals
//---------------------------------
var modalLoaded = false;

/**
 * Opens a new Modal
 * 
 * @param {string} content Content of the modal
 * @param {string} width Width of the modal (defaults to 80%)
 */
function openModal(content,width)
{
	if(!modalLoaded)
	{
		$('#main_modal').dialog({
			autoOpen: false,
			modal: true,
			width: (undefined === width ? '80%' : width),
			buttons: {
				Ok: function() {
					$( this ).dialog('close');
				}
			}
		});
		modalLoaded = true;
	}
	else if(undefined !== width)
	{
		$('#main_modal').dialog('option','width',width);
	}
	
	$('#main_modal').html(content).dialog('open');
}

//---------------------------------
// AJAX config
//---------------------------------
var AJAX_STATUS_SUCCESS = 'success';
var AJAX_STATUS_ERROR = 'error';
var AJAX_STATUS_REDIRECT = 'redirect';

$( document ).ajaxStart(function() {
	$( ".ajax-loading" ).show();
});

$( document ).ajaxComplete(function() {
	$( ".ajax-loading" ).hide();
});


$(document).ready( function(){
	
	if(loggedIn)
	{
		//---------------------------------
		// User dropdown
		//---------------------------------
		$("#user_controls").click( function() {
			if(!menuOpen)
			{
				menuOpen = true;
				$(document).bind( 'click', hideMenu );  // Add "click anywhere to close" listener
				$("#user_dropdown").slideDown(30);  // Open the menu
				return false;
			}
		});
	}
	
	//----------------------------------
	// Modal link - open ANY page in a modal
	//----------------------------------
	$('.modal_link').click( function(e) {
		// Do AJAX
		e.preventDefault();
		
		$.ajax({
			dataType: "json",
			url: $(this).attr('href'),
			async: true,
			cache: false,
			data: {
				'modal': true,
				'referUrl': window.location.href,
			},
			type: 'GET',
			success: function(result)
			{
				var content = '';
				if(result.status == 'error')
				{
					content = result.message;
				}
				else if(result.modal)
				{
					content = result.modal;
				}
				
				activeModal = $('<div>')
					.addClass('modal')
					.attr('title',result.title)
					.appendTo('body')
					.html( content );
				
				var btns = {};
				
				if(result.button)
				{
					btns[result.button] = modalSave;
				}
				
				btns['Close'] = function() {
					activeModal.dialog('destroy').remove();
				};
											
				// Create a modal
				activeModal
					.appendTo('body')
					.dialog({
						autoOpen: true,
						modal: true,
						width: '80%',
						buttons: btns,
					});
			},
		});
	});
});