"use strict";

$(document).ready( function() {
	
	// Init DataTable
	var oTable = $("#events_list").dataTable({
		// Use jQuery UI styles
		"bJQueryUI": true,
		
		// Show 5 pages in pagination
		"iShowPages": 5,
		
		// Pagination type (numbers, default is just next/prev)
		"sPaginationType": "full_numbers",
		
		// Columns
		"aoColumnDefs": [{
			"aTargets": [ 0 ],    // Column number(s)
			"mDataProp": "icon",  // Data property (use to extract server data)
			"sClass": "icon",     // Class to add to table cells
			"sWidth": '25px',     // Width of the column
			"iDataSort": 2,       // When sorted, actually sort on another column
		},{
			"aTargets": [ 1 ],
			"mDataProp": "name",
			"sClass": "name",
			"sWidth": '25%',
		},{
			"aTargets": [ 2 ],
			"mDataProp": "date",
			"sClass": "date",
			"sWidth": '20%',
		},{
			"aTargets": [ 3 ],
			"mDataProp": "location",
			"sClass": "location",
			"sWidth": '25%',
		},{
			"aTargets": [ 4 ],
			"sClass": "user",
			"mDataProp": "user",
		}],
		
		// Default sorting
		"aaSorting": [[ 2, "asc" ]],
		
		// Disable auto width (needed for responsive design)
		"bAutoWidth": false,
		
		// Show processing icon
		"bProcessing": true,
		
		// Process on the server side
		"bServerSide": true,
		
		// Source to load data from through AJAX
		"sAjaxSource": baseUrl + 'events/ajaxList',
		
		// Additional parameters to send to server (e.g. value of the select box)
		"fnServerParams": function(aoData) {
			aoData.push({'name': 'event_types', 'value': $('#event_types').val()});
		},
		
		// Don't load from AJAX on first page load (also tells the table how many rows there are currently)
		"iDeferLoading": [ activeEventCount, eventCount ],
		
		// Function to call when a row is inserted into the table
		"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
			// Add a class based on the date type (archived, ongoing, upcoming)
           	$(nRow).addClass('event_'+aData.type);
        },
	});
	
	// Delays search slightly in order to not overload the server
	oTable.fnSetFilteringDelay();
	
	// Redraws the table if the select box is changed
	$("#event_types").change( function(){
		oTable.fnDraw();
	});
	
});