"use strict";

var tmpResult = null;
var bookForm = null;

/**
 * Auto fill data
 * 
 * Automatically fills in the form based on the information from the tmpResult
 *  variable. You should set this veriable before calling this function.
 * 
 * @param fromAc Set this to true if called from a jQuery Autocomplete.
 */
function autoFill(fromAc)
{
	if( undefined === fromAc ) { fromAc = false; }
	
	if(!tmpResult)
	{
		return;
	}
	var book = tmpResult;
	if(fromAc)
	{
		$('#BookForm_isbn').val(book.industryIdentifiers[0].identifier);
	}
	$('#BookForm_title').val(book.title);
	$('#BookForm_author').val(book.authors ? book.authors[0] : '');  // There can be multiple authors, so use number 0.
	$('#BookForm_blurb').val(book.description ? book.description : '');
	$('#BookForm_release_date').val(book.publishedDate);
	
	bookForm.validateAll();
	
	$('#isbn_result').hide();
}

/**
 * Function to run once the ISBN field has been validated.
 * 
 * @param field The field that was validated.
 * @param form The form doing the validation.
 */
function isbnValidated(field, form) 
{
	//Get isbn
	var isbn = field.val();
	
	//if we don't have an isbn then return
	if(!isbn){
		return;
	}
	
	// Already have a title?
	if($("#BookForm_title").val())
	{
		return;
	}
                                             
	//Set data (eg. the "q" parameter for google books)
	var queryData = {
		q: 'isbn:'+isbn,
	};

	// Do the ajax request
	$.ajax({
		dataType: "json",            // Type of data that you want (leave this on JSON)
		url: "https://www.googleapis.com/books/v1/volumes",    // The url you want to send AJAX to (google books)
		data: queryData,             // Pass the q parameter
		type: 'GET',                 // Request type (GET or POST)
		success: isbnSuccess,        // Name of the function to handle the result 
	});
}

/**
 * This function handles the AJAX result
 * 
 * @param result The result sent back from google books after an ISBN lookup
 */
function isbnSuccess(result)
{
	// Check if we have books
 	if(result.totalItems > 0)
	{
 		tmpResult = result.items[0].volumeInfo;
 		$('#isbn_result_link').text('Autocomplete: "' + tmpResult.title + '"?');
 		$('#isbn_result').show();
	}
	else
	{
		$('#isbn_result').hide();
	}
}

$(document).ready( function()   
{	
	// If you click on the green results link, autofill the other fields.
	$('#isbn_result_link').click(autoFill);
	
	// Title autocomplete.
	$('#BookForm_title').autocomplete({
		delay: 500,  		// Search after the user stops typing for 500ms
		minLength: 3,  		// Minimum length of the text before searching
		source: function(request,response) {
			// Search in title
			var s = 'intitle:"' + request.term + '"';

			// Search in author if needed
			var auth = $('#BookForm_author').val();
			if(auth)
			{
				s += '+inauthor:"' + auth + '"';
			}

			// Data (query string) to send
			var d = {
				q: s,
				projection: 'lite',
				maxResults: 5,
				key: apiKey,
			};
			
			// Do the AJAX request
			$.ajax({
				dataType: 'json',
				url: "https://www.googleapis.com/books/v1/volumes",
				data: d,
				success: function(result)
				{
					var res = [];
					
					if(result.totalItems > 0)
					{
						// Foreach result...
						$.each(result.items, function(i, val) {
							// We push some additional data, like the volume ID, so we can retrieve it easier
							res.push({
								volume: val.id,
								title: val.volumeInfo.title,
								author: val.volumeInfo.authors ? val.volumeInfo.authors[0] : '(unknown)',
							});
						});
					}
					
					response(res);
				}
			});
		},
		select: function( event, ui ) {
			// What to do when a user select something from the autocomplete
			$.ajax({
				dataType: 'json',
				url: "https://www.googleapis.com/books/v1/volumes/" + ui.item.volume,
				success: function(result)
				{
					// Auto fill the fields
					tmpResult = result.volumeInfo;
					autoFill(true);
				},
			});
		},
	}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		// This builds the actual autocomplete items
		return $( "<li>" )
			.append( "<a><span class='ac_main'>" + item.title + "</span><br><span class='ac_sub'>" + item.author + "</span></a>" )
	        .appendTo( ul );
	};
	
	// Author autocomplete
	$('#BookForm_author').autocomplete({
		delay: 500,
		minLength: 3,
		source: function(request,response) {
			// Query string data
			var d = {
				q: 'inauthor:"' + request.term + '"',
				projection: 'lite',
				maxResults: 8,
				key: apiKey,
			};
			$.ajax({
				dataType: 'json',
				url: "https://www.googleapis.com/books/v1/volumes",
				data: d,
				success: function(result)
				{
					var res = [];
					
					if(result.totalItems > 0)
					{
						$.each(result.items, function(i, val) {
							if(val.volumeInfo.authors)
							{
								// Keep some info for later...
								res.push({
									author: val.volumeInfo.authors[0],
								});
							}
						});
					}
					
					response(res);
				}
			});
		},
		select: function( event, ui ) {
			// Fill in just the author field and nothing else
			$('#BookForm_author').val(ui.item.author);
			return false;
		},
	}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		// Builds the actual items
		return $( "<li>" )
			.append( "<a><span class='ac_main'>" + item.author + "</span></a>" )
	        .appendTo( ul );
	};
});
