"use strict";

var activeForm = null;

/**
 * Function to be called by the Status form modal
 */
function statusSave()
{
	activeForm.ajaxSubmit();
}

/**
 * Function called once the status has been changed successfully.
 * 
 * @param result JSON result from server
 * @param form The form that calls this function
 */
function statusSuccess(result,form)
{
	// Close any active modals
	if(activeModal)
	{
		activeModal.dialog('destroy').remove();
	}

	// Update the button.
	switch(result['new'])
	{
		case 'removed':
			$('#status_link').addClass('bluegrey').removeClass('green').text('Add to my list');
			break;
		case 'wishlist':
			$('#status_link').addClass('green').removeClass('bluegrey').text('On wishlist (change)');
			break;
		case 'progress':
			$('#status_link').addClass('green').removeClass('bluegrey').text('Reading (change)');
			break;
		case 'finished':
			$('#status_link').addClass('green').removeClass('bluegrey').text('Finished (change)');
			break;
	}
}

/**
 * Function called once the rating has been saved successfully.
 * 
 * @param result JSON result from the server
 * @param form The form calling this function
 */
function ratingSuccess(result,form)
{
	$("#book_rating_val").text(result.avg + ' (based on ' + result.votes + ' vote' + (result.votes == 1 ? '' : 's') + ')');
}

$(document).ready( function() {
	// Init tabs
	$('#book_tabs').tabs();
	
	if(loggedIn)
	{
		// Init the rating form with the Forms system
		var rateForm = new Form('rating-form',{
			'ajaxSubmit': true,
			'ajaxSuccess': ratingSuccess,
		});
		
		// Register the rating select box to the form
		rateForm.addField('rating', {
			validation:function(field,value,messages) {
				if(jQuery.trim(value) == '')
				{
					messages.push('Please select a rating!');
				}
			},
			errorId: 'rating-error',
		});
	}
	
});