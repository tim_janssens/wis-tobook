"use strict";

/**
 * Shows the cover of a book (if the icon is clicked).
 * 
 * @param isbn ISBN number to load cover for
 */
function showCover(isbn)
{
	openModal("<div style='text-align: center;'><img src='" + baseUrl + "images/covers/" + isbn + ".jpeg' alt='cover'/></div>",'300px');
}

$(document).ready( function() {
	// Init tabs
	$("#user_tabs").tabs();
	
	if( totalBooks > 0 )
	{
		// Init books DataTable
		var oBookTable = $('#books_list').dataTable({
			// Use jQuery UI styles
			"bJQueryUI": true,
			
			// Show 5 pages in pagination
			"iShowPages": 5,
			
			// Use full numbers pagination (default is just prev/next)
			"sPaginationType": "full_numbers",
			
			// Columns
			"aoColumnDefs": [{
				"aTargets": [ 0 ],     // Applies to column(s)
				"mDataProp": "image",  // Data Property (used when extracting server data)
				"sClass": "image",     // Class for every table cell
				"sWidth": '25px',      // Width of the column
				"bSortable": false,    // Column is no sortable
			},{
				"aTargets": [ 1 ],
				"mDataProp": "title",
				"sClass": "title",
				"sWidth": '30%',
			},{
				"aTargets": [ 2 ],
				"mDataProp": "author",
				"sClass": "author",
				"sWidth": '30%',
			},{
				"aTargets": [ 3 ],
				"mDataProp": "status",
				"sClass": "status",
			}],
			
			// Default sorting
			"aaSorting": [[ 1, "asc" ]],
			
			// Disable auto width (needed for responsive design)
			"bAutoWidth": false,
			
			// Show processing indicator
			"bProcessing": true,
			
			// Process on server side
			"bServerSide": true,
			
			// Server side processing URL
			"sAjaxSource": loadBooksUrl,
			
			// Extra params to send to server
			"fnServerParams": function(aoData) {
				aoData.push({'name': 'book_types', 'value': $('#book_types').val()});
			},
			
			// Don't do AJAX first time
			"iDeferLoading": totalBooks,
			
			// Language settings
			"oLanguage": {
				"sEmptyTable": "This user has not added any books",
				"sZeroRecords": "No books match the current filters",  
			},
			
			// Function to call every row insert
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				// Add class based on type (wishlist, progress, finished)
	           	$(nRow).addClass('book_'+aData.status);
	        },
		});
		
		// Delay searching (filtering) until user stops typing for a while (limits AJAX calls)
		oBookTable.fnSetFilteringDelay();

		// Redraws the table if the select box is changed
		$("#book_types").change( function(){
			oBookTable.fnDraw();
		});
		
		// Show cover if icon is clicked.
		$('.show_book_cover').live('click', function() {
			showCover($(this).data('isbn'));
		});
	}
	
	if( totalEvents > 0 )
	{
		// Init events datatable
		var oTable = $("#events_list").dataTable({
			// Use jQuery UI styles
			"bJQueryUI": true,
			
			// Show 5 pages in pagination
			"iShowPages": 5,
			
			// Pagination type
			"sPaginationType": "full_numbers",
			
			// Columns
			"aoColumnDefs": [{
				"aTargets": [ 0 ],
				"mDataProp": "icon",
				"sClass": "icon",
				"sWidth": '25px',
				"iDataSort": 2,       // If sorted, actually sort on column 2 instead
			},{
				"aTargets": [ 1 ],
				"mDataProp": "name",
				"sClass": "name",
				"sWidth": '25%',
			},{
				"aTargets": [ 2 ],
				"mDataProp": "date",
				"sClass": "date",
				"sWidth": '150px',
			},{
				"aTargets": [ 3 ],
				"mDataProp": "location",
				"sClass": "location",
				"sWidth": '25%',
			},{
				"aTargets": [ 4 ],
				"mDataProp": "attendees",
				"sClass": "attendees",
			}],
			
			// Default sorting
			"aaSorting": [[ 2, "asc" ]],
			
			// Disable auto width (needed for responsive design)
			"bAutoWidth": false,
			
			// Show processing indicator
			"bProcessing": true,
			
			// Process on server side
			"bServerSide": true,
			
			// Ajax Source for the data
			"sAjaxSource": loadEventsUrl,
			
			// Additional data to send to the server
			"fnServerParams": function(aoData) {
				aoData.push({'name': 'event_types', 'value': $('#event_types').val()});
			},
			
			// Don't do ajax the first time (and specify the current amount in the table)
			"iDeferLoading": [ activeEvents, totalEvents ],
			
			// Language settings
			"oLanguage": {
				"sEmptyTable": "This user has not created any events",
				"sZeroRecords": "No events match the current filters",  
			},
			
			// Function to call every row
			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
				// Add class based on the type (upcoming, ongoing, erchived)
	           	$(nRow).addClass('event_'+aData.type);
	        },
		});
	
		// Delays search slightly in order to not overload the server
		oTable.fnSetFilteringDelay();
		
		$("#event_types").change( function(){
			// Redraws the table if the select box is changed
			oTable.fnDraw();
		});
	}
	
});