"use strict";

$(document).ready( function() {
	// Use Google Maps autocomplete on the address field
	new google.maps.places.Autocomplete(
		document.getElementById('PreferencesForm_address')
	);
	
	// If we support GeoLocation...
	if(navigator.geolocation)
	{
		// GeoLocation error
		var geoError = function()
		{
			$('#btnGeoLocate').parent().html('Unable to find your location');
		};
		
		// GeoLocation success
		var geoSuccess = function(position)
		{
			// Do another AJAX request to get the actual address
			$.ajax({
				dataType: "json",
				url: 'http://maps.google.com/maps/api/geocode/json?latlng='
					+ position.coords.latitude + ','
					+ position.coords.longitude + '&sensor=false',
				success: function(result)
				{
					if(result.status == 'OK')
					{
						$('#PreferencesForm_address').val(result.results[0].formatted_address);
						$('#btnGeoLocate').parent().empty();
					}
					else
					{
						geoError();
					}
				}
			});
			console.log(position);
		};
		
		// Make button do the geolocation
		$("#btnGeoLocate").show().click( function() {
			navigator.geolocation.getCurrentPosition(geoSuccess,geoError);
		});
	}
});