"use strict";

/**
 * jcrop API instance
 */
var jcrop_api = false;

/**
 * FileReader API instance
 */
var fileReader = false;

/**
 * Reference to avatar form
 * 
 * @type Form
 */
var avaForm = null;

/**
 * Updates the crop values in the hidden text fields.
 * 
 * @param c Array with the crop data
 */
function updateCrop(c) {
	$("#clear_crop").removeClass('grey');
    $('#AvatarForm_x1').val(c.x);
    $('#AvatarForm_y1').val(c.y);
    $('#AvatarForm_x2').val(c.x2);
    $('#AvatarForm_y2').val(c.y2);
    $('#AvatarForm_width').val(c.w);
    $('#AvatarForm_height').val(c.h);
};

$(document).ready( function() {
	// Register the Avatar form in the Forms system.
	avaForm = new Form('avatar-form');
	avaForm.addField('AvatarForm_image');
	
	$("#AvatarForm_image").change(function(e) {
		// Is it a valid image?
		if(e.target.files[0].type != "image/png" && e.target.files[0].type != "image/jpeg")
		{
			avaForm.setError($(this),'Please select a PNG or JPG image!');
			return;
		}
		
		avaForm.clearError($(this));
		
		// Do we support the FileReader API?
		if(!window.File || !window.FileReader)
		{
			$('#crop_options_fake').show();
			return;
		}

	    // Read the selected file
	    var avatar = $('#AvatarForm_image')[0].files[0];

	    fileReader.readAsDataURL(avatar);
	});
	
	// Reset the cropping
	$("#clear_crop").click( function() {
		jcrop_api.release();
		updateCrop({
			'x':'','y':'','x2':'','y2':'','w':'','h':'',
		});
		$("#clear_crop").addClass('grey');
		return false;
	});
	
	// Init FileReader
	fileReader = new FileReader();
    
	// What to do when a file is read
	fileReader.onload = function(e) {
		// Destroy previous image (if any)
		if(jcrop_api)
		{
			jcrop_api.destroy();
			updateCrop({
				'x':'','y':'','x2':'','y2':'','w':'','h':'',
			});
		}
		
		// Create new image
		var img = $("<img>")
			.attr('src',e.target.result);
		
		$("#avatar_preview").html(img);
		
		// Add the JCropping
		img.Jcrop({
			aspectRatio: 1,
			bgOpacity: .4,
			onChange: updateCrop,
			onSelect: updateCrop,
		}, function() {
			jcrop_api = this;
		});
				
	    $('#crop_options').show();
	};
});