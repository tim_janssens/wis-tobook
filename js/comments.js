"use strict";

/**
 * Appends a comment to the comments list.
 * 
 * To be used when adding comments through AJAX.
 * 
 * @param result AJAX result
 * @param form The form that made the ajax request
 */
function appendComment(result,form)
{
	try
	{
		// Get the comment number
		var commentCount = parseInt($('#comment_count').val())+1;
		var text = result.comment.replace('<!-- NUM -->',commentCount.toString());
		$('#comment_count').val(commentCount);
		
		// Insert comment into list
		$(text).appendTo("#comments-list .items").slideDown(200);
		
		// Hide "No comments yet" message
		$('#comments-list .empty').hide();

		// Clear the editor
		var rte = tinymce.editors['CommentForm_comment'];
		if(rte)
		{
			rte.setContent('');
		}
	}
	catch(e) { location.reload(); /* In case something goes wrong, simply reload */ };
}