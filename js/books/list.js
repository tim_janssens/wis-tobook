"use strict";

/**
 * Opens a modal with the cover of the book.
 * 
 * @param isbn ISBN number of the book to show cover for
 */
function showCover(isbn)
{
	openModal("<div style='text-align:center;'><img src='" + baseUrl + "images/covers/" + isbn + ".jpeg' alt='cover'/></div>",'300px');
}

$(document).ready( function() {
	
	// Initialize the Datatable
	var oTable = $("#books_list").dataTable({
		// Use jQuery UI
		"bJQueryUI": true,
		
		// Show up to 5 pages in the pagination
		"iShowPages": 5,
		
		// Pagination type (numbered, default is just next/prev)
		"sPaginationType": "full_numbers",
		
		// Columns
		"aoColumnDefs": [{
			"aTargets": [ 0 ],     // Target column
			"mDataProp": "image",  // Data property (used to extract data from server)
			"sClass": "image",     // Class to add to each table cell
			"bSortable": false,    // Column is not sortable
			"sWidth": '25px',      // Width of the column
		},{
			"aTargets": [ 1 ],
			"mDataProp": "title",
			"sClass": 'title',
			"sWidth": '25%',
		},{
			"aTargets": [ 2 ],
			"mDataProp": "author",
			"sClass": "author",
			"sWidth": '24%',
		},{
			"aTargets": [ 3 ],
			"mDataProp": "genre",
			"sClass": "genre",
			"sWidth": '23%',
		},{
			"aTargets": [ 4 ],
			"mDataProp": "release_date",
			"sClass": "release_date",
		}],
		
		// Default sorting
		"aaSorting": [[ 1, "asc" ]],
		
		// Disables automatic width (needed for responsive design)
		"bAutoWidth": false,
		
		// Show Processing indicator
		"bProcessing": true,
		
		// Process on server side
		"bServerSide": true,
		
		// Source to load data from
		"sAjaxSource": loadBooksUrl,
		
		// Don't load from AJAX the first time.
		// This also doubles to tell the total amount of books
		"iDeferLoading": totalBooks,
		
		// Language configurations
		"oLanguage": {
			"sEmptyTable": "There are no books in our database yet!",
			"sZeroRecords": "No books match the current filters",  
		},
	});
	
	// Delays the filtering (search) so that it doesn't do too many AJAX calls
	oTable.fnSetFilteringDelay();
	
	// Search: Title
	var tTimer = null;
	$('#search_title').keyup( function() {
		window.clearTimeout(tTimer);
		var val = $(this).val();
		tTimer = window.setTimeout(function() {
			oTable.fnFilter( val, 1 );  // Filter on column 1
		}, 250);
	});
	
	// Search: Author
	var aTimer = null;
	$('#search_author').keyup( function() {
		window.clearTimeout(aTimer);
		var val = $(this).val();
		aTimer = window.setTimeout(function() {
			oTable.fnFilter( val, 2 );  // Filter on column 2
		}, 250);
	});
	
	// Search: Genre
	$('#search_genre').change( function() {
		oTable.fnFilter( $(this).val(), 3);  // Filter on column 3
	});
	
	/**
	 * This function does the actual searching by date
	 */
	var dateSearch = function()
	{
		var s = $('#search_date_start').val();
		var e = $('#search_date_end').val();
		
		if(!s){ s = '---'; }
		if(!e){ e = '---'; }
		
		oTable.fnFilter(s + ';' + e, 4);  // Filter on column 4
	};
	
	// Init datepicker for the "from" time
	$('#search_date_start').datepicker({
		'dateFormat':"yy-mm-dd",
		'changeMonth': true,
		'changeYear': true,
		'onSelect': dateSearch,
		'onClose': function() {
			if(!$('#search_date_start').val())
			{
				dateSearch();
			}
		},
	});
	
	// Init datepicker for the "to" time
	$('#search_date_end').datepicker({
		'dateFormat':"yy-mm-dd",
		'changeMonth': true,
		'changeYear': true,
		'onSelect': dateSearch,
		'onClose': function() {
			if(!$('#search_date_end').val())
			{
				dateSearch();
			}
		},
	});
	
	// Toggle advanced filters
	$('#advanced_filter_toggle').show().click( function() {
		$('#filter_footer').toggle();
		if(!$(this).hasClass('green'))
		{
			$(this).addClass('green');
			location.href = '#filter_footer';
		}
		else
		{
			$(this).removeClass('green');
		}
	});
	
	// Adds a listener to the book icons to show the cover when clicked.
	$('.show_book_cover').live('click', function() {
		showCover($(this).data('isbn'));
	});
});