ToBook
======

ToBook is a social application that connects people interested in books, allowing them to create, view, search and manage information about books.

**Functionality:** People can create their own profiles and then add books to it from a database. If the book is not yet present, they can create a new book profile. They can also add information to and edit existing book profiles.

Every book on someone’s profile is given a status: Wish List, In Progress, Finished. After people finish reading a book, they will be able to rate it by adding a review. Other people are able to comment on the book without having to read it.

People can also create events to talk about books, books openings or exchanging books. They can set the location of the events on a map. Other users will then be able to search for events in a specific region.

**Social Integration:** People will be able to share the books on Facebook/Twitter/Google+/... .

**Technologies:**

- Ajax: Ajax is used throughout the site for things like adding comments and rating books.
- Web services: Possible integration with Google Books (REST)?
- Publishing: RDFa??
- HTML5: GEOLOCATION, MICRODATA?
- Mobility: Responsive design, allowing the same site to be used on mobile devices.
- Language and Framework: Most likely PHP, framework to be chosen.
- Google Maps: Maps with the location of the events.
